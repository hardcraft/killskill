CREATE TABLE IF NOT EXISTS `killskill_player_perks` (
  `player_id` INT(11) NOT NULL,
  `experience_amount` INT(11) NOT NULL DEFAULT 1,
  `lapis_chance` INT(11) NOT NULL DEFAULT 5,
  `potion_chance` INT(11) NOT NULL DEFAULT 20,
  `iron_chance` INT(11) NOT NULL DEFAULT 5,
  `diamond_chance` INT(11) NOT NULL DEFAULT 5,
  `blaze_chance` INT(11) NOT NULL DEFAULT 6,
  `supply_amount` INT(11) NOT NULL DEFAULT 2,
  `supply_level` INT(11) NOT NULL DEFAULT 1,
  PRIMARY KEY (`player_id`),
  UNIQUE INDEX `player_id_UNIQUE` (`player_id` ASC),
  CONSTRAINT `fk_killskill_player_perks_player_id`
    FOREIGN KEY (`player_id`)
    REFERENCES `player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;