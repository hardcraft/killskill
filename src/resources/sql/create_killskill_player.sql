CREATE TABLE IF NOT EXISTS `killskill_player` (
  `player_id` INT(11) NOT NULL,
  `experience_looted` INT(11) NOT NULL DEFAULT 0,
  `lapis_looted` INT(11) NOT NULL DEFAULT 0,
  `potion_looted` INT(11) NOT NULL DEFAULT 0,
  `iron_looted` INT(11) NOT NULL DEFAULT 0,
  `diamond_looted` INT(11) NOT NULL DEFAULT 0,
  `blaze_looted` INT(11) NOT NULL DEFAULT 0,
  `head_looted` INT(11) NOT NULL DEFAULT 0,
  `grenade_looted` INT(11) NOT NULL DEFAULT 0,
  `tnt_looted` INT(11) NOT NULL DEFAULT 0,
  `cannon_looted` INT(11) NOT NULL DEFAULT 0,
  `supply_opened` INT(11) NOT NULL DEFAULT 0,
  `play_time` INT(11) NOT NULL DEFAULT 0,
  `kills` INT(11) NOT NULL DEFAULT 0,
  `deaths` INT(11) NOT NULL DEFAULT 0,
  `coins_earned` DECIMAL(19,4) NOT NULL DEFAULT 0,
  PRIMARY KEY (`player_id`),
  UNIQUE INDEX `player_id_UNIQUE` (`player_id` ASC),
  CONSTRAINT `fk_killskill_player_id`
    FOREIGN KEY (`player_id`)
    REFERENCES `player` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci;