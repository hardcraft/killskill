package com.gmail.val59000mc.killskill.melting;

import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public interface MeltingApi {

	boolean isMeltable(ItemStack item);
	
	void attemptMelting(HCPlayer hcPlayer);
}
