package com.gmail.val59000mc.killskill.melting;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerQuitEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.ItemCompareOption;
import com.google.common.base.Preconditions;

public class MeltingManager extends HCListener implements MeltingApi{

	private HCGameAPI api;
	private Map<Material,Integer> maxMeltedMaterials; 
	private Map<UUID,ItemStack> waitingForMelting;

	public MeltingManager(HCGameAPI api) {
		this.api = api;
		this.maxMeltedMaterials = new HashMap<>();
		this.waitingForMelting = new HashMap<>();
		load();
	}

	private void load() {
		
		api.registerListener(this);

		maxMeltedMaterials.put(Material.DIAMOND_SWORD, 1);
		
		maxMeltedMaterials.put(Material.DIAMOND_HELMET, 3);
		maxMeltedMaterials.put(Material.DIAMOND_CHESTPLATE, 4);
		maxMeltedMaterials.put(Material.DIAMOND_LEGGINGS, 4);
		maxMeltedMaterials.put(Material.DIAMOND_BOOTS, 2);

		maxMeltedMaterials.put(Material.DIAMOND_AXE, 2);
		maxMeltedMaterials.put(Material.DIAMOND_PICKAXE, 2);
		maxMeltedMaterials.put(Material.DIAMOND_SPADE, 1);
		maxMeltedMaterials.put(Material.DIAMOND_HOE, 1);
		
		maxMeltedMaterials.put(Material.IRON_HELMET, 3);
		maxMeltedMaterials.put(Material.IRON_CHESTPLATE, 4);
		maxMeltedMaterials.put(Material.IRON_LEGGINGS, 4);
		maxMeltedMaterials.put(Material.IRON_BOOTS, 2);

		maxMeltedMaterials.put(Material.IRON_AXE, 2);
		maxMeltedMaterials.put(Material.IRON_PICKAXE, 2);
		maxMeltedMaterials.put(Material.IRON_SPADE, 1);
		maxMeltedMaterials.put(Material.IRON_HOE, 1);
	}

	@Override
	public boolean isMeltable(ItemStack item) {
		if(item == null)
			return false;
		return maxMeltedMaterials.containsKey(item.getType()) && item.getAmount() == 1;
	}

	@Override
	public void attemptMelting(HCPlayer hcPlayer) {
		Player player = hcPlayer.getPlayer();
		ItemStack item = player.getItemInHand();
		if(isMeltable(item)){
			UUID uuid = hcPlayer.getUuid();
			ItemStack waitingItem = waitingForMelting.get(uuid);
			ItemStack meltResult = getMeltResult(item);
			String materialName = api.getStringsAPI().get("killskill.melting."+meltResult.getType().name()).toString()+(meltResult.getAmount() > 1 ? "s" : "");
			if(waitingItem == null || !Inventories.itemEquals(item, waitingItem, new ItemCompareOption.Builder()
					.withAmount(true)
					.withDamageValue(true)
					.withDisplayName(true)
					.withEnchantments(true)
					.withLore(true)
					.withMaterial(true)
					.build())
			){
				// store the item for melting confirmation
				waitingForMelting.put(uuid, item);
				api.getStringsAPI()
					.get("killskill.melting.quote")
					.replace("%amount%", String.valueOf(meltResult.getAmount()))
					.replace("%material%", materialName)
					.sendChatP(hcPlayer);
				api.getSoundAPI().play(hcPlayer, Sound.NOTE_PIANO, 1, 0.5f);
			}else{
				// melt the item and give melt result
				waitingForMelting.remove(uuid);
				api.getStringsAPI()
					.get("killskill.melting.melted")
					.replace("%amount%", String.valueOf(meltResult.getAmount()))
					.replace("%material%", materialName)
					.sendChatP(hcPlayer);
				api.getSoundAPI().play(hcPlayer, Sound.NOTE_PIANO, 1, 2f);
				player.setItemInHand(null);
				player.getInventory().addItem(meltResult);
			}
		}else{
			api.getStringsAPI()
				.get("killskill.melting.not-meltable")
				.sendChatP(hcPlayer);
		}
		
	}
	
	private ItemStack getMeltResult(ItemStack item){
		Material mat = item.getType();
		Preconditions.checkArgument(mat.name().contains("IRON") || mat.name().contains("DIAMOND"));
		int maxAmount = maxMeltedMaterials.get(mat);
		short maxDurability = mat.getMaxDurability();
		short durability = (short) (maxDurability - item.getDurability());
		double durabilityRatio = (double) durability / (double) maxDurability;
		int meltAmount = (int) Math.ceil(durabilityRatio * (double) maxAmount);
		
		if(mat.name().contains("DIAMOND")){
			return new ItemStack(Material.DIAMOND, meltAmount);
		}else{
			return new ItemStack(Material.IRON_INGOT, meltAmount);
		}
	}
	
	@EventHandler
	public void onDisconnect(HCPlayerQuitEvent e){
		waitingForMelting.remove(e.getHcPlayer().getUuid());
	}

}
