package com.gmail.val59000mc.killskill.common;

public class Constants {
	
	public static final String MEMBER_STUFF_NAME = "Membre";
	public static final String VIP_STUFF_NAME = "VIP";
	public static final String VIP_PLUS_STUFF_NAME = "VIP+";
	
	public static final String SPEC_INV_NAME = "spec-killskill";
	public static final String GRENADE_METADATA = "killskill-grenade";
	public static final String TNT_METADATA = "killskill-tnt";
	public static final String CANNON_METADATA = "killskill-cannon";
	
	public static final int DEFAULT_SECONDS_BEFORE_SUPPLY_EXPIRATION = 90;
	
}
