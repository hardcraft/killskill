package com.gmail.val59000mc.killskill;

import java.io.File;

import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.HCGamesLib;
import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.commands.HCBoosterCommand;
import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.hcgameslib.commands.HCKickAllCommand;
import com.gmail.val59000mc.hcgameslib.commands.HCSayCommand;
import com.gmail.val59000mc.hcgameslib.commands.HCWhitelistCommand;
import com.gmail.val59000mc.hcgameslib.game.HCGame;
import com.gmail.val59000mc.hcgameslib.listeners.HCAFKListener;
import com.gmail.val59000mc.hcgameslib.listeners.HCBoosterListener;
import com.gmail.val59000mc.hcgameslib.listeners.HCChatListener;
import com.gmail.val59000mc.hcgameslib.listeners.HCMySQLListener;
import com.gmail.val59000mc.hcgameslib.listeners.HCPingListener;
import com.gmail.val59000mc.hcgameslib.listeners.HCPlayerConnectionListener;
import com.gmail.val59000mc.hcgameslib.listeners.HCPlayerDamageListener;
import com.gmail.val59000mc.hcgameslib.listeners.HCPlayerDeathListener;
import com.gmail.val59000mc.killskill.callbacks.KillSkillCallbacks;
import com.gmail.val59000mc.killskill.commands.KillSkillPlayCommand;
import com.gmail.val59000mc.killskill.commands.KillSkillSpawnCommand;
import com.gmail.val59000mc.killskill.commands.KillSkillSpecCommand;
import com.gmail.val59000mc.killskill.commands.KillSkillTestCommand;
import com.gmail.val59000mc.killskill.commands.KillSkillVanishCommand;
import com.gmail.val59000mc.killskill.listeners.KillSkillBlocksListener;
import com.gmail.val59000mc.killskill.listeners.KillSkillCommandOverrideListener;
import com.gmail.val59000mc.killskill.listeners.KillSkillConnectionListener;
import com.gmail.val59000mc.killskill.listeners.KillSkillCraftListener;
import com.gmail.val59000mc.killskill.listeners.KillSkillDamageAndInteractListener;
import com.gmail.val59000mc.killskill.listeners.KillSkillInventoryListener;
import com.gmail.val59000mc.killskill.listeners.KillSkillKickAllListener;
import com.gmail.val59000mc.killskill.listeners.KillSkillMySQLListener;
import com.gmail.val59000mc.killskill.listeners.KillSkillSIGListener;
import com.gmail.val59000mc.killskill.listeners.KillSkillSupplyListener;
import com.gmail.val59000mc.spigotutils.Configurations;
import com.google.common.collect.Sets;

public class KillSkill extends JavaPlugin {
	
	public void onEnable(){

		this.getDataFolder().mkdirs();
		File config = Configurations.saveConfigDefaultsToFile(Configurations.getConfigFromResource(this, "config.yml"), new File(this.getDataFolder(),"config.yml"));
		File lang = Configurations.saveConfigDefaultsToFile(Configurations.getConfigFromResource(this, "lang.yml"), new File(this.getDataFolder(),"lang.yml"));
		

		HCCommand specExecutor = new KillSkillSpecCommand(this);
		HCCommand vanishCmd = new KillSkillVanishCommand(this);
		HCCommand playCmd = new KillSkillPlayCommand(this);
		
		HCGameAPI game = new HCGame.Builder("Kill Skill", this, config, lang)
				.withPluginCallbacks(new KillSkillCallbacks())
				.withListeners(Sets.newHashSet(
					new HCChatListener(),
					new HCMySQLListener(),
					new KillSkillMySQLListener(),
					new HCPingListener(),
					new HCPlayerConnectionListener(),
					new HCPlayerDeathListener(),
					new KillSkillBlocksListener(),
					new HCPlayerDamageListener(),
					new KillSkillDamageAndInteractListener(),
					new KillSkillInventoryListener(),
					new KillSkillSIGListener(),
					new KillSkillConnectionListener(),
					new KillSkillCommandOverrideListener(specExecutor, vanishCmd, playCmd), // /spec, /vanish and /playcommands will be overriden afterwards
					new KillSkillCraftListener(),
					new KillSkillKickAllListener(),
					new HCBoosterListener(),
					new KillSkillSupplyListener(),
					new HCAFKListener()
				))
				.withCommands(Sets.newHashSet(
					new KillSkillTestCommand(this),
					new HCWhitelistCommand(HCGamesLib.getPlugin()),
					new KillSkillSpawnCommand(this),
					new HCKickAllCommand(HCGamesLib.getPlugin()),
					new HCSayCommand(HCGamesLib.getPlugin()),
					new HCBoosterCommand(HCGamesLib.getPlugin())
				))
				.build();
		game.loadGame();
	}
	public void onDisable(){
	}
}
