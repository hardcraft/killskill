package com.gmail.val59000mc.killskill.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.killskill.supply.SupplyChest;

public class KillSkillSupplyOpenEvent extends HCEvent{

	private SupplyChest chest;
	private KillSkillPlayer hcPlayer;
	
	public KillSkillSupplyOpenEvent(HCGameAPI api, SupplyChest chest, KillSkillPlayer hcPlayer){
		super(api);
		this.chest = chest;
		this.hcPlayer = hcPlayer;
	}

	public SupplyChest getSupplyChest() {
		return chest;
	}

	public KillSkillPlayer getKsPlayer() {
		return hcPlayer;
	}
	
}
