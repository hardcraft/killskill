package com.gmail.val59000mc.killskill.events;

import org.bukkit.event.Cancellable;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;

public class KillSkillTypeSpawnCommandEvent extends HCEvent implements Cancellable{

	private KillSkillPlayer ksPlayer;
	private boolean isCancelled;

	public KillSkillTypeSpawnCommandEvent(HCGameAPI api, KillSkillPlayer ksPlayer) {
		super(api);
		this.ksPlayer = ksPlayer;
	}

	public KillSkillPlayer getKsPlayer() {
		return ksPlayer;
	}

	@Override
	public boolean isCancelled() {
		return isCancelled;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.isCancelled = cancel;
	}
	
}