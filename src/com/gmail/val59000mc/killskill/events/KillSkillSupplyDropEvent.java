package com.gmail.val59000mc.killskill.events;

import org.bukkit.event.Cancellable;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.killskill.supply.SupplyChest;

public class KillSkillSupplyDropEvent extends HCEvent implements Cancellable{

	private SupplyChest chest;
	private boolean isCancelled;
	
	public KillSkillSupplyDropEvent(HCGameAPI api, SupplyChest chest){
		super(api);
		this.chest = chest;
	}

	public SupplyChest getSupplyChest() {
		return chest;
	}

	@Override
	public boolean isCancelled() {
		return isCancelled;
	}

	@Override
	public void setCancelled(boolean isCancelled) {
		this.isCancelled = isCancelled;
	}

	
}
