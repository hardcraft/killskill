package com.gmail.val59000mc.killskill.events;

import org.bukkit.block.Block;
import org.bukkit.event.Cancellable;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;

public class KillSkillStartPortalBonusEvent extends HCEvent implements Cancellable{

	private boolean isCancelled;
	private KillSkillPlayer ksPlayer;
	private Block portal;
	
	public KillSkillStartPortalBonusEvent(HCGameAPI api, Block portal, KillSkillPlayer ksPlayer) {
		super(api);
		this.portal = portal;
		this.ksPlayer = ksPlayer;
		this.isCancelled = false;
	}

	@Override
	public boolean isCancelled() {
		return isCancelled;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.isCancelled = cancel;
	}

	public KillSkillPlayer getKsPlayer() {
		return ksPlayer;
	}

	public Block getPortal() {
		return portal;
	}
	
	

}
