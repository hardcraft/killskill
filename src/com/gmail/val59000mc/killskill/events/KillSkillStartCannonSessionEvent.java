package com.gmail.val59000mc.killskill.events;

import org.bukkit.event.Cancellable;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;

public class KillSkillStartCannonSessionEvent extends HCEvent implements Cancellable{

	private boolean isCancelled;
	private KillSkillPlayer ksPlayer;
	
	public KillSkillStartCannonSessionEvent(HCGameAPI api, KillSkillPlayer ksPlayer) {
		super(api);
		this.ksPlayer = ksPlayer;
		this.isCancelled = false;
	}

	@Override
	public boolean isCancelled() {
		return isCancelled;
	}

	@Override
	public void setCancelled(boolean cancel) {
		this.isCancelled = cancel;
	}

	public KillSkillPlayer getKsPlayer() {
		return ksPlayer;
	}
	
	

}
