package com.gmail.val59000mc.killskill.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.killskill.supply.SupplyChest;

public class KillSkillSupplyTouchesGroundEvent extends HCEvent{

	private SupplyChest chest;
	
	public KillSkillSupplyTouchesGroundEvent(HCGameAPI api, SupplyChest chest){
		super(api);
		this.chest = chest;
	}

	public SupplyChest getSupplyChest() {
		return chest;
	}
	
}
