package com.gmail.val59000mc.killskill.events;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;

public class KillSkillOpenSpecInventoryEvent extends HCEvent{

	private KillSkillPlayer ksPlayer;

	public KillSkillOpenSpecInventoryEvent(HCGameAPI api, KillSkillPlayer ksPlayer) {
		super(api);
		this.ksPlayer = ksPlayer;
	}

	public KillSkillPlayer getKsPlayer() {
		return ksPlayer;
	}
	
	

}
