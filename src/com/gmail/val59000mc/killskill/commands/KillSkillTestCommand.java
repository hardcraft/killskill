package com.gmail.val59000mc.killskill.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.killskill.items.KillSkillItems;
import com.gmail.val59000mc.killskill.supply.SupplyApi;
import com.gmail.val59000mc.killskill.supply.SupplyChest;

public class KillSkillTestCommand extends HCCommand{

	private SupplyApi supplyManager;
	
	public KillSkillTestCommand(JavaPlugin plugin) {
		super(plugin, "test");
	}
	
	
	public void setSupplyManager(SupplyApi supplyManager) {
		this.supplyManager = supplyManager;
	}


	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player){
			
			Player player = (Player) sender;
			
			if(args.length == 0){
				player.sendMessage("§cMissing argument !");
			}else{
				switch(args[0]){
					case "give-head":
						giveHead(player);
						break;
					case "give-creeper":
						giveCreeper(player);
						break;
					case "give-tnt":
						giveTnt(player);
						break;
					case "give-cannon":
						giveCannon(player);
						break;
					case "drop-supply":
						dropSupply(player);
						break;
					default:
						player.sendMessage("§cUnknown command !");
						break;
				}
			}
		}
		return true;
	}

	private void giveHead(Player player) {
		player.sendMessage("§aHead item given");
		player.getInventory().addItem(KillSkillItems.getHeadItem(1));
	}

	private void giveCreeper(Player player) {
		player.sendMessage("§aCreeper item given");
		player.getInventory().addItem(KillSkillItems.getGrenadeItem(1));
	}

	private void giveTnt(Player player) {
		player.sendMessage("§aTNT item given");
		player.getInventory().addItem(KillSkillItems.getTntItem(1));
	}

	private void giveCannon(Player player) {
		player.sendMessage("§aCannon item given");
		player.getInventory().addItem(KillSkillItems.getCanonItem(1));
	}

	private void dropSupply(Player player) {
		SupplyChest supply = supplyManager.dropRandomSupply();
		if(supply == null){
			player.sendMessage("§aSupply could not be dropped.");
		}else{
			player.sendMessage("§aSupply dropped.");
		}
	}
}
