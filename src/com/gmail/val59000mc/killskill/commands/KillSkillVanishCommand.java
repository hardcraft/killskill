package com.gmail.val59000mc.killskill.commands;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.hcgameslib.events.HCGameStateChangeEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerQuitEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerStateChangeEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.players.PlayersManager;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.killskill.callbacks.KillSkillCallbacks;
import com.gmail.val59000mc.killskill.events.KillSkillOpenSpecInventoryEvent;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;

public class KillSkillVanishCommand extends HCCommand{
	public KillSkillVanishCommand(JavaPlugin plugin) {
		super(plugin, "vanish");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player){
			
			Player player = (Player) sender;
			
			if(player.hasPermission("hardcraftpvp.set-vanished")){

				HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
				
				if(hcPlayer != null){

					
					switch(getAPi().getGameState()){
						case CONTINUOUS:
							// allow spec at this state
							break;
						default:
							 // no spec at this state
							getStringsApi()
								.get("messages.command.vanish.not-allowed-now")
								.sendChatP(hcPlayer);
							return true;
					}
					
					if(hcPlayer.is(PlayerState.VANISHED)){
						Bukkit.getPluginManager().callEvent(new KillSkillOpenSpecInventoryEvent(getAPi(), (KillSkillPlayer) hcPlayer));
						return true;
					}
					
					if(!((KillSkillCallbacks) getAPi().getCallbacksApi()).getLobbyBounds().contains(hcPlayer.getPlayer().getLocation())){
						getStringsApi()
							.get("killskill.command.vanish.not-in-spawn")
							.sendChatP(hcPlayer);
						return true;
					}
					
					KillSkillTypeVanishCommandEvent typeCommandEvent = new KillSkillTypeVanishCommandEvent(getAPi(), hcPlayer);
					Bukkit.getPluginManager().callEvent(typeCommandEvent);
					if(!typeCommandEvent.isCancelled()){
						
						getAPi().buildTask("command vanish for "+hcPlayer.getName(), new HCTask() {
							
							private int countdown = 5;
							
							@Override
							public void run() {
								
								getStringsApi()
									.get("messages.command.vanish.countdown-title")
									.replace("%time%", String.valueOf(countdown))
									.sendTitle(hcPlayer, 0, 21, 1);
								getSoundApi().play(hcPlayer, Sound.NOTE_STICKS, 1, 2);
								
								countdown--;
								
							}
							
						})
						.withIterations(5)
						.withLastCallback(new HCTask() {
							
							@Override
							public void run() {
								stop();
								PlayersManager pm = (PlayersManager) getAPi().getPlayersManagerAPI();
								pm.vanishPlayer(hcPlayer);
								pm.refreshVisiblePlayers(hcPlayer);
								pm.refreshRankNameTag(hcPlayer,0);
								getStringsApi()
									.get("killskill.player-has-left-the-game")
									.replace("%player%", hcPlayer.getName())
									.sendChatP();
							}	
						}).addListener(new HCTaskListener(){
							
							@EventHandler
							public void onCancelVanish(KillSkillCancelVanishTimeoutEvent e){
								if(e.getHcPlayer().equals(hcPlayer)){
									cancelVanish(hcPlayer);
								}
							}
							
							@EventHandler
							public void onGameStateChange(HCGameStateChangeEvent e){
								cancelVanish(hcPlayer);
							}
							
							@EventHandler
							public void onTypeCommandAgain(KillSkillTypeVanishCommandEvent e){
								if(e.getHcPlayer().equals(hcPlayer)){
									cancelVanish(hcPlayer);
									e.setCancelled(true);
								}
							}

							@EventHandler
							public void onPlayerStateGame(HCPlayerStateChangeEvent e){
								if(e.getHcPlayer().equals(hcPlayer)){
									cancelVanish(hcPlayer);
								}
							}

							@EventHandler
							public void onQuit(HCPlayerQuitEvent e){
								if(e.getHcPlayer().equals(hcPlayer)){
									cancelVanish(hcPlayer);
								}
							}
							
							private void cancelVanish(HCPlayer hcPlayer){
								getScheduler().stop();
								getStringsApi()
									.get("messages.command.vanish.cancel-timeout")
									.sendChatP(hcPlayer);
							}
							
						})
						.build()
						.start();
						
					}
					
				}
				
			}
		}
		return true;
	}

	private class KillSkillCancelVanishTimeoutEvent extends HCEvent{

		private HCPlayer hcPlayer;

		public KillSkillCancelVanishTimeoutEvent(HCGameAPI api, HCPlayer hcPlayer) {
			super(api);
			this.hcPlayer = hcPlayer;
		}

		public HCPlayer getHcPlayer() {
			return hcPlayer;
		}
		
	}
	
	private class KillSkillTypeVanishCommandEvent extends HCEvent implements Cancellable{

		private HCPlayer hcPlayer;
		private boolean isCancelled;

		public KillSkillTypeVanishCommandEvent(HCGameAPI api, HCPlayer hcPlayer) {
			super(api);
			this.hcPlayer = hcPlayer;
		}

		public HCPlayer getHcPlayer() {
			return hcPlayer;
		}

		@Override
		public boolean isCancelled() {
			return isCancelled;
		}

		@Override
		public void setCancelled(boolean cancel) {
			this.isCancelled = cancel;
		}
		
	}
}
