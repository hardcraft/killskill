package com.gmail.val59000mc.killskill.commands;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.EventHandler;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.hcgameslib.events.HCGameStateChangeEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerQuitEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerStateChangeEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.players.PlayersManager;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.killskill.callbacks.KillSkillCallbacks;
import com.gmail.val59000mc.killskill.events.KillSkillOpenSpecInventoryEvent;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.google.common.collect.Sets;

public class KillSkillSpecCommand extends HCCommand{

	
	public KillSkillSpecCommand(JavaPlugin plugin) {
		super(plugin, "spec");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player){
			
			Player player = (Player) sender;

			HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
			
			if(hcPlayer != null && hcPlayer.isOnline()){
				
				switch(getAPi().getGameState()){
					case CONTINUOUS:
						// allow spec at this state
						break;
					default:
						 // no spec at this state
						getStringsApi()
							.get("messages.command.spec.not-allowed-now")
							.sendChatP(hcPlayer);
						return true;
				}
				
				if(hcPlayer.isAny(Sets.newHashSet(PlayerState.SPECTATING,PlayerState.DEAD))){
					Bukkit.getPluginManager().callEvent(new KillSkillOpenSpecInventoryEvent(getAPi(), (KillSkillPlayer) hcPlayer));
					return true;
				}
				
				if(!((KillSkillCallbacks) getAPi().getCallbacksApi()).getLobbyBounds().contains(hcPlayer.getPlayer().getLocation())){
					getStringsApi()
						.get("killskill.command.spec.not-in-spawn")
						.sendChatP(hcPlayer);
					return true;
				}
				
				KillSkillTypeSpecCommandEvent typeCommandEvent = new KillSkillTypeSpecCommandEvent(getAPi(), hcPlayer);
				Bukkit.getPluginManager().callEvent(typeCommandEvent);
				if(!typeCommandEvent.isCancelled()){
					
					getAPi().buildTask("command spec for "+hcPlayer.getName(), new HCTask() {
						
						private int countdown = 5;
						
						@Override
						public void run() {
							
							getStringsApi()
								.get("messages.command.spec.countdown-title")
								.replace("%time%", String.valueOf(countdown))
								.sendTitle(hcPlayer, 0, 21, 0);
							getSoundApi().play(hcPlayer, Sound.NOTE_STICKS, 1, 2);
							
							countdown--;
							
							
						}
						
					})
					.withIterations(5)
					.withLastCallback(new HCTask() {
						
						@Override
						public void run() {
							stop();
							PlayersManager pm = (PlayersManager) getAPi().getPlayersManagerAPI();
							pm.spectatePlayer(hcPlayer);
							pm.refreshVisiblePlayers(hcPlayer);
							pm.refreshRankNameTag(hcPlayer,0);
						}
						
					}).addListener(new HCTaskListener(){
						
						@EventHandler
						public void onCancelSpec(KillSkillCancelSpecTimeoutEvent e){
							if(e.getHcPlayer().equals(hcPlayer)){
								cancelSpec(hcPlayer);
							}
						}
						
						@EventHandler
						public void onGameStateChange(HCGameStateChangeEvent e){
							cancelSpec(hcPlayer);
						}
						
						@EventHandler
						public void onTypeCommandAgain(KillSkillTypeSpecCommandEvent e){
							if(e.getHcPlayer().equals(hcPlayer)){
								cancelSpec(hcPlayer);
								e.setCancelled(true);
							}
						}

						@EventHandler
						public void onPlayerStateGame(HCPlayerStateChangeEvent e){
							if(e.getHcPlayer().equals(hcPlayer)){
								cancelSpec(hcPlayer);
							}
						}

						@EventHandler
						public void onQuit(HCPlayerQuitEvent e){
							if(e.getHcPlayer().equals(hcPlayer)){
								cancelSpec(hcPlayer);
							}
						}
						
						private void cancelSpec(HCPlayer hcPlayer){
							getScheduler().stop();
							getStringsApi()
								.get("messages.command.spec.cancel-timeout")
								.sendChatP(hcPlayer);
						}
						
					})
					.build()
					.start();

				}
				
			}
			
		}
		return true;
	}

	private class KillSkillCancelSpecTimeoutEvent extends HCEvent{

		private HCPlayer hcPlayer;

		public KillSkillCancelSpecTimeoutEvent(HCGameAPI api, HCPlayer hcPlayer) {
			super(api);
			this.hcPlayer = hcPlayer;
		}

		public HCPlayer getHcPlayer() {
			return hcPlayer;
		}
		
	}
	
	private class KillSkillTypeSpecCommandEvent extends HCEvent implements Cancellable{

		private HCPlayer hcPlayer;
		private boolean isCancelled;

		public KillSkillTypeSpecCommandEvent(HCGameAPI api, HCPlayer hcPlayer) {
			super(api);
			this.hcPlayer = hcPlayer;
		}

		public HCPlayer getHcPlayer() {
			return hcPlayer;
		}

		@Override
		public boolean isCancelled() {
			return isCancelled;
		}

		@Override
		public void setCancelled(boolean cancel) {
			this.isCancelled = cancel;
		}
		
	}
}
