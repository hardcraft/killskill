package com.gmail.val59000mc.killskill.commands;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.hcgameslib.events.HCEvent;
import com.gmail.val59000mc.hcgameslib.events.HCGameStateChangeEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerDamagedByHCPlayerEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerDamagedByPotionEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerDamagedEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerQuitEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerStateChangeEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.killskill.events.KillSkillTypeSpawnCommandEvent;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.google.common.collect.Sets;

public class KillSkillSpawnCommand extends HCCommand{

	
	public KillSkillSpawnCommand(JavaPlugin plugin) {
		super(plugin, "spawn");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player){
			
			Player player = (Player) sender;

			KillSkillPlayer hcPlayer = (KillSkillPlayer) getPmApi().getHCPlayer(player);
			
			if(hcPlayer != null){
				
				boolean allowedToUseNow = true;
				
				switch(getAPi().getGameState()){
					case CONTINUOUS:
						allowedToUseNow = true;
						break;
					default:
						allowedToUseNow = false;
						break;
				}
				
				if(hcPlayer.isAny(Sets.newHashSet(PlayerState.SPECTATING,PlayerState.DEAD,PlayerState.VANISHED))){
					allowedToUseNow = false;
				}

				if(!allowedToUseNow){
					getStringsApi()
						.get("killskill.command.spawn.not-allowed-now")
						.sendChatP(hcPlayer);
					return true;
				}

				KillSkillTypeSpawnCommandEvent typeCommandEvent = new KillSkillTypeSpawnCommandEvent(getAPi(), hcPlayer);
				Bukkit.getPluginManager().callEvent(typeCommandEvent);
				if(!typeCommandEvent.isCancelled()){
					
					getAPi().buildTask("command spawn for "+hcPlayer.getName(), new HCTask() {
						
						private int countdown = 3;
						private Location initialLocation = player.getLocation();
						
						@Override
						public void run() {
							
							if(hcPlayer.isOnline()){
								Location location = hcPlayer.getPlayer().getLocation();
								if(hasMovedFromInitialLocation(location)){
									Bukkit.getPluginManager().callEvent(new KillSkillCancelSpecTimeoutEvent(getAPi(),hcPlayer));
								}else{
									if(countdown > 0){
										getStringsApi()
											.get("killskill.command.spawn.countdown")
											.replace("%time%", String.valueOf(countdown))
											.sendChatP(hcPlayer);
										getSoundApi().play(hcPlayer, Sound.NOTE_STICKS, 0.5f, 2);
									}
								}
						
							}
							
							if(countdown == 0 && scheduler.isRunning()){
								getStringsApi()
									.get("killskill.command.spawn.teleported-to-spawn")
									.sendChatP(hcPlayer);
								
								player.teleport(getAPi().getWorldConfig().getLobby());
								player.setHealth(player.getMaxHealth());
								((KillSkillPlayer) hcPlayer).resetLastPVP();
								getSoundApi().play(hcPlayer, Sound.ENDERMAN_TELEPORT, 0.5f, 2);
							}
							
							countdown--;
							
							
						}

						private boolean hasMovedFromInitialLocation(Location loc) {
							return (Math.abs(loc.getX()-initialLocation.getX()) > 0.1 
									|| Math.abs(loc.getY() - initialLocation.getY()) > 0.1 
									|| Math.abs(loc.getZ() - initialLocation.getZ()) > 0.1);								
						}
						
					})
					.withIterations(4)
					.addListener(new HCTaskListener(){
						
						@EventHandler
						public void onCancelSpec(KillSkillCancelSpecTimeoutEvent e){
							if(e.getHcPlayer().equals(hcPlayer)){
								cancelSpawn(hcPlayer);
							}
						}
						

						@EventHandler
						public void onDamage(HCPlayerDamagedEvent e){
							if(hcPlayer.isOnline()){
								if(e.getHcDamaged().equals(hcPlayer)){
									cancelSpawn(hcPlayer);
								}
							}
						}
						
						@EventHandler
						public void onDamageOther(HCPlayerDamagedByHCPlayerEvent e){
							if(hcPlayer.isOnline()){
								if(e.getHcDamager().equals(hcPlayer)){
									cancelSpawn(hcPlayer);
								}
							}
						}
						
						@EventHandler
						public void onDamageOther(HCPlayerDamagedByPotionEvent e){
							if(hcPlayer.isOnline()){
								if(e.getHcDamager().equals(hcPlayer)){
									cancelSpawn(hcPlayer);
								}
							}
						}
						
						@EventHandler
						public void onGameStateChange(HCGameStateChangeEvent e){
							cancelSpawn(hcPlayer);
						}
						
						@EventHandler(priority = EventPriority.HIGHEST)
						public void onTypeCommandAgain(KillSkillTypeSpawnCommandEvent e){
							if(!e.isCancelled() && e.getKsPlayer().equals(hcPlayer)){
								getStringsApi()
									.get("killskill.command.spawn.already-waiting-for-teleportation")
									.sendChatP(hcPlayer);
								e.setCancelled(true);
							}
						}

						@EventHandler
						public void onPlayerStateGame(HCPlayerStateChangeEvent e){
							if(e.getHcPlayer().equals(hcPlayer)){
								cancelSpawn(hcPlayer);
							}
						}

						@EventHandler
						public void onQuit(HCPlayerQuitEvent e){
							if(e.getHcPlayer().equals(hcPlayer)){
								cancelSpawn(hcPlayer);
							}
						}
						
						private void cancelSpawn(HCPlayer hcPlayer){
							getScheduler().stop();
							getStringsApi()
								.get("killskill.command.spawn.cancel-countdown")
								.sendChatP(hcPlayer);
						}
						
					})
					.build()
					.start();

				}
				
			}
			
		}
		return true;
	}

	private class KillSkillCancelSpecTimeoutEvent extends HCEvent{

		private HCPlayer hcPlayer;

		public KillSkillCancelSpecTimeoutEvent(HCGameAPI api, HCPlayer hcPlayer) {
			super(api);
			this.hcPlayer = hcPlayer;
		}

		public HCPlayer getHcPlayer() {
			return hcPlayer;
		}
		
	}
}
