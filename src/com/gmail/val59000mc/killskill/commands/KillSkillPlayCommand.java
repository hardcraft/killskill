package com.gmail.val59000mc.killskill.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.players.PlayersManager;
import com.google.common.collect.Sets;

public class KillSkillPlayCommand extends HCCommand{

	public KillSkillPlayCommand(JavaPlugin plugin) {
		super(plugin, "play");
	}

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if(sender instanceof Player){
			
			Player player = (Player) sender;
			

			HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
			
			switch(getAPi().getGameState()){
				case CONTINUOUS:
					// allow play at this state
					break;
				default:
				 // no play at this state
				getStringsApi()
					.get("killskill.command.play.not-allowed-now")
					.sendChatP(hcPlayer);
				return true;
			}
			
			if(hcPlayer != null){
				
				if(hcPlayer.isAny(Sets.newHashSet(PlayerState.VANISHED,PlayerState.DEAD,PlayerState.SPECTATING))){
					if(getPmApi().getPlayersCount(true, PlayerState.PLAYING) < getPmApi().getMaxPlayers()){
						PlayersManager pm = (PlayersManager) getAPi().getPlayersManagerAPI();
						((PlayersManager)pm).relogPlayingPlayer(hcPlayer);
						pm.refreshVisiblePlayers(hcPlayer);
						pm.refreshRankNameTag(hcPlayer,0);
						player.teleport(getAPi().getWorldConfig().getLobby());
					}else{
						getStringsApi()
							.get("killskill.game-is-full")
							.sendChatP(hcPlayer);
					}
				}else{
					getStringsApi()
						.get("killskill.command.play.not-spectating")
						.sendChatP(hcPlayer);
				}
				
				return true;
				
			}
			
		}
		
		return true;
	}
}
