package com.gmail.val59000mc.killskill.stats;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import javax.sql.rowset.CachedRowSet;

import org.bukkit.Bukkit;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;

public class ScoreboardStats {

	private HCGameAPI api;
	
	private List<String> topMonth;
	private List<String> topOnline;
	private Map<KillSkillPlayer,List<String>> ownMonth;
	
	private String selectPlayerStatsByMonth;
	private String selectTopKillsByMonth;
	private String selectTopCoinsByMonth;
	
	public ScoreboardStats(HCGameAPI api) {
		this.api = api;
		this.topMonth = new ArrayList<String>();
		this.topOnline = new ArrayList<String>();
		this.ownMonth = new HashMap<KillSkillPlayer,List<String>>();
		
		HCMySQLAPI sql = api.getMySQLAPI();
		if(sql.isEnabled()){
			selectPlayerStatsByMonth = sql.readQueryFromResource(api.getPlugin(),"resources/sql/select_player_stats_by_month.sql");
			selectTopKillsByMonth = sql.readQueryFromResource(api.getPlugin(),"resources/sql/select_top_kills_by_month.sql");
			selectTopCoinsByMonth = sql.readQueryFromResource(api.getPlugin(),"resources/sql/select_top_coins_by_month.sql");
		}
		
		this.scoreboardOwn = api.getStringsAPI().get("killskill.scoreboard.own").toString();
		this.scoreboardKillsDeaths = api.getStringsAPI().get("killskill.scoreboard.kills-deaths").toString();
		this.scoreboardCoins = api.getStringsAPI().get("killskill.scoreboard.coins").toString();
		this.scoreboardExpLapis = api.getStringsAPI().get("killskill.scoreboard.exp-lapis").toString();
		this.scoreboardIronDiamond = api.getStringsAPI().get("killskill.scoreboard.iron-diamond").toString();
		this.scoreboardBlazeHead = api.getStringsAPI().get("killskill.scoreboard.blaze-head").toString();
		this.scoreboardTntGrenadeCannon = api.getStringsAPI().get("killskill.scoreboard.tnt-grenade-cannon").toString();
		
		this.scoreboardOwnMonth = api.getStringsAPI().get("killskill.scoreboard.own-month").toString();
		
		this.scoreboardTopMonth = api.getStringsAPI().get("killskill.scoreboard.top-month").toString();
		this.scoreboardKills = api.getStringsAPI().get("killskill.scoreboard.kills").toString();
		this.scoreboardEmpty = api.getStringsAPI().get("killskill.scoreboard.empty").toString();
		
		this.scoreboardTopOnline = api.getStringsAPI().get("killskill.scoreboard.top-online").toString();
	}

	public List<String> getMonthsStats(KillSkillPlayer ksPlayer) {
		// if not available, giving player own stats
		if(topMonth.isEmpty()){
			return getOwnStats(ksPlayer);
		}else{
			return topMonth;
		}
	}

	public List<String> getOnlineStats(KillSkillPlayer ksPlayer) {
		// if not available, giving player own stats
		if(topMonth.isEmpty()){
			return getOwnStats(ksPlayer);
		}else{
			return topOnline;
		}
	}
	
	private String scoreboardOwn;
	private String scoreboardKillsDeaths;
	private String scoreboardCoins;
	private String scoreboardExpLapis;
	private String scoreboardIronDiamond;
	private String scoreboardBlazeHead;
	private String scoreboardTntGrenadeCannon;

	public List<String> getOwnStats(KillSkillPlayer ksPlayer) {

		List<String> content = new ArrayList<String>();
		
		content.add(" ");
		content.add(scoreboardOwn);
		content.add(" ");
		
		// Kills
		content.add(scoreboardKillsDeaths);
		content.add(" §a"+ksPlayer.getKills()+"§f/§a"+ksPlayer.getDeaths());
		
		// Coins
		content.add(scoreboardCoins);
		content.add(" §a"+ksPlayer.getMoney());

		// Experience/Lapis
		content.add(scoreboardExpLapis);
		content.add(" §a"+ksPlayer.getExperienceLooted()+"§f/§a"+ksPlayer.getLapisLooted());

		// Iron/Diamond
		content.add(scoreboardIronDiamond);
		content.add(" §a"+ksPlayer.getIronLooted()+"§f/§a"+ksPlayer.getDiamondLooted());

		// Blaze/Head
		content.add(scoreboardBlazeHead);
		content.add(" §a"+ksPlayer.getBlazeLooted()+"§f/§a"+ksPlayer.getHeadLooted());

		// TNT/Grenade/Cannon
		content.add(scoreboardTntGrenadeCannon);
		content.add(" §a"+ksPlayer.getTntLooted()+"§f/§a"+ksPlayer.getGrenadeLooted()+"§f/§a"+ksPlayer.getCannonLooted());
		return content;
		
	}

	public List<String> getOwnMonthStats(KillSkillPlayer ksPlayer){
		if(ownMonth.containsKey(ksPlayer)){
			return ownMonth.get(ksPlayer);
		}else{
			// if not available, fiving own stats and waiting for next round
			return getOwnStats(ksPlayer);
		}
	}

	public void update(ScoreboardType next) {
		switch(next){
			case MONTH_STATS:
				cacheTopMonth();
				break;
			case ONLINE_STATS:
				// cache online stats
				cacheTopOnline();
				break;
			case OWN_STATS:
				// nothing to update and cache
				break;
			case OWN_MONTH_STATS:
				cacheOwnMonth();
				break;
			default:
				break;
		}
		
	}

	
	private String scoreboardOwnMonth;
	
	private void cacheOwnMonth() {

		if(api.getMySQLAPI().isEnabled()){
			Bukkit.getScheduler().runTaskAsynchronously(api.getPlugin(), new Runnable() {
				
				@Override
				public void run() {
				
					HCMySQLAPI sql = api.getMySQLAPI();
					
					String month = new SimpleDateFormat("MMMM", new Locale("fr")).format(new Date());
					
					HashMap<KillSkillPlayer,List<String>> stats = new HashMap<KillSkillPlayer,List<String>>();
					for(HCPlayer hcPlayer : api.getPlayersManagerAPI().getPlayers(true, null)){
						if(hcPlayer.getId() != 0){

							KillSkillPlayer ksPlayer = (KillSkillPlayer) hcPlayer;
							
							try {
								
								CachedRowSet result = sql.query(sql.prepareStatement(selectPlayerStatsByMonth,
									String.valueOf(hcPlayer.getId()),
									sql.getMonth(),
									sql.getYear()
									)
								);
							
								if(result.first()){
									List<String> content = new ArrayList<String>();
									
									content.add(" ");
									content.add(scoreboardOwnMonth.replace("%month%", month));
									content.add(" ");
									
									// Kills
									content.add(scoreboardKillsDeaths);
									content.add(" §a"+result.getInt("kills")+"§f/§a"+result.getInt("deaths"));
									
									// Coins
									content.add(scoreboardCoins);
									content.add(" §a"+result.getDouble("coins_earned"));
		
									// Experience/Lapis
									content.add(scoreboardExpLapis);
									content.add(" §a"+result.getInt("experience_looted")+"§f/§a"+result.getInt("lapis_looted"));
		
									// Iron/Diamond
									content.add(scoreboardIronDiamond);
									content.add(" §a"+result.getInt("iron_looted")+"§f/§a"+result.getInt("diamond_looted"));
		
									// Blaze/Head
									content.add(scoreboardBlazeHead);
									content.add(" §a"+result.getInt("blaze_looted")+"§f/§a"+result.getInt("head_looted"));
		
									// TNT/Grenade/Cannon
									content.add(scoreboardTntGrenadeCannon);
									content.add(" §a"+result.getInt("tnt_looted")+"§f/§a"+result.getInt("grenade_looted")+"§f/§a"+result.getInt("cannon_looted"));
									
									stats.put(ksPlayer, content);
								}
								
								synchronized(ownMonth){
									ownMonth.clear();
									ownMonth.putAll(stats);
								}

							} catch (SQLException e) {
								e.printStackTrace();
								Log.severe("Couldn't read statistics for player "+ksPlayer.getName()+" Read error above.");
							}
						}
					}
					
				}
			});
		}
		
		
	}

	private String scoreboardTopMonth;
	private String scoreboardEmpty;
	
	private void cacheTopMonth() {
		
		if(api.getMySQLAPI().isEnabled()){
			Bukkit.getScheduler().runTaskAsynchronously(api.getPlugin(), new Runnable() {
				
				@Override
				public void run() {
				
					HCMySQLAPI sql = api.getMySQLAPI();
					
					String month = new SimpleDateFormat("MMMM", new Locale("fr")).format(new Date());
					
					HashMap<KillSkillPlayer,List<String>> stats = new HashMap<KillSkillPlayer,List<String>>();

					try {	
						CachedRowSet resultKills = sql.query(sql.prepareStatement(selectTopKillsByMonth,
							sql.getMonth(),
							sql.getYear()
							)
						);
	
						CachedRowSet resultCoins = sql.query(sql.prepareStatement(selectTopCoinsByMonth,
							sql.getMonth(),
							sql.getYear()
							)
						);
	
						List<String> content = new ArrayList<String>();
						content.add(" ");
						content.add(scoreboardTopMonth.replace("%month%", month));
						content.add(" ");
						
						List<String> contentKills = new ArrayList<String>();
						contentKills.add(scoreboardKills);
						
						List<String> contentCoins = new ArrayList<String>();
						contentCoins.add(scoreboardCoins);
					
						boolean continueKills = resultKills.first();
						boolean continueCoins = resultCoins.first();
						
						
						for(int i=0 ; i<5 ; i++){
							if(continueKills){
								String highscoreKills = " §a"+resultKills.getInt("kills")+" §7: §f"+resultKills.getString("name");
								contentKills.add(highscoreKills);
								continueKills = resultKills.next();
							}else{
								contentKills.add(scoreboardEmpty);
							}
							
							if(continueCoins){
								String highscoreCoins = " §a"+resultCoins.getDouble("coins_earned")+" §7: §f"+resultCoins.getString("name");
								contentCoins.add(highscoreCoins);
								continueCoins = resultCoins.next();
							}else{
								contentCoins.add(scoreboardEmpty);
							}
						}

						
						content.addAll(contentKills);
						content.addAll(contentCoins);
						
						synchronized(topMonth){
							topMonth.clear();
							topMonth.addAll(content);
						}
						
					} catch (SQLException e) {
						e.printStackTrace();
						Log.severe("Couldn't read top month statistics. Read error above");
					}
					
				}
			});
		}

		
	}

	private String scoreboardTopOnline;
	private String scoreboardKills;
	
	private void cacheTopOnline() {
		List<String> stats = new ArrayList<String>();

		stats.add(" ");
		stats.add(scoreboardTopOnline);
		stats.add(" ");
		
		// top kills
		stats.add(scoreboardKills);
		List<HCPlayer> topKills = api.getPlayersManagerAPI()
			.getPlayers(true, PlayerState.PLAYING)
			.stream()
			.sorted((x,y) -> Integer.compare(x.getKills(),y.getKills()))
			.collect(Collectors.toList());
		for(int i=0 ; i<5 ; i++){
			if(topKills.size()>i){
				HCPlayer hcPlayer = topKills.get(topKills.size()-i-1);
				String highscoreKills = " §a"+hcPlayer.getKills()+" §7: §f"+hcPlayer.getName();
				stats.add(highscoreKills);
			}else{
				stats.add(scoreboardEmpty);
			}
		}
		
		// top coins
		stats.add(scoreboardCoins);
		List<HCPlayer> topCoins = api.getPlayersManagerAPI()
				.getPlayers(true, PlayerState.PLAYING)
				.stream()
				.sorted((x,y) -> Double.compare(x.getMoney(),y.getMoney()))
				.collect(Collectors.toList());
		for(int i=0 ; i<5 ; i++){
			if(topCoins.size()>i){
				HCPlayer hcPlayer = topCoins.get(topCoins.size()-i-1);
				String highscoreCoins = " §a"+hcPlayer.getMoney()+" §7: §f"+hcPlayer.getName();
				stats.add(highscoreCoins);
			}else{
				stats.add(scoreboardEmpty);
			}
		}


		synchronized(topOnline){
			this.topOnline.clear();
			this.topOnline.addAll(stats);
		}
		
	}

}
