package com.gmail.val59000mc.killskill.stats;

public enum ScoreboardType {
	OWN_STATS,
	ONLINE_STATS,
	OWN_MONTH_STATS,
	MONTH_STATS;
	
	public ScoreboardType next(){
		return values()[(ordinal() + 1) % values().length];
	}
}
