package com.gmail.val59000mc.killskill.bonuses.executors;

import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;

import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.hcgameslib.tasks.HCTaskScheduler;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.gmail.val59000mc.spigotutils.particles.ParticleEffect;
import com.gmail.val59000mc.spigotutils.particles.ParticleEffect.BlockData;

public abstract class AbsractSpecialPortalBonus extends AbstractPortalBonus{

	public AbsractSpecialPortalBonus(Block portal, KillSkillPlayer target) {
		super(portal, target);
	}

	protected HCTaskScheduler buildTask(String name, byte woolColor) {
		return getApi().buildTask(name, new HCTask() {
			
			@Override
			public void run() {

				// play explosion effect
				Location tntLoc = location.clone().add(Randoms.randomDouble(-5, 5), Randoms.randomDouble(0, 5), Randoms.randomDouble(-5, 5));
				location.getWorld().spigot().playEffect(tntLoc, Effect.EXPLOSION_HUGE);
				getSoundApi().play(Sound.EXPLODE, tntLoc, 2, 1);
				for(int i=0 ; i<20 ; i++){
					Location particleLoc = location.clone().add(Randoms.randomDouble(-5, 5), Randoms.randomDouble(0, 5), Randoms.randomDouble(-5, 5));
					ParticleEffect.BLOCK_CRACK.display(new BlockData(Material.WOOL, (byte) woolColor), 1F, 1F, 1F, 1F, 3, particleLoc, 30);
				}
			}
		})
		.withInterval(4)
		.withIterations(30)
		.withLastCallback(new HCTask() {
			
			@Override
			public void run() {
				getSoundApi().play(Sound.WITHER_DEATH, 1, 0);
				AbsractSpecialPortalBonus.this.stop();
			}
		})
		.build();
	}
	
}
