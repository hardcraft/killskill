package com.gmail.val59000mc.killskill.bonuses.executors;

import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.killskill.items.KillSkillItems;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Randoms;

import net.md_5.bungee.api.ChatColor;

public class TrollPortalBonus extends AbsractSpecialPortalBonus{

	public TrollPortalBonus(Block portal, KillSkillPlayer target) {
		super(portal, target);
	}

	@Override
	public void startBonus() {
		//display player title
		getStringsApi()
			.get("killskill.portal-bonus.bonus-activated")
			.prepend(ChatColor.DARK_RED+"")
			.sendTitle(target, 10, 25, 10);
				
		getStringsApi()
			.get("killskill.portal-bonus.troll")
			.replace("%player%", target.getName())
			.sendChatP();
		
		getSoundApi().play(Sound.ZOMBIE_PIG_ANGRY, 2, 0);
		getSoundApi().play(Sound.ZOMBIE_INFECT, 2, 0);
		getSoundApi().play(Sound.ZOMBIE_WOODBREAK, 2, 0);
		getSoundApi().play(Sound.ZOMBIE_WOOD, 2, 0);
		
		if(target.isOnline()){
			Player player = target.getPlayer();
			// give effect (20s) 
			Effects.add(player, PotionEffectType.INCREASE_DAMAGE, 400, 1);
			Effects.add(player, PotionEffectType.SPEED, 400, 1);
			Effects.add(player, PotionEffectType.DAMAGE_RESISTANCE, 400, 1);
			Effects.add(player, PotionEffectType.REGENERATION, 400, 1);
			
			// give items
			int amount = Randoms.randomInteger(2, 5);
			ItemStack tnt = KillSkillItems.getTntItem(amount);
			player.getInventory().addItem(tnt);
			target.addTntLooted(amount);
			
			getStringsApi()
				.get("killskill.portal-bonus.received-tnt-item")
				.replace("%amount%", String.valueOf(amount))
				.sendActionBar(target,10);
		}
		
		buildTask("troll portal bonus", (byte) 14).start();
	}	

	@Override
	public void stopBonus() {
		// TODO Auto-generated method stub
		
	}

}
