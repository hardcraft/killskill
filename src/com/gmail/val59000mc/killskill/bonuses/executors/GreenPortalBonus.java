package com.gmail.val59000mc.killskill.bonuses.executors;

import org.bukkit.Color;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.spigotutils.Effects;

import net.md_5.bungee.api.ChatColor;

public class GreenPortalBonus extends AbsractNormalPortalBonus{

	public GreenPortalBonus(Block portal, KillSkillPlayer target) {
		super(portal, target);
	}

	@Override
	public void startBonus() {
		//display player title
		getStringsApi()
			.get("killskill.portal-bonus.bonus-activated")
			.prepend(ChatColor.GREEN+"")
			.sendTitle(target, 10, 25, 10);
		
		// broadcast message
		getStringsApi()
			.get("killskill.portal-bonus.green")
			.replace("%player%", target.getName())
			.sendChatP();

		// play sound
		getSoundApi().play(Sound.WITHER_SPAWN, 1, 0);
		
		// give effect 
		if(target.isOnline()){
			Effects.add(target.getPlayer(), PotionEffectType.REGENERATION, 3600, 1);
			getStringsApi()
				.get("killskill.portal-bonus.received-regeneration-effect")
				.sendActionBar(target,10);
		}
		
		buildTask("green portal bonus", Color.GREEN).start();
	}

	@Override
	public void stopBonus() {
		// TODO Auto-generated method stub
		
	}

}
