package com.gmail.val59000mc.killskill.bonuses.executors;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.hcgameslib.tasks.HCTaskScheduler;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.spigotutils.Randoms;

public abstract class AbsractNormalPortalBonus extends AbstractPortalBonus{

	public AbsractNormalPortalBonus(Block portal, KillSkillPlayer target) {
		super(portal, target);
	}

	
	protected void detonateFireworkAtLocation(Location location, Color bukkitColor) {
		Firework fw = (Firework) location.getWorld().spawnEntity(location, EntityType.FIREWORK);
		FireworkMeta fwm = fw.getFireworkMeta();
		fwm.setPower(0);
		fwm.addEffect(FireworkEffect.builder()
			.withColor(bukkitColor)
			.withColor(Color.WHITE)
			.withFade(bukkitColor)
			.with(Type.BALL)
			.build()
		);
		fw.setFireworkMeta(fwm);
		Bukkit.getScheduler().runTaskLater(getApi().getPlugin(), new Runnable() {
			public void run() {
				fw.detonate();
			}
		}, 2);
	}
	
	protected HCTaskScheduler buildTask(String name, Color color){
		return getApi().buildTask(name, new HCTask() {
			
			private float pitch = 0;
			private double fireworkOffset = 0;
			
			@Override
			public void run() {
				getSoundApi().play(Sound.ENDERDRAGON_WINGS, 1, pitch);
				pitch += 0.12;
				location.getWorld().strikeLightningEffect(location.clone().add(Randoms.randomDouble(-3, 3), 0, Randoms.randomDouble(-3, 3)));
				detonateFireworkAtLocation(location.clone().add(0, fireworkOffset, 0), color);
				fireworkOffset += 4;

			}
		})
		.withInterval(4)
		.withIterations(30)
		.withLastCallback(new HCTask() {
			
			@Override
			public void run() {
				getSoundApi().play(Sound.WITHER_DEATH, 1, 0);
				AbsractNormalPortalBonus.this.stop();
			}
		})
		.build();
	}
	
}
