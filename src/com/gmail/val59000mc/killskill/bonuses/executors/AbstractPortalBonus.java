package com.gmail.val59000mc.killskill.bonuses.executors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.event.EventHandler;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.events.HCGameStateChangeEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerKilledByPlayerEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerKilledEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerQuitEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerStateChangeEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.killskill.events.KillSkillStartPortalBonusEvent;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.spigotutils.Randoms;

public abstract class AbstractPortalBonus extends HCListener{
	
	protected Block portal;
	protected Location location;
	protected KillSkillPlayer target;
	private Boolean running;
	
	public AbstractPortalBonus(Block portal, KillSkillPlayer target){
		this.portal = portal;
		this.location = portal.getLocation();
		this.target = target;
		this.running = null;
	}
	
	protected void stop(){
		if(running == true){
			this.running = false;
			this.stopBonus();
			getApi().unregisterListener(this);
			
			//give reward
			int randomReward = Randoms.randomInteger(25, 75);
			getPmApi().rewardMoneyTo(target, randomReward);
		}
	}
	
	public void start(HCGameAPI api){
		if(running == null){
			this.running = true;
			api.registerListener(this);
			Bukkit.getScheduler().runTaskLater(api.getPlugin(), new Runnable() {
				
				@Override
				public void run() {					
					startBonus();
				}
			}, 1);
		}
	}
	
	protected abstract void startBonus();
	
	protected abstract void stopBonus();
	
	@EventHandler
	public void onDisconnect(HCPlayerQuitEvent e){
		if(e.getHcPlayer().equals(target)){
			this.stop();
		}
	}
	
	@EventHandler
	public void onChangePlayerState(HCPlayerStateChangeEvent e){
		if(e.getHcPlayer().equals(target)){
			this.stop();
		}
	}

	@EventHandler
	public void onChangeGameState(HCGameStateChangeEvent e){
		this.stop();
	}


	@EventHandler
	public void onDeath(HCPlayerKilledEvent e){
		if(e.getHcPlayer().equals(target)){
			this.stop();
		}
	}

	@EventHandler
	public void onDeath(HCPlayerKilledByPlayerEvent e){
		if(e.getHCKilled().equals(target)){
			this.stop();
		}
	}

	@EventHandler
	public void onTriggersameBonus(KillSkillStartPortalBonusEvent e){
		// no player can trigger the same bonus while it is still playing
		if(e.getPortal().equals(portal)){
			e.setCancelled(true);
		}
	}
}
