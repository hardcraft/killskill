package com.gmail.val59000mc.killskill.bonuses.executors;

import org.bukkit.Color;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.spigotutils.Effects;

import net.md_5.bungee.api.ChatColor;

public class RedPortalBonus extends AbsractNormalPortalBonus{
	
	public RedPortalBonus(Block portal, KillSkillPlayer target) {
		super(portal, target);
	}

	@Override
	public void startBonus() {

		//display player title
		getStringsApi()
			.get("killskill.portal-bonus.bonus-activated")
			.prepend(ChatColor.RED+"")
			.sendTitle(target, 10, 25, 10);
		
		// broadcast message
		getStringsApi()
			.get("killskill.portal-bonus.red")
			.replace("%player%", target.getName())
			.sendChatP();

		// play sound
		getSoundApi().play(Sound.WITHER_SPAWN, 1, 0);
		
		// give effect 
		if(target.isOnline()){
			Effects.add(target.getPlayer(), PotionEffectType.INCREASE_DAMAGE, 3600, 1);
			getStringsApi()
				.get("killskill.portal-bonus.received-strength-effect")
				.sendActionBar(target,10);
		}
		
		buildTask("red portal bonus", Color.RED).start();
		
	}

	@Override
	public void stopBonus() {
		// TODO Auto-generated method stub
	}

}
