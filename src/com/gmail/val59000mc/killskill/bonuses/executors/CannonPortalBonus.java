package com.gmail.val59000mc.killskill.bonuses.executors;

import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.killskill.items.KillSkillItems;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.spigotutils.Effects;
import com.gmail.val59000mc.spigotutils.Randoms;

import net.md_5.bungee.api.ChatColor;

public class CannonPortalBonus extends AbsractSpecialPortalBonus{

	public CannonPortalBonus(Block portal, KillSkillPlayer target) {
		super(portal, target);
	}

	@Override
	public void startBonus() {
		//display player title
		getStringsApi()
			.get("killskill.portal-bonus.bonus-activated")
			.prepend(ChatColor.DARK_BLUE+"")
			.sendTitle(target, 10, 25, 10);
				
		getStringsApi()
			.get("killskill.portal-bonus.cannon")
			.replace("%player%", target.getName())
			.sendChatP();
		
		getSoundApi().play(Sound.EXPLODE, 2, 0);
		
		if(target.isOnline()){
			Player player = target.getPlayer();
			// give effect (20s) 
			Effects.add(player, PotionEffectType.INCREASE_DAMAGE, 400, 1);
			Effects.add(player, PotionEffectType.SPEED, 400, 1);
			Effects.add(player, PotionEffectType.DAMAGE_RESISTANCE, 400, 1);
			Effects.add(player, PotionEffectType.REGENERATION, 400, 1);
			
			// give items
			int amount = Randoms.randomInteger(2, 5);
			ItemStack cannon = KillSkillItems.getCanonItem(amount);
			player.getInventory().addItem(cannon);
			target.addCannonLooted(amount);
			
			getStringsApi()
				.get("killskill.portal-bonus.received-cannon-item")
				.replace("%amount%", String.valueOf(amount))
				.sendActionBar(target,10);
		}
		
		buildTask("cannon portal bonus", (byte) 11).start();
	}

	@Override
	public void stopBonus() {
		// TODO Auto-generated method stub
		
	}

}
