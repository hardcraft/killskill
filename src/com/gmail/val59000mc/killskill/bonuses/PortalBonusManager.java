package com.gmail.val59000mc.killskill.bonuses;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.game.GameState;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.killskill.bonuses.executors.AbstractPortalBonus;
import com.gmail.val59000mc.killskill.events.KillSkillStartPortalBonusEvent;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;

public class PortalBonusManager {

	private HCGameAPI api;
	
	public PortalBonusManager(HCGameAPI api) {
		this.api = api;
	}

	public boolean startPortalBonus(KillSkillPlayer ksPlayer, Block portal) {
		if(ksPlayer == null || !api.is(GameState.CONTINUOUS) || !ksPlayer.is(true, PlayerState.PLAYING)){
			return false;
		}else{
			KillSkillStartPortalBonusEvent event = new KillSkillStartPortalBonusEvent(api, portal, ksPlayer);
			Bukkit.getPluginManager().callEvent(event); // event may be cancelled if another bonus is running for the same player
			if(event.isCancelled()){
				api.getStringsAPI()
					.get("killskill.portal-bonus.cannot-use-bonus-now")
					.sendChat(ksPlayer);
				return false;
			}else{
				AbstractPortalBonus bonus = findPortalBonusType(portal).newPortalBonus(portal, ksPlayer);
				bonus.start(api);
				return true;
			}
		}
	}

	private PortalBonusType findPortalBonusType(Block portal) {
		
		// check is special portal bonus
		if(portal.getRelative(BlockFace.NORTH).getType().equals(Material.WOOL)
			&& portal.getRelative(BlockFace.EAST).getType().equals(Material.WOOL)
			&& portal.getRelative(BlockFace.SOUTH).getType().equals(Material.WOOL)
			&& portal.getRelative(BlockFace.WEST).getType().equals(Material.WOOL)){
			return findPortalSpecialBonusTypeFromConfig();
		}else{
			// check color of normal portal bonus
			return findNormalPortalBonusTypeFromStainedGlass(portal);
		}
		
	}


	private PortalBonusType findPortalSpecialBonusTypeFromConfig(){
		try{
			return PortalBonusType.valueOf(api.getConfig().getString("special-portal-bonus-type").toString());
		}catch(IllegalArgumentException e){
			return PortalBonusType.LION;
		}
	}


	@SuppressWarnings("deprecation")
	private PortalBonusType findNormalPortalBonusTypeFromStainedGlass(Block portal){
		Block beneath = portal;
		while(beneath.getY() > 0 && !beneath.getType().equals(Material.STAINED_GLASS)){
			beneath = beneath.getRelative(BlockFace.DOWN);
		}
		if(beneath.getType().equals(Material.STAINED_GLASS)){
			
			switch(beneath.getState().getData().getData()){
				case 5:
				case 13:
					return PortalBonusType.GREEN;
				case 4:
					return PortalBonusType.YELLOW;
				case 3:
				case 9:
				case 11:
					return PortalBonusType.BLUE;
				default:
				case (byte) 14:
					return PortalBonusType.RED;
			}

		}else{
			return PortalBonusType.RED;
		}
	}

}
