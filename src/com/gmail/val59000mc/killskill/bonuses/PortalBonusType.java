package com.gmail.val59000mc.killskill.bonuses;

import org.bukkit.block.Block;

import com.gmail.val59000mc.killskill.bonuses.executors.AbstractPortalBonus;
import com.gmail.val59000mc.killskill.bonuses.executors.BluePortalBonus;
import com.gmail.val59000mc.killskill.bonuses.executors.CannonPortalBonus;
import com.gmail.val59000mc.killskill.bonuses.executors.GreenPortalBonus;
import com.gmail.val59000mc.killskill.bonuses.executors.LionPortalBonus;
import com.gmail.val59000mc.killskill.bonuses.executors.RedPortalBonus;
import com.gmail.val59000mc.killskill.bonuses.executors.TrollPortalBonus;
import com.gmail.val59000mc.killskill.bonuses.executors.YellowPortalBonus;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;

public enum PortalBonusType {
	RED{
		@Override
		public AbstractPortalBonus newPortalBonus(Block portal, KillSkillPlayer target) {
			return new RedPortalBonus(portal, target);
		}
	},
	BLUE{
		@Override
		public AbstractPortalBonus newPortalBonus(Block portal, KillSkillPlayer target) {
			return new BluePortalBonus(portal, target);
		}
	},
	YELLOW{
		@Override
		public AbstractPortalBonus newPortalBonus(Block portal, KillSkillPlayer target) {
			return new YellowPortalBonus(portal, target);
		}
	},
	GREEN{
		@Override
		public AbstractPortalBonus newPortalBonus(Block portal, KillSkillPlayer target) {
			return new GreenPortalBonus(portal, target);
		}
	},
	TROLL{
		@Override
		public AbstractPortalBonus newPortalBonus(Block portal, KillSkillPlayer target) {
			return new TrollPortalBonus(portal, target);
		}
	},
	LION{
		@Override
		public AbstractPortalBonus newPortalBonus(Block portal, KillSkillPlayer target) {
			return new LionPortalBonus(portal, target);
		}
	},
	CANNON{
		@Override
		public AbstractPortalBonus newPortalBonus(Block portal, KillSkillPlayer target) {
			return new CannonPortalBonus(portal, target);
		}
	};

	public abstract AbstractPortalBonus newPortalBonus(Block portal, KillSkillPlayer target);
	
}
