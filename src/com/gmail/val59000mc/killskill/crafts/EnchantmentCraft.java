package com.gmail.val59000mc.killskill.crafts;

import org.bukkit.enchantments.Enchantment;

public class EnchantmentCraft {
	private Enchantment enchantement;
	private Integer level;
	public EnchantmentCraft(Enchantment enchantement, Integer level) {
		super();
		this.enchantement = enchantement;
		this.level = level;
	}
	public Enchantment getEnchantement() {
		return enchantement;
	}
	public Integer getLevel() {
		return level;
	}
	
}
