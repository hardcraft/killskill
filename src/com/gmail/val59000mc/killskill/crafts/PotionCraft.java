package com.gmail.val59000mc.killskill.crafts;

import org.bukkit.potion.PotionEffectType;

public class PotionCraft {
	private PotionEffectType type;
	private Integer amplifier;
	private Integer duration;
	private boolean throwable;
	public PotionCraft(PotionEffectType type, Integer amplifier, Integer duration, boolean throwable) {
		super();
		this.type = type;
		this.amplifier = amplifier;
		this.duration = 20*duration;
		this.throwable = throwable;
	}
	public PotionEffectType getType() {
		return type;
	}
	public Integer getAmplifier() {
		return amplifier;
	}
	public Integer getDuration() {
		return duration;
	}
	public boolean isThrowable() {
		return throwable;
	}	
}
