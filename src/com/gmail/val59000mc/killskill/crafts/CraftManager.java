package com.gmail.val59000mc.killskill.crafts;

import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.Recipe;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.ShapelessRecipe;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.ItemCompareOption;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.gmail.val59000mc.spigotutils.items.ItemBuilder;
import com.google.common.collect.Lists;

import net.md_5.bungee.api.ChatColor;

public class CraftManager {

	private HCGameAPI api;
	
	// enchantment and potions effects (key=quality, value=enchant or effect)
	private Map<Integer,List<EnchantmentCraft>> enchantments;
	private Map<Integer,List<PotionCraft>> effects;
	
	// chance weights
	private List<Integer> ironBookWeights;
	private List<Integer> diamondBookWeights;
	private List<Integer> ironPotionWeights;
	private List<Integer> diamondPotionWeights;
	
	// reusable names
	private String ironBookName;
	private List<String> ironBookLore;
	private ItemStack ironBook;
	
	private String diamondBookName;
	private List<String> diamondBookLore;
	private ItemStack diamondBook;
	
	private String ironPotionName;
	private List<String> ironPotionLore;
	private ItemStack ironPotion;
	
	private String diamondPotionName;
	private List<String> diamondPotionLore;
	private ItemStack diamondPotion;
	
	private Map<Integer,String> romanIntegers;


	public CraftManager(HCGameAPI api) {
		super();
		this.api = api;
		
		// PROTECTION_ENVIRONMENTAL
		// THORNS
		// DEPTH_STRIDER
		// DAMAGE_ALL
		// KNOCKBACK
		// FIRE_ASPECT
		// DURABILITY
		// ARROW_DAMAGE
		// ARROW_KNOCKBACK
		// ARROW_FIRE
		// ARROW_INFINITE
		
		this.enchantments = new HashMap<Integer, List<EnchantmentCraft>>();
		
		this.enchantments.put(0, Lists.newArrayList(
			new EnchantmentCraft(Enchantment.DURABILITY, 2),
			new EnchantmentCraft(Enchantment.DEPTH_STRIDER, 2),
			new EnchantmentCraft(Enchantment.PROTECTION_ENVIRONMENTAL, 2),
			new EnchantmentCraft(Enchantment.DAMAGE_ALL, 2),
			new EnchantmentCraft(Enchantment.KNOCKBACK, 2),
			new EnchantmentCraft(Enchantment.ARROW_DAMAGE, 2),
			new EnchantmentCraft(Enchantment.THORNS, 2)
		));
		this.enchantments.put(1, Lists.newArrayList(
			new EnchantmentCraft(Enchantment.PROTECTION_ENVIRONMENTAL, 3),
			new EnchantmentCraft(Enchantment.FIRE_ASPECT, 1),
			new EnchantmentCraft(Enchantment.DAMAGE_ALL, 3),
			new EnchantmentCraft(Enchantment.ARROW_KNOCKBACK, 2),
			new EnchantmentCraft(Enchantment.ARROW_DAMAGE, 3)
		));
		this.enchantments.put(2, Lists.newArrayList(
			new EnchantmentCraft(Enchantment.DAMAGE_ALL, 4),
			 new EnchantmentCraft(Enchantment.DURABILITY, 3),
			new EnchantmentCraft(Enchantment.ARROW_DAMAGE, 4),
			new EnchantmentCraft(Enchantment.DEPTH_STRIDER, 3),
			new EnchantmentCraft(Enchantment.THORNS, 3),
			new EnchantmentCraft(Enchantment.ARROW_INFINITE, 1)
		));
		this.enchantments.put(3, Lists.newArrayList(
			new EnchantmentCraft(Enchantment.PROTECTION_ENVIRONMENTAL, 4),
			new EnchantmentCraft(Enchantment.DAMAGE_ALL, 5),
			new EnchantmentCraft(Enchantment.ARROW_DAMAGE, 5),
			new EnchantmentCraft(Enchantment.FIRE_ASPECT, 2),
			new EnchantmentCraft(Enchantment.ARROW_FIRE, 1)
		));
		
		// book weights
		ironBookWeights = Lists.newArrayList(
			0,0,0,0,
			1,1,1,
			2,2,
			3
		);
		Collections.shuffle(ironBookWeights);
		
		diamondBookWeights = Lists.newArrayList(
			0,
			1,1,
			2,2,2,
			3,3
		);
		Collections.shuffle(diamondBookWeights);
		
		// SPEED
		// INCREASE_DAMAGE
		// HEAL
		// HARM
		// JUMP
		// BLINDNESS
		// CONFUSION
		// REGENERATION
		// DAMAGE_RESISTANCE
		// FIRE_RESISTANCE
		// WEAKNESS
		// POISON
		// WITHER
		// HEALTH_BOOST
		
		this.effects = new HashMap<Integer, List<PotionCraft>>();
		
		this.effects.put(0, Lists.newArrayList(
			new PotionCraft(PotionEffectType.SPEED, 0, 90, false),
			new PotionCraft(PotionEffectType.FIRE_RESISTANCE, 0, 90, false),
			new PotionCraft(PotionEffectType.JUMP, 0, 90, false),
			new PotionCraft(PotionEffectType.DAMAGE_RESISTANCE, 0, 90, false),
			new PotionCraft(PotionEffectType.HEALTH_BOOST, 1, 90, false)
		));
		this.effects.put(1, Lists.newArrayList(
			new PotionCraft(PotionEffectType.SPEED, 1, 90, true),
			new PotionCraft(PotionEffectType.INCREASE_DAMAGE, 0, 90, false),
			new PotionCraft(PotionEffectType.HEAL, 0, 0, false),
			new PotionCraft(PotionEffectType.HARM, 0, 0, true),
			new PotionCraft(PotionEffectType.POISON, 0, 15, true),
			new PotionCraft(PotionEffectType.JUMP, 0, 90, false),
			new PotionCraft(PotionEffectType.REGENERATION, 0, 20, false),
			new PotionCraft(PotionEffectType.HEALTH_BOOST, 3, 90, false)
		));
		this.effects.put(2, Lists.newArrayList(
			new PotionCraft(PotionEffectType.WEAKNESS, 0, 45, true),
			new PotionCraft(PotionEffectType.CONFUSION, 1, 15, true),
			new PotionCraft(PotionEffectType.BLINDNESS, 0, 8, true),
			new PotionCraft(PotionEffectType.DAMAGE_RESISTANCE, 1, 90, false),
			new PotionCraft(PotionEffectType.HEAL, 0, 0, true),
			new PotionCraft(PotionEffectType.POISON, 0, 40, true),
			new PotionCraft(PotionEffectType.FIRE_RESISTANCE, 0, 300, false),
			new PotionCraft(PotionEffectType.HEALTH_BOOST, 5, 90, false),
			new PotionCraft(PotionEffectType.REGENERATION, 1, 30, false),
			new PotionCraft(PotionEffectType.JUMP, 1, 90, false)
		));
		this.effects.put(3, Lists.newArrayList(
			new PotionCraft(PotionEffectType.SPEED, 1, 90, true),
			new PotionCraft(PotionEffectType.HEAL, 1, 0, true),
			new PotionCraft(PotionEffectType.HARM, 1, 0, true),
			new PotionCraft(PotionEffectType.POISON, 1, 20, true),
			new PotionCraft(PotionEffectType.WITHER, 0, 20, true),
			new PotionCraft(PotionEffectType.HEALTH_BOOST, 7, 90, false),
			new PotionCraft(PotionEffectType.BLINDNESS, 0, 15, true),
			new PotionCraft(PotionEffectType.DAMAGE_RESISTANCE, 2, 180, false),
			new PotionCraft(PotionEffectType.CONFUSION, 2, 45, true),
			new PotionCraft(PotionEffectType.JUMP, 2, 90, false)
		));
		this.effects.put(4, Lists.newArrayList(
			new PotionCraft(PotionEffectType.SPEED, 1, 180, true),
			new PotionCraft(PotionEffectType.INCREASE_DAMAGE, 1, 90, false),
			new PotionCraft(PotionEffectType.HARM, 2, 0, true),
			new PotionCraft(PotionEffectType.HEAL, 2, 0, true),
			new PotionCraft(PotionEffectType.WEAKNESS, 2, 90, true),
			new PotionCraft(PotionEffectType.POISON, 1, 30, true),
			new PotionCraft(PotionEffectType.WITHER, 1, 30, true),
			new PotionCraft(PotionEffectType.DAMAGE_RESISTANCE, 3, 90, false),
			new PotionCraft(PotionEffectType.BLINDNESS, 0, 30, true),
			new PotionCraft(PotionEffectType.REGENERATION, 2, 45, false)
		));
		this.effects.put(5, Lists.newArrayList(
			new PotionCraft(PotionEffectType.SPEED, 3, 45, true),
			new PotionCraft(PotionEffectType.INCREASE_DAMAGE, 3, 45, true),
			new PotionCraft(PotionEffectType.HEALTH_BOOST, 11, 90, false),
			new PotionCraft(PotionEffectType.DAMAGE_RESISTANCE, 5, 90, false),
			new PotionCraft(PotionEffectType.REGENERATION, 3, 45, false),
			new PotionCraft(PotionEffectType.JUMP, 4, 90, false),
			new PotionCraft(PotionEffectType.BLINDNESS, 0, 45, true),
			new PotionCraft(PotionEffectType.POISON, 3, 20, true),
			new PotionCraft(PotionEffectType.HARM, 3, 0, true),
			new PotionCraft(PotionEffectType.HEAL, 3, 0, true)
		));
		

		// book weights
		ironPotionWeights = Lists.newArrayList(
			0,0,0,0,0,0,
			1,1,1,1,1,
			2,2,2,2,
			3,3,3,
			4,4,
			5
		);
		Collections.shuffle(ironPotionWeights);
		
		diamondPotionWeights = Lists.newArrayList(
			0,
			1,1,
			2,2,2,
			3,3,3,3,
			4,4,4,4,4,
			5,5,5,5
		);
		Collections.shuffle(diamondPotionWeights);
	}

	public void registerCrafts() {
		
		HCStringsAPI s = api.getStringsAPI();
		
		// strings
		ironBookName = s.get("killskill.items.random-iron-book-name").toString();
		ironBookLore = Lists.newArrayList(s.get("killskill.items.random-iron-book-lore").toString().split(Pattern.quote("|")));
		ironBook = new ItemStack(Material.ENCHANTED_BOOK);
		ItemMeta ironMeta = ironBook.getItemMeta();
		ironMeta.setDisplayName(ironBookName);
		ironMeta.setLore(ironBookLore);
		ironBook.setItemMeta(ironMeta);
		
		diamondBookName = s.get("killskill.items.random-diamond-book-name").toString();
		diamondBookLore = Lists.newArrayList(s.get("killskill.items.random-diamond-book-lore").toString().split(Pattern.quote("|")));
		diamondBook = new ItemStack(Material.ENCHANTED_BOOK);
		ItemMeta diamondMeta = diamondBook.getItemMeta();
		diamondMeta.setDisplayName(diamondBookName);
		diamondMeta.setLore(diamondBookLore);
		diamondBook.setItemMeta(diamondMeta);
		
		ironPotionName = s.get("killskill.items.random-iron-potion-name").toString();
		ironPotionLore = Lists.newArrayList(s.get("killskill.items.random-iron-potion-lore").toString().split(Pattern.quote("|")));
		ironPotion = new ItemBuilder(Material.POTION)
		.buildMeta()
		.withDisplayName(ironPotionName)
		.withLore(ironPotionLore)
		.item()
		.build();
		
		diamondPotionName = s.get("killskill.items.random-diamond-potion-name").toString();
		diamondPotionLore = Lists.newArrayList(s.get("killskill.items.random-diamond-potion-lore").toString().split(Pattern.quote("|")));
		diamondPotion = new ItemBuilder(Material.POTION)
		.buildMeta()
		.withDisplayName(diamondPotionName)
		.withLore(diamondPotionLore)
		.item()
		.build();
		
		// remove default blaze powder craft
		removeCraft(new ItemStack(Material.BLAZE_POWDER,2));
		
		// store blaze powder
		registerShapedCraft(new ItemBuilder(Material.BLAZE_ROD)
			.buildMeta()
			.withDisplayName(s.get("killskill.items.stacked-blaze-name").toString())
			.withLore(s.get("killskill.items.stacked-blaze-lore").toString().split(Pattern.quote("|")))
			.item()
			.build(), 
			new String[]{ "xxx","xxx","xxx"},
			Lists.newArrayList("x"),
			Lists.newArrayList(
				new ItemStack(Material.BLAZE_POWDER, 1)
			)
		);
		
		// unstore XP
		registerShapelessCraft(new ItemStack(Material.BLAZE_POWDER, 9), 
			Lists.newArrayList(
				new ItemStack(Material.BLAZE_ROD, 1)
			)
		);
		
		
		// store XP
		registerShapedCraft(new ItemBuilder(Material.SEA_LANTERN)
			.buildMeta()
			.withDisplayName(s.get("killskill.items.stacked-xp-name").toString())
			.withLore(s.get("killskill.items.stacked-xp-lore").toString().split(Pattern.quote("|")))
			.item()
			.build(), 
			new String[]{ "xxx","xxx","xxx"},
			Lists.newArrayList("x"),
			Lists.newArrayList(
				new ItemStack(Material.EXP_BOTTLE, 1)
			)
		);
		
		// unstore XP
		registerShapelessCraft(new ItemStack(Material.EXP_BOTTLE, 9), 
			Lists.newArrayList(
				new ItemStack(Material.SEA_LANTERN, 1)
			)
		);
		
		// notch apple
		registerShapedCraft(new ItemStack(Material.GOLDEN_APPLE, 2, (short) 1), 
			new String[]{ "ddd","dhd","ddd"},
			Lists.newArrayList("d","h"),
			Lists.newArrayList(
				new ItemStack(Material.DIAMOND),
				new ItemStack(Material.SKULL_ITEM, 1, (short) 3)
			)
		);

		
		// iron book
		registerShapedCraft(ironBook.clone(), 
			new String[]{ "iii","ibi","iii"},
			Lists.newArrayList("i","b"),
			Lists.newArrayList(
				new ItemStack(Material.IRON_INGOT),
				new ItemStack(Material.BOOK)
			)
		);
		
		// diamond book
		registerShapedCraft(diamondBook.clone(), 
			new String[]{ "ddd","dbd","ddd"},
			Lists.newArrayList("d","b"),
			Lists.newArrayList(
				new ItemStack(Material.DIAMOND),
				new ItemStack(Material.BOOK)
			)
		);
		

		// iron potion
	    registerShapedCraft(ironPotion.clone(), 
			new String[]{ "iii","ibi","iii"},
			Lists.newArrayList("i","b"),
			Lists.newArrayList(
				new ItemStack(Material.IRON_INGOT),
				new ItemStack(Material.BLAZE_POWDER)
			)
		);
		

		// diamond potion
	    registerShapedCraft(diamondPotion.clone(), 
			new String[]{ "ddd","dbd","ddd"},
			Lists.newArrayList("d","b"),
			Lists.newArrayList(
				new ItemStack(Material.DIAMOND),
				new ItemStack(Material.BLAZE_POWDER)
			)
		);
	    
	    // roman integers
	    this.romanIntegers = new HashMap<Integer,String>();
	    romanIntegers.put(1, "I");
	    romanIntegers.put(2, "II");
	    romanIntegers.put(3, "III");
	    romanIntegers.put(4, "IV");
	    romanIntegers.put(5, "V");
	    romanIntegers.put(6, "VI");
	    romanIntegers.put(7, "VII");
	    romanIntegers.put(8, "VIII");
	    romanIntegers.put(9, "IX");
	    romanIntegers.put(10, "X");
	    romanIntegers.put(11, "XI");
	    romanIntegers.put(12, "XII");
	}
	
	/**
	 * Remove a craft by i
	 * @param result
	 */
	private void removeCraft(ItemStack result){

		//List<Recipe> recipes = api.getPlugin().getServer().getRecipesFor(result);
			
		Iterator<Recipe> it = Bukkit.getServer().recipeIterator();
		while(it.hasNext()){
			Recipe recipe = it.next();
			if(recipe.getResult().isSimilar(result)){
				it.remove();
				break;
			}
		}
		
	}
	
	private void registerShapelessCraft(ItemStack result, List<ItemStack> ingredients){
		ShapelessRecipe recipe = new ShapelessRecipe(result);
		
		for(ItemStack ingredient : ingredients){
			if(!ingredient.getType().equals(Material.AIR)){
				recipe.addIngredient(ingredient.getType());
			}
		}
		
		api.getPlugin().getServer().addRecipe(recipe);
	}

	
	private void registerShapedCraft(ItemStack result, String[] shape, List<String> symbols, List<ItemStack> ingredients){
		ShapedRecipe recipe = new ShapedRecipe(result).shape(shape);
		
		if(symbols.size() != ingredients.size())
			return;
		
		for(int i=0 ; i<symbols.size() ; i++){
			if(!ingredients.get(i).getType().equals(Material.AIR)){
				recipe.setIngredient(symbols.get(i).charAt(0),ingredients.get(i).getData());
			}
		}
		
		api.getPlugin().getServer().addRecipe(recipe);
	}

	/**
	 * Check if stack is random iron enchant book with no enchant assigned yet
	 * @param stack
	 * @return
	 */
	public boolean isRandomIronEnchantBook(ItemStack stack) {
		return Inventories.itemEquals(stack, ironBook, 
				new ItemCompareOption
					.Builder()
					.withMaterial(true)
					.withDisplayName(true)
					.withLore(true)
					.build());
	}


	/**
	 * Check if stack is random diamond enchant book with no enchant assigned yet
	 * @param stack
	 * @return
	 */
	public boolean isRandomDiamondEnchantBook(ItemStack stack) {
		return Inventories.itemEquals(stack, diamondBook, 
				new ItemCompareOption
					.Builder()
					.withMaterial(true)
					.withDisplayName(true)
					.withLore(true)
					.build());
	}

	/**
	 * Check if stack is iron random potion with no effect assigned yet
	 * @param stack
	 * @return
	 */
	public boolean isRandomIronPotion(ItemStack stack) {
		return Inventories.itemEquals(stack, ironPotion, 
				new ItemCompareOption
					.Builder()
					.withMaterial(true)
					.withDisplayName(true)
					.withLore(true)
					.build());
	}

	/**
	 * Check if stack is diamond random potion with no effect assigned yet
	 * @param stack
	 * @return
	 */
	public boolean isRandomDiamondPotion(ItemStack stack) {
		return Inventories.itemEquals(stack, diamondPotion, 
				new ItemCompareOption
					.Builder()
					.withMaterial(true)
					.withDisplayName(true)
					.withLore(true)
					.build());
	}

	// add random book enchant
	
	
	public void addMediumQualityEnchant(ItemStack result, HCPlayer hcPlayer) {
		addRandomEnchant(result, hcPlayer, ironBookWeights);
	}

	public void addHighQualityEnchant(ItemStack result, HCPlayer hcPlayer) {
		addRandomEnchant(result, hcPlayer, diamondBookWeights);
	}

	
	private void addRandomEnchant(ItemStack result, HCPlayer hcPlayer, List<Integer> weights){
		EnchantmentStorageMeta meta = (EnchantmentStorageMeta) result.getItemMeta();
		EnchantmentCraft ench = getRandomWeightedEnchant(weights, enchantments);
		String bookName = getBookName(ench);
		meta.setDisplayName(bookName);
		meta.setLore(Lists.newArrayList(ChatColor.YELLOW+ChatColor.stripColor(ironBookName)));
		meta.addStoredEnchant(ench.getEnchantement(), ench.getLevel(), true);
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		result.setItemMeta(meta);
		api.getSoundAPI().play(hcPlayer, Sound.ANVIL_USE, 1, 2);
		api.getStringsAPI().get("killskill.crafts.random-enchant").replace("%ench%", bookName).sendChatP(hcPlayer);
	}

	
	// add random potion effect
	
	public void addMediumQualityEffect(ItemStack result, HCPlayer hcPlayer) {
		addRandomEffect(result, hcPlayer, ironPotionWeights);
	}

	public void addHighQualityEffect(ItemStack result, HCPlayer hcPlayer) {
		addRandomEffect(result, hcPlayer, diamondPotionWeights);
	}
	
	private void addRandomEffect(ItemStack result, HCPlayer hcPlayer, List<Integer> weights){
		PotionMeta meta = (PotionMeta) result.getItemMeta();
		PotionCraft effect = getRandomWeightedEffect(weights, effects);
		String potionName = getPotionName(effect);
		meta.setDisplayName(potionName);
		meta.setLore(Lists.newArrayList(ChatColor.YELLOW+ChatColor.stripColor(ironPotionName)));
		meta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS);
		if(effect.isThrowable()){
			result.setDurability((short) 16384);
		}
		meta.addCustomEffect(new PotionEffect(effect.getType(), effect.getDuration(), effect.getAmplifier()), true);
		result.setItemMeta(meta);
		api.getSoundAPI().play(hcPlayer, Sound.DRINK, 1, 1);
		api.getStringsAPI().get("killskill.crafts.random-effect").replace("%effect%", potionName).sendChatP(hcPlayer);
	}

	// utility
	
	private String getBookName(EnchantmentCraft ench) {
		String effectName = api.getStringsAPI().get("killskill.crafts.enchantments."+ench.getEnchantement().getName()).toString();
		String level = " "+romanIntegers.get(ench.getLevel());
		return effectName+level;
	}
	
	private String getPotionName(PotionCraft effect) {
		String effectName = api.getStringsAPI().get("killskill.crafts.effects."+effect.getType().getName()).toString();
		String level = " "+romanIntegers.get(effect.getAmplifier()+1);

		int totalSeconds = effect.getDuration()/20;
		int seconds = totalSeconds%60;
		int minutes = (totalSeconds-seconds)/60;
		String time = seconds == 0 ? "" : " ("+minutes+":"+seconds+")";
		
		return effectName+level+time;
	}

	private EnchantmentCraft getRandomWeightedEnchant(List<Integer> weights, Map<Integer,List<EnchantmentCraft>> perks){
		int randomWeight = weights.get(Randoms.randomInteger(0, weights.size()-1));
		List<EnchantmentCraft> perksList = perks.get(randomWeight);
		return perksList.get(Randoms.randomInteger(0, perksList.size()-1));
	}
	
	private PotionCraft getRandomWeightedEffect(List<Integer> weights, Map<Integer,List<PotionCraft>> perks){
		int randomWeight = weights.get(Randoms.randomInteger(0, weights.size()-1));
		List<PotionCraft> perksList = perks.get(randomWeight);
		return perksList.get(Randoms.randomInteger(0, perksList.size()-1));
	}
}
