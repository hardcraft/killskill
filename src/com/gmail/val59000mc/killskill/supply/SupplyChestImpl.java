package com.gmail.val59000mc.killskill.supply;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang.math.RandomUtils;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.entity.Chicken;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityUnleashEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.util.Vector;

import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.killskill.events.KillSkillSupplyDropEvent;
import com.gmail.val59000mc.killskill.events.KillSkillSupplyExpireEvent;
import com.gmail.val59000mc.killskill.events.KillSkillSupplyOpenEvent;
import com.gmail.val59000mc.killskill.events.KillSkillSupplyTouchesGroundEvent;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.spigotutils.particles.ParticleEffect;

public class SupplyChestImpl extends HCListener implements SupplyChest{

	private Location initialLocation;
	private Location groundLocation;
	private Chest chest;
	private FallingBlock fallingBlock;
	private Map<UUID,Chicken> chickens;	
	private Set<Chunk> chunksLoaded;	
	private Vector particleDirection;
	private List<Location> particlesLocations;
	
	public SupplyChestImpl(Location location) {
		this.initialLocation = location.clone().add(0.5,0,0.5);
		this.groundLocation = initialLocation.getWorld().getHighestBlockAt(initialLocation).getLocation().add(0.5,1,0.5);		
		this.chickens = new HashMap<>();
		this.chunksLoaded = new HashSet<>();
		this.fallingBlock = null;
		
		
		particleDirection = new Vector(0,0,0);
		
		double angleStep = 2d*Math.PI/360d;
		particlesLocations = new ArrayList<>();
		for(int i = 0 ; i < 360 ; i+=10){
			double cos = Math.cos(angleStep * (double) i);
			double sin = Math.sin(angleStep * (double) i);
			particlesLocations.add(getGroundLocation().add(cos,-0.8,sin));
			particlesLocations.add(getGroundLocation().add(2d * cos,-0.8,2d * sin));
			particlesLocations.add(getGroundLocation().add(3d * cos,-0.8,3d * sin));
			particlesLocations.add(getGroundLocation().add(4d * cos,-0.8,4d * sin));
		}
		for(int i = -500 ; i<500 ; i+=30){
			particlesLocations.add(getGroundLocation().add((double) i / 100d, -0.8d, 0d));
			particlesLocations.add(getGroundLocation().add(0d,-0.8d, (double) i / 100d));
		}
		for(int i=-50 ; i<15000 ; i+=100){
			particlesLocations.add(getGroundLocation().add(0d,(double) i / 100d, 0d));
		}
	}
	

	@SuppressWarnings("deprecation")
	@Override
	public void drop(){
		World world = initialLocation.getWorld();
		
		if(fallingBlock != null)
			throw new UnsupportedOperationException("Supply chest has already been dropped");
		
		fallingBlock = (FallingBlock) world.spawnFallingBlock(initialLocation, 58, (byte) 2);
		chunksLoaded.add(fallingBlock.getLocation().getChunk());

		for (int i = 0; i < 20; i++) {
			double x = 0.5d*RandomUtils.nextDouble();
			double z = 0.5d*RandomUtils.nextDouble();
            Chicken chicken = (Chicken) world.spawnEntity(initialLocation.clone().add(x, 3, z), EntityType.CHICKEN);
            chicken.setRemoveWhenFarAway(false);
            chickens.put(chicken.getUniqueId(), chicken);
    		chunksLoaded.add(chicken.getLocation().getChunk());
            chicken.setLeashHolder(fallingBlock);
        }
		
		getApi().buildTask("falling supply chest slowly uuid="+fallingBlock.getUniqueId(), new HCTask() {
			
			private int iterations = 0;
			
			@Override
			public void run() {
				
				Location ground = getGroundLocation();
				
				if(fallingBlock.getVelocity().getY() < -0.3){
					fallingBlock.setVelocity(fallingBlock.getVelocity().add(new Vector(0,0.1,0)));
				}
				
				if(iterations%3 == 0){
					getApi().getSoundAPI().play(Sound.CLICK, ground, 1, 2);
				}
				
				if(iterations%10 == 0){
					displayTargetOnGround();
				}
				
				iterations++;
				
			}
			
		})
		.withInterval(1)
		.addListener(new HCTaskListener(){

			@EventHandler
		    public void onLeashBreak(EntityUnleashEvent event) {
				Entity entity = event.getEntity();
		        if (chickens.get(entity.getUniqueId()) != null){
		            for (Entity ent : entity.getNearbyEntities(2, 2, 2)){
		                if (ent instanceof Item && ((Item) ent).getItemStack().getType() == Material.LEASH){
		                    ent.remove();
		                }
		            }
		        }
				Log.debug("Stop supply chest because of broken leash");
		        getScheduler().stop();
		        getApi().unregisterListener(SupplyChestImpl.this);
				destroyParachute();
		    }
			
			@EventHandler
			public void onChunkUnload(ChunkUnloadEvent e){
				if(chunksLoaded.contains(e.getChunk())){
					Log.debug("Stop supply chest because of unloading chunk");
					getScheduler().stop();
					getApi().unregisterListener(SupplyChestImpl.this);
					destroyParachute();
				}
			}
			
			@EventHandler
		    public void onFallingBlockLand(EntityChangeBlockEvent e){
				
		        if(e.getEntity().equals(fallingBlock)){
		        	Log.debug("falling chest landed");
		        	
		        	getScheduler().stop();
					destroyParachute();

		        	Location dropLocation = e.getBlock().getLocation();
		        	getApi().sync(()->{
		        		Block block = dropLocation.getBlock();
		        		block.setType(Material.CHEST);
		        		chest = (Chest) block.getState();
		        		KillSkillSupplyTouchesGroundEvent touchesGround = new KillSkillSupplyTouchesGroundEvent(getApi(), SupplyChestImpl.this);
		        		getPlugin().getServer().getPluginManager().callEvent(touchesGround);
		        	},1);
		        	
		        }
		    }
			
		})
		// security if for whatever reason the chest doesn't touch the ground
		.withLastCallback(new HCTask() {
			
			@Override
			public void run() {
				scheduler.stop();
				destroyParachute();
			}
		})
		.withIterations(1200) // max fall duration 60 sec
		.build()
		.start();
	}
	
	private void destroyParachute(){
    	Log.debug("destroying parachute");
		for (Chicken chicken : chickens.values()) {
            chicken.setLeashHolder(null);
            chicken.remove();
        }
		fallingBlock.remove();
	}
	
	@EventHandler
    public void onOpenChest(InventoryOpenEvent e){
		InventoryHolder holder = e.getInventory().getHolder();
        if (holder instanceof Chest && e.getPlayer() instanceof Player){
        	Chest chest = (Chest) holder;
        	if(chest.getLocation().distanceSquared(getGroundLocation()) < 2){
            	HCPlayer hcPlayer = getPmApi().getHCPlayer((Player) e.getPlayer());
            	if(hcPlayer != null && hcPlayer.isPlaying()){
                	KillSkillSupplyOpenEvent openSupply = new KillSkillSupplyOpenEvent(getApi(), this, (KillSkillPlayer) hcPlayer);
            		getPlugin().getServer().getPluginManager().callEvent(openSupply);
        			getApi().unregisterListener(this);
            	}else{
            		e.setCancelled(true);
            	}
        	}
        }
    }

	@EventHandler
    public void onOtherDrop(KillSkillSupplyDropEvent e){
		e.setCancelled(true);
    }
	
	@EventHandler
    public void onExpireChest(KillSkillSupplyExpireEvent e){
		if(e.getSupplyChest().equals(this)){
			getApi().unregisterListener(this);
		}
    }
	
	@EventHandler
    public void onChickenDamage(EntityDamageEvent e){
		if(chickens.containsKey(e.getEntity().getUniqueId())){
			e.setCancelled(true);
		}
    }


	@Override
	public Location getGroundLocation() {
		return groundLocation.clone();
	}


	@Override
	public Location getDropLocation() {
		return initialLocation.clone();
	}


	@Override
	public Chest getBlock() {
		return chest;
	}
	
	@Override
	public void displayTargetOnGround() {
		for(Location particleLocation : particlesLocations){
			ParticleEffect.FLAME.display(particleDirection,0.1f,particleLocation, 300);
		}
	}
	
	
	
}
