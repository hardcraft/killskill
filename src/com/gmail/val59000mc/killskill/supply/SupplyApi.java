package com.gmail.val59000mc.killskill.supply;

import com.gmail.val59000mc.killskill.players.KillSkillPlayer;

public interface SupplyApi {

	/**
	 * Drop a new random supply if allowed
	 * @return the supplyChest or null if not allowed
	 */
	public SupplyChest dropRandomSupply();

	public void handleOpenChest(SupplyChest chest, KillSkillPlayer ksPlayer);

	public void handleExpireChest(SupplyChest chest);

	public void handleLandingChest(SupplyChest chest);
	
}
