package com.gmail.val59000mc.killskill.supply;
import java.lang.reflect.Field;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.enchantments.EnchantmentTarget;
import org.bukkit.enchantments.EnchantmentWrapper;
import org.bukkit.inventory.ItemStack;

public class FakeGlowEnchant extends EnchantmentWrapper
{
	
	private static Enchantment glow;
	
	public FakeGlowEnchant( int id )
	{
		super(id);
	}
	
	@Override
	public boolean canEnchantItem(ItemStack item)
	{
		return true;
	}
	
	@Override
	public boolean conflictsWith(Enchantment other)
	{
		return false;
	}
	
	@Override
	public EnchantmentTarget getItemTarget()
	{
		return null;
	}
	
	@Override
	public int getMaxLevel()
	{
		return 10;
	}
	
	@Override
	public String getName()
	{
		return "FakeGlow";
	}
	
	@Override
	public int getStartLevel()
	{
		return 1;
	}
	
	public static Enchantment getGlow()
	{
		if ( glow != null )
			return glow;
		
		try
		{
			Field f = Enchantment.class.getDeclaredField("acceptingNew");
			f.setAccessible(true);
			f.set(null , true);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		
		FakeGlowEnchant newGlowEnchant = new FakeGlowEnchant(255);
		Enchantment registered = Enchantment.getByName(newGlowEnchant.getName());
		if(registered == null){
			glow = newGlowEnchant;
			Enchantment.registerEnchantment(glow);
		}else{
			glow = registered;
		}
		return glow;
	}
	
	public static ItemStack addGlow(ItemStack item){		
		item.addEnchantment(getGlow() , 1);
		return item;
	}
	
}