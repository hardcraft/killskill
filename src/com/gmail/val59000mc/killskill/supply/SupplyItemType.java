package com.gmail.val59000mc.killskill.supply;

public enum SupplyItemType {
	DIAMOND_ARMOR,
	IRON_ARMOR,
	IRON_SWORD,
	DIAMOND_SWORD,
	MISCELLANEOUS,
	BOW,
	POTION;
}
