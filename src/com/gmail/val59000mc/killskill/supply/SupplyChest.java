package com.gmail.val59000mc.killskill.supply;

import org.bukkit.Location;
import org.bukkit.block.Chest;

public interface SupplyChest {

	/**
	 * Start dropping the supply chest
	 */
	void drop();
	
	/**
	 * Returns a copy of the supply chest ground location
	 * @return
	 */
	Location getGroundLocation();

	/**
	 * Returns a copy of the supply chest initial drop location
	 * @return
	 */
	Location getDropLocation();
	
	/**
	 * Get the actual bukkit chest block
	 * May be null
	 * @return
	 */
	Chest getBlock();

	/**
	 * Display a particle targetr on the ground
	 */
	void displayTargetOnGround();
}
