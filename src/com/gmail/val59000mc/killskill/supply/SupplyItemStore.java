package com.gmail.val59000mc.killskill.supply;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.Validate;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.killskill.items.KillSkillItems;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.gmail.val59000mc.spigotutils.items.ItemBuilder;
import com.rit.sucy.CustomEnchantment;
import com.rit.sucy.EnchantmentAPI;

public class SupplyItemStore {

	private HCGameAPI api;
	
	/**
	 * Store the items indexed by type then level, for fast read access.
	 * Intialization may be a bit slow but it doesn't matter.
	 */
	private List<SupplyItemType> types = Collections.unmodifiableList(Arrays.asList(SupplyItemType.values()));
	private Map<SupplyItemType,Map<Integer,List<ItemStack>>> store;
	private Map<Integer, List<Integer>> chances;
	
	
	public SupplyItemStore(HCGameAPI api){
		this.api = api;
		this.store = new HashMap<>();
		this.chances = new HashMap<>();
		load();
	}
	
	/**
	 * Get a list of random supply items considerint player's supply level and supply amount
	 * @param killSkillPlayer
	 * @return
	 */
	public List<ItemStack> getRandomSupplyItems(KillSkillPlayer killSkillPlayer){
		List<ItemStack> items = new ArrayList<>();
		for(int i=0 ; i<killSkillPlayer.getSupplyAmount() ; i++){
			items.add(getRandomSupplyItem(killSkillPlayer));
		}
		return items;
	}
	
	/**
	 * Get a random supply item considering player's supply level
	 * @param killSkillPlayer
	 * @return
	 */
	public ItemStack getRandomSupplyItem(KillSkillPlayer killSkillPlayer){
		SupplyItemType randomType = types.get(Randoms.randomInteger(0, types.size()-1));
		return getRandomSupplyItem(randomType, killSkillPlayer);
	}
	
	/**
	 * Get a random supply of a given type item considering player's supply level
	 * @param type
	 * @param killSkillPlayer
	 * @return
	 */
	public ItemStack getRandomSupplyItem(SupplyItemType type, KillSkillPlayer killSkillPlayer){
		int randomLevel = getRandomSupplyChanceLevel(killSkillPlayer);
		return getRandomSupplyItem(type, randomLevel);
	}
	
	private Integer getRandomSupplyChanceLevel(KillSkillPlayer ksPlayer){
		int playerSupplyLevel = ksPlayer.getSupplyLevel();
		
		// prevent out of bounds
		if(playerSupplyLevel < 1 || playerSupplyLevel > 7)
			return 1;
		
		// retrieve chances supply mapping for given level
		List<Integer> chancesForGivenLevel = chances.get(playerSupplyLevel);
		Integer randomChance = chancesForGivenLevel.get(Randoms.randomInteger(0, 99));
		Log.debug("Random supply level calculated for "+ksPlayer.getName()+"'s supply level ("+playerSupplyLevel+") is "+randomChance);
		return randomChance;
	}
	
	
	/**
	 * Get a random supply of a given type and a given level
	 * @param type
	 * @param killSkillPlayer
	 * @return
	 */
	private ItemStack getRandomSupplyItem(SupplyItemType type, int level){
		List<ItemStack> items = store.get(type).get(level);
		return items.get(Randoms.randomInteger(0, items.size()-1));
	}

	private void load() {
		
		Log.info("Loading supply items");
		Long start = System.currentTimeMillis();
		
		// initialize internal chances map
		
		// level 1
		List<Integer> level_1 = new ArrayList<>();
		addToList(level_1, 1, 70);
		addToList(level_1, 2, 5);
		addToList(level_1, 3, 5);
		addToList(level_1, 4, 5);
		addToList(level_1, 5, 5);
		addToList(level_1, 6, 5);
		addToList(level_1, 7, 5);
		Collections.shuffle(level_1);
		chances.put(1, level_1);
		
		// level 2
		List<Integer> level_2 = new ArrayList<>();
		addToList(level_2, 1, 38);
		addToList(level_2, 2, 37);
		addToList(level_2, 3, 5);
		addToList(level_2, 4, 5);
		addToList(level_2, 5, 5);
		addToList(level_2, 6, 5);
		addToList(level_2, 7, 5);
		Collections.shuffle(level_2);
		chances.put(2, level_2);
		
		// level 3
		List<Integer> level_3 = new ArrayList<>();
		addToList(level_3, 1, 27);
		addToList(level_3, 2, 27);
		addToList(level_3, 3, 26);
		addToList(level_3, 4, 5);
		addToList(level_3, 5, 5);
		addToList(level_3, 6, 5);
		addToList(level_3, 7, 5);
		Collections.shuffle(level_3);
		chances.put(3, level_3);
		
		// level 4
		List<Integer> level_4 = new ArrayList<>();
		addToList(level_4, 1, 22);
		addToList(level_4, 2, 21);
		addToList(level_4, 3, 21);
		addToList(level_4, 4, 21);
		addToList(level_4, 5, 5);
		addToList(level_4, 6, 5);
		addToList(level_4, 7, 5);
		Collections.shuffle(level_4);
		chances.put(4, level_4);
		
		// level 5
		List<Integer> level_5 = new ArrayList<>();
		addToList(level_5, 1, 18);
		addToList(level_5, 2, 18);
		addToList(level_5, 3, 18);
		addToList(level_5, 4, 18);
		addToList(level_5, 5, 18);
		addToList(level_5, 6, 5);
		addToList(level_5, 7, 5);
		Collections.shuffle(level_5);
		chances.put(5, level_5);
		
		// level 6
		List<Integer> level_6 = new ArrayList<>();
		addToList(level_6, 1, 16);
		addToList(level_6, 2, 16);
		addToList(level_6, 3, 16);
		addToList(level_6, 4, 16);
		addToList(level_6, 5, 16);
		addToList(level_6, 6, 15);
		addToList(level_6, 7, 5);
		Collections.shuffle(level_6);
		chances.put(6, level_6);
		
		// level 7
		List<Integer> level_7 = new ArrayList<>();
		addToList(level_7, 1, 15);
		addToList(level_7, 2, 15);
		addToList(level_7, 3, 14);
		addToList(level_7, 4, 14);
		addToList(level_7, 5, 14);
		addToList(level_7, 6, 14);
		addToList(level_7, 7, 14);
		Collections.shuffle(level_7);
		chances.put(7, level_7);
		
		// initialize internal store map
		for(SupplyItemType type : SupplyItemType.values()){
			Map<Integer,List<ItemStack>> storeByType = new HashMap<>();
			for(int i = 1 ; i <= 7 ; i++){
				storeByType.put(i, new ArrayList<>());
			}
			store.put(type, storeByType);
		}
		
		SupplyItem.supplyItemStore = this;

		int five = 5;
		int tenth = 10;
		int fifteen = 10;
		int quarter = 25;
		int third = 33;
		int mid = 50;
		int twoThirds = 66;
		int threeQuarters = 75;
		int eighty = 80;
		
		// DIAMOND_ARMOR
		SupplyItem.supplyItemType = SupplyItemType.DIAMOND_ARMOR;
		
		// level 1
		SupplyItem.supplyItemLevel = 1;
		SupplyItem.from(new ItemStack(Material.DIAMOND_HELMET)).withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_LEGGINGS)).withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_BOOTS)).withLife(third).store();
		
		// level 2
		SupplyItem.supplyItemLevel = 2;
		SupplyItem.from(new ItemStack(Material.DIAMOND_HELMET)).withLife(twoThirds).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withLife(twoThirds).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_LEGGINGS)).withLife(twoThirds).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_BOOTS)).withLife(twoThirds).store();
		
		// level 3
		SupplyItem.supplyItemLevel = 3;
		SupplyItem.from(new ItemStack(Material.DIAMOND_HELMET)).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_LEGGINGS)).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_BOOTS)).store();	
		SupplyItem.from(new ItemStack(Material.DIAMOND_HELMET)).withCustomEnchant("Life", 1).withRareLore().withLife(fifteen).store();	
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withCustomEnchant("Demoralizing", 1).withRareLore().withLife(tenth).store();	
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withCustomEnchant("Cursed", 1).withRareLore().withLife(tenth).store();	
		SupplyItem.from(new ItemStack(Material.DIAMOND_HELMET)).withCustomEnchant("Brilliance", 1).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_LEGGINGS)).withCustomEnchant("Frost", 1).withRareLore().withLife(tenth).store();
		
		// level 4
		SupplyItem.supplyItemLevel = 4;
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_HELMET).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_CHESTPLATE).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_LEGGINGS).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_BOOTS).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true).item().build()).store();	
		SupplyItem.from(new ItemStack(Material.DIAMOND_LEGGINGS)).withCustomEnchant("Adrenaline", 1).withRareLore().withLife(tenth).store();		
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withCustomEnchant("Life", 1).withRareLore().withLife(mid).store();		
		SupplyItem.from(new ItemStack(Material.DIAMOND_BOOTS)).withCustomEnchant("Jump", 1).withRareLore().withLife(fifteen).store();	
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withCustomEnchant("Demoralizing", 1).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withCustomEnchant("Cursed", 1).withRareLore().withLife(third).store();		
		SupplyItem.from(new ItemStack(Material.DIAMOND_HELMET)).withCustomEnchant("Brilliance", 1).withRareLore().withLife(quarter).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_LEGGINGS)).withCustomEnchant("Frost", 1).withRareLore().withLife(quarter).store();
		
		// level 5
		SupplyItem.supplyItemLevel = 5;
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_HELMET).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_CHESTPLATE).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_LEGGINGS).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_BOOTS).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true).item().build()).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_LEGGINGS)).withCustomEnchant("Adrenaline", 1).withRareLore().withLife(quarter).store();					
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withCustomEnchant("Toxic", 2).withRareLore().withLife(quarter).store();				
		SupplyItem.from(new ItemStack(Material.DIAMOND_BOOTS)).withCustomEnchant("Lively", 2).withRareLore().withLife(quarter).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withCustomEnchant("Life", 2).withRareLore().withLife(quarter).store();	
		SupplyItem.from(new ItemStack(Material.DIAMOND_BOOTS)).withCustomEnchant("Jump", 2).withRareLore().withLife(tenth).store();	
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withCustomEnchant("Demoralizing", 2).withRareLore().withLife(quarter).store();	
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withCustomEnchant("Cursed", 2).withRareLore().withLife(quarter).store();		
		SupplyItem.from(new ItemStack(Material.DIAMOND_HELMET)).withCustomEnchant("Brilliance", 1).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_LEGGINGS)).withCustomEnchant("Frost", 1).withRareLore().withLife(mid).store();
		
		// level 6
		SupplyItem.supplyItemLevel = 6;
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_HELMET).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_CHESTPLATE).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_LEGGINGS).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_BOOTS).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true).item().build()).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_LEGGINGS)).withCustomEnchant("Adrenaline", 2).withRareLore().withLife(fifteen).store();					
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withCustomEnchant("Toxic", 3).withRareLore().withLife(fifteen).store();				
		SupplyItem.from(new ItemStack(Material.DIAMOND_BOOTS)).withCustomEnchant("Lively", 3).withRareLore().withLife(fifteen).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withCustomEnchant("Life", 2).withRareLore().withLife(mid).store();			
		SupplyItem.from(new ItemStack(Material.DIAMOND_BOOTS)).withCustomEnchant("Jump", 2).withRareLore().withLife(quarter).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withCustomEnchant("Demoralizing", 2).withRareLore().withLife(mid).store();		
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withCustomEnchant("Cursed", 2).withRareLore().withLife(mid).store();			
		SupplyItem.from(new ItemStack(Material.DIAMOND_HELMET)).withCustomEnchant("Brilliance", 2).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_LEGGINGS)).withCustomEnchant("Frost", 2).withRareLore().withLife(mid).store();
		
		// level 7
		SupplyItem.supplyItemLevel = 7;			
		SupplyItem.from(new ItemStack(Material.DIAMOND_LEGGINGS)).withCustomEnchant("Adrenaline", 3).withRareLore().withLife(tenth).store();				
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withCustomEnchant("Toxic", 4).withRareLore().withLife(tenth).store();				
		SupplyItem.from(new ItemStack(Material.DIAMOND_BOOTS)).withCustomEnchant("Lively", 4).withRareLore().withLife(tenth).store();					
		SupplyItem.from(new ItemStack(Material.DIAMOND_BOOTS)).withCustomEnchant("Lively", 4).withRareLore().withLife(mid).store();			
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withCustomEnchant("Life", 1).withRareLore().withLife(mid).store();	
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withCustomEnchant("Life", 3).withRareLore().withLife(fifteen).store();			
		SupplyItem.from(new ItemStack(Material.DIAMOND_BOOTS)).withCustomEnchant("Jump", 3).withRareLore().withLife(fifteen).store();			
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withCustomEnchant("Demoralizing", 3).withRareLore().withLife(mid).store();			
		SupplyItem.from(new ItemStack(Material.DIAMOND_CHESTPLATE)).withCustomEnchant("Cursed", 3).withRareLore().withLife(quarter).store();		
		SupplyItem.from(new ItemStack(Material.DIAMOND_HELMET)).withCustomEnchant("Brilliance", 3).withRareLore().withLife(third).store();		
		SupplyItem.from(new ItemStack(Material.DIAMOND_LEGGINGS)).withCustomEnchant("Frost", 3).withRareLore().withLife(third).store();		
		
		
		
		// IRON_ARMOR
		SupplyItem.supplyItemType = SupplyItemType.IRON_ARMOR;
		
		// level 1
		SupplyItem.supplyItemLevel = 1;
		SupplyItem.from(new ItemStack(Material.IRON_HELMET)).withLife(third).store();
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withLife(third).store();
		SupplyItem.from(new ItemStack(Material.IRON_LEGGINGS)).withLife(third).store();
		SupplyItem.from(new ItemStack(Material.IRON_BOOTS)).withLife(third).store();
		
		// level 2
		SupplyItem.supplyItemLevel = 2;
		SupplyItem.from(new ItemStack(Material.IRON_HELMET)).withLife(twoThirds).store();
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withLife(twoThirds).store();
		SupplyItem.from(new ItemStack(Material.IRON_LEGGINGS)).withLife(twoThirds).store();
		SupplyItem.from(new ItemStack(Material.IRON_BOOTS)).withLife(twoThirds).store();
		
		// level 3
		SupplyItem.supplyItemLevel = 3;
		SupplyItem.from(new ItemStack(Material.IRON_HELMET)).store();
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).store();
		SupplyItem.from(new ItemStack(Material.IRON_LEGGINGS)).store();
		SupplyItem.from(new ItemStack(Material.IRON_BOOTS)).store();	
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Life", 1).withRareLore().withLife(fifteen).store();	
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Demoralizing", 1).withRareLore().withLife(tenth).store();	
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Cursed", 1).withRareLore().withLife(tenth).store();	
		SupplyItem.from(new ItemStack(Material.IRON_HELMET)).withCustomEnchant("Brilliance", 1).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.IRON_LEGGINGS)).withCustomEnchant("Frost", 1).withRareLore().withLife(tenth).store();
		
		// level 4
		SupplyItem.supplyItemLevel = 4;
		SupplyItem.from(new ItemBuilder(Material.IRON_HELMET).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_CHESTPLATE).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_LEGGINGS).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_BOOTS).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true).item().build()).store();	
		SupplyItem.from(new ItemStack(Material.IRON_LEGGINGS)).withCustomEnchant("Adrenaline", 1).withRareLore().withLife(tenth).store();		
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Life", 1).withRareLore().withLife(mid).store();		
		SupplyItem.from(new ItemStack(Material.IRON_BOOTS)).withCustomEnchant("Jump", 1).withRareLore().withLife(fifteen).store();	
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Demoralizing", 1).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Cursed", 1).withRareLore().withLife(third).store();		
		SupplyItem.from(new ItemStack(Material.IRON_HELMET)).withCustomEnchant("Brilliance", 1).withRareLore().withLife(quarter).store();
		SupplyItem.from(new ItemStack(Material.IRON_LEGGINGS)).withCustomEnchant("Frost", 1).withRareLore().withLife(quarter).store();
		
		// level 5
		SupplyItem.supplyItemLevel = 5;
		SupplyItem.from(new ItemBuilder(Material.IRON_HELMET).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_CHESTPLATE).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_LEGGINGS).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_BOOTS).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 2, true).item().build()).store();
		SupplyItem.from(new ItemStack(Material.IRON_LEGGINGS)).withCustomEnchant("Adrenaline", 1).withRareLore().withLife(quarter).store();					
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Toxic", 2).withRareLore().withLife(quarter).store();				
		SupplyItem.from(new ItemStack(Material.IRON_BOOTS)).withCustomEnchant("Lively", 2).withRareLore().withLife(quarter).store();
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Life", 2).withRareLore().withLife(quarter).store();	
		SupplyItem.from(new ItemStack(Material.IRON_BOOTS)).withCustomEnchant("Jump", 2).withRareLore().withLife(tenth).store();	
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Demoralizing", 2).withRareLore().withLife(quarter).store();	
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Cursed", 2).withRareLore().withLife(quarter).store();		
		SupplyItem.from(new ItemStack(Material.IRON_HELMET)).withCustomEnchant("Brilliance", 1).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_LEGGINGS)).withCustomEnchant("Frost", 1).withRareLore().withLife(mid).store();
		
		// level 6
		SupplyItem.supplyItemLevel = 6;
		SupplyItem.from(new ItemBuilder(Material.IRON_HELMET).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_CHESTPLATE).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_LEGGINGS).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_BOOTS).buildMeta().withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 3, true).item().build()).store();
		SupplyItem.from(new ItemStack(Material.IRON_LEGGINGS)).withCustomEnchant("Adrenaline", 2).withRareLore().withLife(fifteen).store();					
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Toxic", 3).withRareLore().withLife(fifteen).store();				
		SupplyItem.from(new ItemStack(Material.IRON_BOOTS)).withCustomEnchant("Lively", 3).withRareLore().withLife(fifteen).store();
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Life", 2).withRareLore().withLife(mid).store();			
		SupplyItem.from(new ItemStack(Material.IRON_BOOTS)).withCustomEnchant("Jump", 2).withRareLore().withLife(quarter).store();
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Demoralizing", 2).withRareLore().withLife(mid).store();		
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Cursed", 2).withRareLore().withLife(mid).store();			
		SupplyItem.from(new ItemStack(Material.IRON_HELMET)).withCustomEnchant("Brilliance", 2).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_LEGGINGS)).withCustomEnchant("Frost", 2).withRareLore().withLife(mid).store();
		
		// level 7
		SupplyItem.supplyItemLevel = 7;			
		SupplyItem.from(new ItemStack(Material.IRON_LEGGINGS)).withCustomEnchant("Adrenaline", 3).withRareLore().withLife(tenth).store();				
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Toxic", 4).withRareLore().withLife(tenth).store();				
		SupplyItem.from(new ItemStack(Material.IRON_BOOTS)).withCustomEnchant("Lively", 4).withRareLore().withLife(tenth).store();					
		SupplyItem.from(new ItemStack(Material.IRON_BOOTS)).withCustomEnchant("Lively", 4).withRareLore().withLife(mid).store();			
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Life", 1).withRareLore().withLife(mid).store();	
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Life", 3).withRareLore().withLife(fifteen).store();			
		SupplyItem.from(new ItemStack(Material.IRON_BOOTS)).withCustomEnchant("Jump", 3).withRareLore().withLife(fifteen).store();			
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Demoralizing", 3).withRareLore().withLife(mid).store();			
		SupplyItem.from(new ItemStack(Material.IRON_CHESTPLATE)).withCustomEnchant("Cursed", 3).withRareLore().withLife(quarter).store();		
		SupplyItem.from(new ItemStack(Material.IRON_HELMET)).withCustomEnchant("Brilliance", 3).withRareLore().withLife(third).store();		
		SupplyItem.from(new ItemStack(Material.IRON_LEGGINGS)).withCustomEnchant("Frost", 3).withRareLore().withLife(third).store();		
				
		
		
		// IRON_SWORD
		SupplyItem.supplyItemType = SupplyItemType.IRON_SWORD;
		
		// level 1
		SupplyItem.supplyItemLevel = 1;
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DURABILITY, 1, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DURABILITY, 1, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DURABILITY, 1, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DURABILITY, 1, true).item().build()).withLife(mid).store();
		
		// level 2
		SupplyItem.supplyItemLevel = 2;
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DURABILITY, 2, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DURABILITY, 2, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DURABILITY, 2, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DURABILITY, 2, true).item().build()).withLife(mid).store();
		
		// level 3
		SupplyItem.supplyItemLevel = 3;
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 1, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 1, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 1, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 1, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 1, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 1, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 1, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 1, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 1, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 1, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 1, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 1, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 1, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 1, true).item().build()).withLife(eighty).store();
		
		// level 4
		SupplyItem.supplyItemLevel = 4;
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 2, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 2, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 2, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 2, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 2, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 2, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 2, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 2, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 2, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 2, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 2, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 2, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 2, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 2, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Fervor", 1).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Wither", 1).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Knockup", 1).withRareLore().withLife(third).store();
		
		// level 5
		SupplyItem.supplyItemLevel = 5;
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 3, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 3, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 3, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 3, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 3, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 3, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 3, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Fervor", 1).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Fervor", 2).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Wither", 1).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Wither", 2).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Knockup", 1).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Knockup", 2).withRareLore().withLife(third).store();
		
		// level 6
		SupplyItem.supplyItemLevel = 6;
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 4, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 4, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 4, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 4, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 4, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 4, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 4, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 1, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 1, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 1, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 1, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 1, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 1, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 1, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Fervor", 2).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Fervor", 3).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Wither", 2).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Wither", 3).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Knockup", 2).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Knockup", 3).withRareLore().withLife(third).store();
		
		// level 7
		SupplyItem.supplyItemLevel = 7;
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 5, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 5, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 5, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 5, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 5, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 5, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 5, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 2, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 2, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 2, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 2, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 2, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 2, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.IRON_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 2, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Fervor", 3).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Fervor", 4).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Wither", 3).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Wither", 4).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Knockup", 3).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_SWORD)).withCustomEnchant("Knockup", 4).withRareLore().withLife(third).store();
				
		
		
		// DIAMOND_SWORD
		SupplyItem.supplyItemType = SupplyItemType.DIAMOND_SWORD;
		
		// level 1
		SupplyItem.supplyItemLevel = 1;
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withLife(quarter).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withLife(mid).store();
		
		// level 2
		SupplyItem.supplyItemLevel = 2;
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withLife(twoThirds).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withLife(threeQuarters).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withLife(eighty).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).store();
		
		// level 3
		SupplyItem.supplyItemLevel = 3;
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 1, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 1, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 1, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 1, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 1, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 1, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 1, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 1, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 1, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 1, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 1, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 1, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 1, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 1, true).item().build()).withLife(eighty).store();
		
		// level 4
		SupplyItem.supplyItemLevel = 4;
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 2, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 2, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 2, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 2, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 2, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 2, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 2, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 2, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 2, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 2, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 2, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 2, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 2, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.KNOCKBACK, 2, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Fervor", 1).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Wither", 1).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Knockup", 1).withRareLore().withLife(tenth).store();
		
		// level 5
		SupplyItem.supplyItemLevel = 5;
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 3, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 3, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 3, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 3, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 3, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 3, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 3, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Fervor", 1).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Fervor", 2).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Wither", 1).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Wither", 2).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Knockup", 1).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Knockup", 2).withRareLore().withLife(five).store();
		
		// level 6
		SupplyItem.supplyItemLevel = 6;
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 4, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 4, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 4, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 4, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 4, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 4, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 4, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 1, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 1, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 1, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 1, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 1, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 1, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 1, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Fervor", 2).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Fervor", 3).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Wither", 2).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Wither", 3).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Knockup", 2).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Knockup", 3).withRareLore().withLife(five).store();
		
		// level 7
		SupplyItem.supplyItemLevel = 7;
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 5, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 5, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 5, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 5, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 5, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 5, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.DAMAGE_ALL, 5, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 2, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 2, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 2, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 2, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 2, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 2, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.DIAMOND_SWORD).buildMeta().withEnchant(Enchantment.FIRE_ASPECT, 2, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Fervor", 3).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Fervor", 4).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Wither", 3).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Wither", 4).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Knockup", 3).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SWORD)).withCustomEnchant("Knockup", 4).withRareLore().withLife(five).store();
		
		
		
		// MISCELLANEOUS
		SupplyItem.supplyItemType = SupplyItemType.MISCELLANEOUS;
		
		// level 1
		SupplyItem.supplyItemLevel = 1;
		SupplyItem.from(KillSkillItems.getExperienceLoot(1)).store();
		SupplyItem.from(KillSkillItems.getLapisLoot(1)).store();
		SupplyItem.from(KillSkillItems.getIronLoot(1)).store();
		SupplyItem.from(KillSkillItems.getDiamondLoot(1)).store();
		SupplyItem.from(KillSkillItems.getHealPotionLoot(1)).store();
		SupplyItem.from(KillSkillItems.getBlazeLoot(1)).store();
		SupplyItem.from(new ItemStack(Material.STICK, 1)).store();
		SupplyItem.from(new ItemStack(Material.BOOK, 1)).store();
		
		// level 2
		SupplyItem.supplyItemLevel = 2;
		SupplyItem.from(KillSkillItems.getExperienceLoot(2)).store();
		SupplyItem.from(KillSkillItems.getLapisLoot(2)).store();
		SupplyItem.from(KillSkillItems.getIronLoot(2)).store();
		SupplyItem.from(KillSkillItems.getDiamondLoot(2)).store();
		SupplyItem.from(KillSkillItems.getHealPotionLoot(2)).store();
		SupplyItem.from(KillSkillItems.getBlazeLoot(2)).store();
		SupplyItem.from(new ItemStack(Material.STICK, 2)).store();
		SupplyItem.from(new ItemStack(Material.BOOK, 2)).store();
		
		// level 3
		SupplyItem.supplyItemLevel = 3;
		SupplyItem.from(KillSkillItems.getExperienceLoot(3)).store();
		SupplyItem.from(KillSkillItems.getLapisLoot(3)).store();
		SupplyItem.from(KillSkillItems.getIronLoot(3)).store();
		SupplyItem.from(KillSkillItems.getDiamondLoot(3)).store();
		SupplyItem.from(KillSkillItems.getHealPotionLoot(3)).store();
		SupplyItem.from(KillSkillItems.getBlazeLoot(3)).store();
		SupplyItem.from(new ItemStack(Material.STICK, 3)).store();
		SupplyItem.from(new ItemStack(Material.BOOK, 3)).store();
		
		// level 4
		SupplyItem.supplyItemLevel = 4;
		SupplyItem.from(KillSkillItems.getExperienceLoot(4)).store();
		SupplyItem.from(KillSkillItems.getLapisLoot(4)).store();
		SupplyItem.from(KillSkillItems.getIronLoot(4)).store();
		SupplyItem.from(KillSkillItems.getDiamondLoot(4)).store();
		SupplyItem.from(KillSkillItems.getHealPotionLoot(4)).store();
		SupplyItem.from(KillSkillItems.getBlazeLoot(4)).store();
		SupplyItem.from(new ItemStack(Material.IRON_PICKAXE)).withCustomEnchant("Blind", 1).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_PICKAXE)).withCustomEnchant("Blind", 1).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_AXE)).withCustomEnchant("Berserking", 1).withRareLore().withLife(quarter).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_AXE)).withCustomEnchant("Berserking", 1).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_HOE)).withCustomEnchant("Lifesteal", 1).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_HOE)).withCustomEnchant("Lifesteal", 1).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_HOE)).withCustomEnchant("Poison", 1).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_HOE)).withCustomEnchant("Poison", 1).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_PICKAXE)).withCustomEnchant("Slowing", 1).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_PICKAXE)).withCustomEnchant("Slowing", 1).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_SPADE)).withCustomEnchant("Weakness", 1).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SPADE)).withCustomEnchant("Weakness", 1).withRareLore().withLife(five).store();
		
		// level 5
		SupplyItem.supplyItemLevel = 5;
		SupplyItem.from(KillSkillItems.getExperienceLoot(5)).store();
		SupplyItem.from(KillSkillItems.getLapisLoot(5)).store();
		SupplyItem.from(KillSkillItems.getIronLoot(5)).store();
		SupplyItem.from(KillSkillItems.getDiamondLoot(5)).store();
		SupplyItem.from(KillSkillItems.getHealPotionLoot(5)).store();
		SupplyItem.from(KillSkillItems.getBlazeLoot(5)).store();
		SupplyItem.from(KillSkillItems.getGrenadeItem(1)).store();
		SupplyItem.from(KillSkillItems.getTntItem(1)).store();
		SupplyItem.from(KillSkillItems.getCanonItem(1)).store();
		SupplyItem.from(new ItemStack(Material.ENDER_PEARL, 1)).store();
		SupplyItem.from(new ItemStack(Material.IRON_PICKAXE)).withCustomEnchant("Blind", 1).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_PICKAXE)).withCustomEnchant("Blind", 2).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_PICKAXE)).withCustomEnchant("Blind", 1).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_PICKAXE)).withCustomEnchant("Blind", 2).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_AXE)).withCustomEnchant("Berserking", 1).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.IRON_AXE)).withCustomEnchant("Berserking", 2).withRareLore().withLife(quarter).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_AXE)).withCustomEnchant("Berserking", 1).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_AXE)).withCustomEnchant("Berserking", 2).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_HOE)).withCustomEnchant("Lifesteal", 1).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_HOE)).withCustomEnchant("Lifesteal", 2).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_HOE)).withCustomEnchant("Lifesteal", 1).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_HOE)).withCustomEnchant("Lifesteal", 2).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_HOE)).withCustomEnchant("Poison", 1).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_HOE)).withCustomEnchant("Poison", 2).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_HOE)).withCustomEnchant("Poison", 1).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_HOE)).withCustomEnchant("Poison", 2).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_PICKAXE)).withCustomEnchant("Slowing", 1).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_PICKAXE)).withCustomEnchant("Slowing", 2).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_PICKAXE)).withCustomEnchant("Slowing", 1).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_PICKAXE)).withCustomEnchant("Slowing", 2).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_SPADE)).withCustomEnchant("Weakness", 1).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_SPADE)).withCustomEnchant("Weakness", 2).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SPADE)).withCustomEnchant("Weakness", 1).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SPADE)).withCustomEnchant("Weakness", 2).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.IRON_AXE)).withCustomEnchant("Lightning", 1).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_AXE)).withCustomEnchant("Lightning", 1).withRareLore().withLife(five).store();
		
		// level 6
		SupplyItem.supplyItemLevel = 6;
		SupplyItem.from(KillSkillItems.getExperienceLoot(6)).store();
		SupplyItem.from(KillSkillItems.getLapisLoot(6)).store();
		SupplyItem.from(KillSkillItems.getIronLoot(6)).store();
		SupplyItem.from(KillSkillItems.getDiamondLoot(6)).store();
		SupplyItem.from(KillSkillItems.getHealPotionLoot(6)).store();
		SupplyItem.from(KillSkillItems.getBlazeLoot(6)).store();
		SupplyItem.from(KillSkillItems.getGrenadeItem(2)).store();
		SupplyItem.from(KillSkillItems.getTntItem(2)).store();
		SupplyItem.from(KillSkillItems.getCanonItem(2)).store();
		SupplyItem.from(KillSkillItems.getHeadItem(1)).store();
		SupplyItem.from(new ItemStack(Material.ENDER_PEARL, 2)).store();
		SupplyItem.from(new ItemStack(Material.IRON_PICKAXE)).withCustomEnchant("Blind", 2).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_PICKAXE)).withCustomEnchant("Blind", 3).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_PICKAXE)).withCustomEnchant("Blind", 2).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_PICKAXE)).withCustomEnchant("Blind", 3).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_AXE)).withCustomEnchant("Berserking", 2).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.IRON_AXE)).withCustomEnchant("Berserking", 3).withRareLore().withLife(quarter).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_AXE)).withCustomEnchant("Berserking", 2).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_AXE)).withCustomEnchant("Berserking", 3).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_HOE)).withCustomEnchant("Lifesteal", 2).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_HOE)).withCustomEnchant("Lifesteal", 3).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_HOE)).withCustomEnchant("Lifesteal", 2).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_HOE)).withCustomEnchant("Lifesteal", 3).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_HOE)).withCustomEnchant("Poison", 2).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_HOE)).withCustomEnchant("Poison", 3).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_HOE)).withCustomEnchant("Poison", 2).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_HOE)).withCustomEnchant("Poison", 3).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_PICKAXE)).withCustomEnchant("Slowing", 2).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_PICKAXE)).withCustomEnchant("Slowing", 3).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_PICKAXE)).withCustomEnchant("Slowing", 2).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_PICKAXE)).withCustomEnchant("Slowing", 3).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_SPADE)).withCustomEnchant("Weakness", 2).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_SPADE)).withCustomEnchant("Weakness", 3).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SPADE)).withCustomEnchant("Weakness", 2).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SPADE)).withCustomEnchant("Weakness", 3).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_AXE)).withCustomEnchant("Lightning", 2).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_AXE)).withCustomEnchant("Lightning", 2).withRareLore().withLife(five).store();
		
		// level 7
		SupplyItem.supplyItemLevel = 7;
		SupplyItem.from(KillSkillItems.getExperienceLoot(7)).store();
		SupplyItem.from(KillSkillItems.getLapisLoot(7)).store();
		SupplyItem.from(KillSkillItems.getIronLoot(7)).store();
		SupplyItem.from(KillSkillItems.getDiamondLoot(7)).store();
		SupplyItem.from(KillSkillItems.getHealPotionLoot(7)).store();
		SupplyItem.from(KillSkillItems.getBlazeLoot(7)).store();
		SupplyItem.from(KillSkillItems.getGrenadeItem(3)).store();
		SupplyItem.from(KillSkillItems.getTntItem(3)).store();
		SupplyItem.from(KillSkillItems.getCanonItem(3)).store();
		SupplyItem.from(KillSkillItems.getHeadItem(2)).store();
		SupplyItem.from(new ItemStack(Material.ENDER_PEARL, 3));
		SupplyItem.from(new ItemStack(Material.IRON_PICKAXE)).withCustomEnchant("Blind", 3).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_PICKAXE)).withCustomEnchant("Blind", 4).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_PICKAXE)).withCustomEnchant("Blind", 3).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_PICKAXE)).withCustomEnchant("Blind", 4).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_AXE)).withCustomEnchant("Berserking", 3).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.IRON_AXE)).withCustomEnchant("Berserking", 4).withRareLore().withLife(quarter).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_AXE)).withCustomEnchant("Berserking", 3).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_AXE)).withCustomEnchant("Berserking", 4).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_HOE)).withCustomEnchant("Lifesteal", 3).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_HOE)).withCustomEnchant("Lifesteal", 4).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_HOE)).withCustomEnchant("Lifesteal", 3).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_HOE)).withCustomEnchant("Lifesteal", 4).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_HOE)).withCustomEnchant("Poison", 3).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_HOE)).withCustomEnchant("Poison", 4).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_HOE)).withCustomEnchant("Poison", 3).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_HOE)).withCustomEnchant("Poison", 4).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_PICKAXE)).withCustomEnchant("Slowing", 3).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_PICKAXE)).withCustomEnchant("Slowing", 4).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_PICKAXE)).withCustomEnchant("Slowing", 3).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_PICKAXE)).withCustomEnchant("Slowing", 4).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_SPADE)).withCustomEnchant("Weakness", 3).withRareLore().withLife(mid).store();
		SupplyItem.from(new ItemStack(Material.IRON_SPADE)).withCustomEnchant("Weakness", 4).withRareLore().withLife(third).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SPADE)).withCustomEnchant("Weakness", 3).withRareLore().withLife(tenth).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_SPADE)).withCustomEnchant("Weakness", 4).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.IRON_AXE)).withCustomEnchant("Lightning", 3).withRareLore().withLife(five).store();
		SupplyItem.from(new ItemStack(Material.DIAMOND_AXE)).withCustomEnchant("Lightning", 3).withRareLore().withLife(five).store();
			
		
		
		// BOW
		SupplyItem.supplyItemType = SupplyItemType.BOW;
		
		// level 1
		SupplyItem.supplyItemLevel = 1;
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 1, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 1, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 1, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 1, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 1, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 1, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 1, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 1, true).item().build()).store();

		// level 2
		SupplyItem.supplyItemLevel = 2;
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 2, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 2, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 2, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 2, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 2, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 2, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 2, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 2, true).item().build()).store();
		
		// level 3
		SupplyItem.supplyItemLevel = 3;
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).item().build()).store();
		
		// level 4
		SupplyItem.supplyItemLevel = 4;
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).item().build()).store();
		
		// level 5
		SupplyItem.supplyItemLevel = 5;
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 1, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 1, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 1, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 1, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 1, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 1, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 1, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 1, true).item().build()).store();
		
		// level 6
		SupplyItem.supplyItemLevel = 6;
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 2, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 2, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 2, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 2, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 2, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 2, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 2, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 2, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_FIRE, 1, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_FIRE, 1, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_FIRE, 1, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_FIRE, 1, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_FIRE, 1, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_FIRE, 1, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_FIRE, 1, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 3, true).withEnchant(Enchantment.ARROW_FIRE, 1, true).item().build()).store();
		
		// level 7
		SupplyItem.supplyItemLevel = 7;
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 2, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 2, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 2, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 2, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 2, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 2, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 2, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).withEnchant(Enchantment.ARROW_KNOCKBACK, 2, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).withEnchant(Enchantment.ARROW_FIRE, 1, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).withEnchant(Enchantment.ARROW_FIRE, 1, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).withEnchant(Enchantment.ARROW_FIRE, 1, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).withEnchant(Enchantment.ARROW_FIRE, 1, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).withEnchant(Enchantment.ARROW_FIRE, 1, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).withEnchant(Enchantment.ARROW_FIRE, 1, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).withEnchant(Enchantment.ARROW_FIRE, 1, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 4, true).withEnchant(Enchantment.ARROW_FIRE, 1, true).item().build()).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 5, true).item().build()).withLife(tenth).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 5, true).item().build()).withLife(third).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 5, true).item().build()).withLife(quarter).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 5, true).item().build()).withLife(mid).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 5, true).item().build()).withLife(twoThirds).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 5, true).item().build()).withLife(threeQuarters).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 5, true).item().build()).withLife(eighty).store();
		SupplyItem.from(new ItemBuilder(Material.BOW).buildMeta().withEnchant(Enchantment.ARROW_DAMAGE, 5, true).item().build()).store();

		
		
		// POTION
		SupplyItem.supplyItemType = SupplyItemType.POTION;
		
		// level 1
		SupplyItem.supplyItemLevel = 1;
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 8261)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 8194)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 8227)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 8203)).store();

		
		// level 2
		SupplyItem.supplyItemLevel = 2;
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 8229)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 8258)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 8259)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 16453)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 8235)).store();
		
		// level 3
		SupplyItem.supplyItemLevel = 3;
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 8226)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 16419)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 16421)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 8193)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 16386)).store();
		
		// level 4
		SupplyItem.supplyItemLevel = 4;
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 16385)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 16417)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 8257)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 16450)).store();
		
		// level 5
		SupplyItem.supplyItemLevel = 5;
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 16388)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 16449)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 8225)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 16418)).store();
		
		// level 6
		SupplyItem.supplyItemLevel = 6;
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 8201)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 16452)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 16393)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 16460)).store();
		
		// level 7
		SupplyItem.supplyItemLevel = 7;
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 16452)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 8233)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 8265)).store();
		SupplyItem.from(new ItemStack(Material.POTION, 1, (short) 16428)).store();

		// Chesk that store is filled correctly
		for(SupplyItemType type : SupplyItemType.values()){
			for(int i=1 ; i<=7 ; i++){
				Validate.isTrue(store.get(type).get(i).size() > 0, "List of type "+type.name()+" and level "+i+" is empty");
			}
		}
		

		// Chesk that chances are filled correctly
		Validate.isTrue(chances.size() == 7, "chances map hasn't a size of 7");
		for(int L=1; L<=7 ; L++){
			List<Integer> list = chances.get(L);
			int size = list.size();
			Validate.isTrue(size == 100, "list L="+L+" hasn't a size of 100");
		}
		
		Log.info("Successfuly loaded supply items in "+(System.currentTimeMillis()-start)+"ms");
		
	}
	
	private void addToList(List<Integer> list, Integer number, int amount){
		for(int i=0 ; i<amount ; i++){
			list.add(number);
		}
	}
	
	
	private static class SupplyItem{
		private static SupplyItemStore supplyItemStore;
		private static SupplyItemType supplyItemType;
		private static int supplyItemLevel = 1;
		private static Map<Material,Integer> durabilities;
		
		static {
			// initialize durabilities map used to simplify calculations
			durabilities = new HashMap<>();
			durabilities.put(Material.IRON_HELMET, 166);
			durabilities.put(Material.IRON_CHESTPLATE, 242);
			durabilities.put(Material.IRON_LEGGINGS, 226);
			durabilities.put(Material.IRON_BOOTS, 196);
			durabilities.put(Material.DIAMOND_HELMET, 364);
			durabilities.put(Material.DIAMOND_CHESTPLATE, 529);
			durabilities.put(Material.DIAMOND_LEGGINGS, 496);
			durabilities.put(Material.DIAMOND_BOOTS, 430);
			durabilities.put(Material.IRON_SWORD, 251);
			durabilities.put(Material.IRON_AXE, 251);
			durabilities.put(Material.IRON_PICKAXE, 251);
			durabilities.put(Material.IRON_SPADE, 251);
			durabilities.put(Material.IRON_HOE, 251);
			durabilities.put(Material.DIAMOND_SWORD, 1562);
			durabilities.put(Material.DIAMOND_AXE, 1562);
			durabilities.put(Material.DIAMOND_PICKAXE, 1562);
			durabilities.put(Material.DIAMOND_SPADE, 1562);
			durabilities.put(Material.DIAMOND_HOE, 1562);
			durabilities.put(Material.BOW, 385);
			durabilities.put(Material.FISHING_ROD, 65);
		}
		
		private ItemStack itemStack;
		
		private SupplyItem(ItemStack itemStack){
			this.itemStack = itemStack;
		}
		
		public static SupplyItem from(ItemStack itemStack){
			return new SupplyItem(itemStack);
		}
		
		/**
		 * Get the durability to apply to an item stack
		 * to make it so the remaining "life" of the item
		 * will match the given percentage of the max 
		 * durability of this material.
		 * @param material
		 * @param percentage
		 * @return
		 */
		public SupplyItem withLife(int percentage){
			Integer maxDurability = durabilities.get(itemStack.getType());
			if(maxDurability != null){
				itemStack.setDurability((short) (maxDurability - (Math.round((double) maxDurability * (double) percentage / 100d))));
			}
			return this;
		}
		
		public <T extends CustomEnchantment> SupplyItem withCustomEnchant(String enchantName, int level){
			EnchantmentAPI.getEnchantment(enchantName).addToItem(itemStack, level);
			FakeGlowEnchant.addGlow(itemStack);
			return this;
		}
		
		public SupplyItem withRareLore(){
			String rareLore = supplyItemStore.api.getStringsAPI().get("killskill.supply.rare-lore").toString();
			String pickupRareLore = supplyItemStore.api.getStringsAPI().get("killskill.supply.pickup-rare-lore").toString();
			ItemMeta meta = itemStack.getItemMeta();
			List<String> lore = new ArrayList<>();
			if(meta.hasLore())
				lore = meta.getLore();
			lore.add(rareLore);
			lore.add(pickupRareLore);
			meta.setLore(lore);
			itemStack.setItemMeta(meta);
			return this;
		}
		
		/**
		 * Store an item in the internal map, by type and level
		 * @param type
		 * @param level (between 1 and 7)
		 * @param item (not null and not air)
		 */
		public void store(){
			supplyItemStore.store.get(supplyItemType).get(supplyItemLevel).add(itemStack);
		}
		
	}
}
