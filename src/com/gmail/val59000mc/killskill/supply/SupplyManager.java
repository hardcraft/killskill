package com.gmail.val59000mc.killskill.supply;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.math.RandomUtils;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.block.Chest;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.craftbukkit.libs.jline.internal.Log;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.Firework;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.entity.ThrownExpBottle;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.FireworkMeta;
import org.bukkit.util.Vector;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.killskill.common.Constants;
import com.gmail.val59000mc.killskill.events.KillSkillSupplyDropEvent;
import com.gmail.val59000mc.killskill.events.KillSkillSupplyExpireEvent;
import com.gmail.val59000mc.killskill.events.KillSkillSupplyOpenEvent;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.gmail.val59000mc.spigotutils.Time;
import com.gmail.val59000mc.spigotutils.particles.ParticleEffect;
import com.gmail.val59000mc.spigotutils.particles.ParticleEffect.BlockData;
import com.google.common.collect.Sets;

import de.inventivegames.hologram.Hologram;
import de.inventivegames.hologram.HologramAPI;

public class SupplyManager implements SupplyApi{

	private HCGameAPI api;
	
	// config
	private List<Location> dropLocations;
	private Long lastSupplyChest;
	private int secondsBeforeExpiration;
	private SupplyItemStore itemStore;
	
	public SupplyManager(HCGameAPI api){
		this.api = api;
		this.dropLocations = new ArrayList<>();
		this.itemStore = new SupplyItemStore(api);
		this.lastSupplyChest = 0L;
		load();
		startSupplyTask();
	}

	private void load() {
		
		FileConfiguration cfg = api.getConfig();
		
		// load locations
		List<String> locationsStr = cfg.getStringList("supply.drop-locations");
		World world = api.getWorldConfig().getWorld();
		if(locationsStr == null){
			dropLocations.add(api.getWorldConfig().getCenter());
		}else{
			for(String locationStr : locationsStr){
				Location location = Parser.parseLocation(world, locationStr);
				dropLocations.add(world.getHighestBlockAt(location).getLocation().add(0, 100, 0));
			}
		}
		
		// load expiration
		this.secondsBeforeExpiration = cfg.getInt("supply.seconds-before-expiration", Constants.DEFAULT_SECONDS_BEFORE_SUPPLY_EXPIRATION);
	}

	private void startSupplyTask() {
		
		api.buildTask("supply task 30s", new HCTask() {
			
			private boolean checkDropSupply(){
				int nbPlayers = api.getPlayersManagerAPI().getPlayers(true, PlayerState.PLAYING).size();
				
				if(nbPlayers < 5)
					return false;
				
				long waitMillis;
				
				if(nbPlayers >= 20){
					waitMillis = 900000L; // 15 min
				}else if(nbPlayers >= 15){
					waitMillis = 1050000L; // 17.5 min
				}else{
					waitMillis = 1200000L; // 20 min
				}
				
				Long now = System.currentTimeMillis();
				
				return now - lastSupplyChest >= waitMillis;
			}
			
			@Override
			public void run() {
				if(checkDropSupply()){
					
					api.getStringsAPI().get("killskill.supply.pre-alert-new-supply").sendChatP();
					
					api.buildTask("supply pre alert sound", new HCTask() { 
						public void run() { 
							api.getSoundAPI().play(Sound.ORB_PICKUP, 1, 0.5f);
						}
					})
					.withInterval(1)
					.withIterations(30)
					.build()
					.start();
					
					lastSupplyChest = System.currentTimeMillis();
					
					api.sync(()->{
						if(api.getPlayersManagerAPI().getPlayers(true, PlayerState.PLAYING).size() > 0){
							dropRandomSupply();
						}
					}, 600); // 30 s
					
				}
			}
		})
		.withInterval(1200) // 60s
		.withDelay(200) // 10 s
		.build()
		.start();
	}

	///////////
	// API
	/////////////
	
	@Override
	public SupplyChest dropRandomSupply() {
		Location location = getRandomDropLocation();
		SupplyChest fallingSupply = new SupplyChestImpl(location);
		
		// check if supply chest is allowed, if another supply chest is processing, deny the dropping and return null
		KillSkillSupplyDropEvent event = new KillSkillSupplyDropEvent(api, fallingSupply);
		api.getPlugin().getServer().getPluginManager().callEvent(event);
		if(event.isCancelled())
			return null;
		
		api.registerListener((SupplyChestImpl) fallingSupply);
		alertNewSupply(location);
		fallingSupply.drop();
		
		return fallingSupply;
	}

	@Override
	public void handleLandingChest(SupplyChest chest) {
		api.getSoundAPI().play(Sound.ZOMBIE_WOODBREAK, 2, 1);

		Location chestLocation = chest.getGroundLocation();
		
		api.getStringsAPI()
			.get("killskill.supply.landed")
			.replace("%x%", String.valueOf(chestLocation.getBlockX()))
			.replace("%z%", String.valueOf(chestLocation.getBlockZ()))
			.sendChatP();
		
		
		for(int i = 0 ; i < 625 ; i+=125){
			TNTPrimed tnt = (TNTPrimed) chestLocation.getWorld().spawnEntity(chestLocation,EntityType.PRIMED_TNT);
			tnt.setFuseTicks(20);
			tnt.setIsIncendiary(false);
			tnt.setYield(3);
			tnt.setVelocity(new Vector(Math.cos((double) i / 100d), 1, Math.sin((double) i / 100d)).multiply(0.4));
		}
		
		spawnExpirationHologram(chest);
		
		
	}

	private void spawnExpirationHologram(SupplyChest chest) {

		Location location = chest.getGroundLocation().add(0,2,0);
		Hologram hologram = HologramAPI.createHologram(location, "");
		hologram.spawn();
		
		
		api.buildTask("supply chest expiration hologram armor-stand", new HCTask() {
			
			private int iterations = 0;
			private int remainingTime = secondsBeforeExpiration;
			private Set<Integer> broadcastExpireAtRemainingTime = Sets.newHashSet(5,10,30,45,60);
			
			private void updateHologram(){
				ChatColor color;
				if(remainingTime <= 15){
					color = ChatColor.RED;
				}else if(remainingTime <= 30){
					color = ChatColor.GOLD;
				}else if(remainingTime <= 60){
					color = ChatColor.YELLOW;
				}else{
					color = ChatColor.GREEN;
				}
				
				int minutes = (remainingTime - remainingTime%60) / 60;
				int seconds = remainingTime%60;
				
				StringBuilder builder = new StringBuilder();
				builder.append(color);
				builder.append(String.format("%02d", minutes));
				builder.append(":");
				builder.append(String.format("%02d", seconds));
				
				hologram.setText(builder.toString());
			}
			
			private void spawnFirework(){
		        Firework fw = (Firework) location.getWorld().spawnEntity(location, EntityType.FIREWORK);
		        FireworkMeta fwm = fw.getFireworkMeta();
		       
		        fwm.addEffect(FireworkEffect.builder()
	        		.withColor(Color.fromRGB(255,255,0))
	        		.withFade(Color.fromRGB(255, 255, 255))
	        		.with(Type.BALL_LARGE)
	        		.build()
		        );
		        
		        fwm.setPower(2);
		        fw.setFireworkMeta(fwm);  
			}
			
			@Override
			public void run() {
				
				if(remainingTime == 0){
					KillSkillSupplyExpireEvent expireChest = new KillSkillSupplyExpireEvent(api, chest);
					api.getPlugin().getServer().getPluginManager().callEvent(expireChest);
					hologram.despawn();
					scheduler.stop();
				}else{
					
					if(iterations%10 == 0){
						chest.displayTargetOnGround();
					}
					
					if(iterations%40 == 0){
						spawnFirework();
					}
					
					if(iterations%20 == 0){
						remainingTime--;
						if(broadcastExpireAtRemainingTime.contains(remainingTime)){
							api.getStringsAPI()
								.get("killskill.supply.expire-in")
								.replace("%time%", Time.getFormattedTime(remainingTime))
								.sendChatP();
							api.getSoundAPI().play(Sound.STEP_WOOD, 2, 2);
						}
						updateHologram();
					}
					
				}
				
				iterations++;
			}
			
		})
		.addListener(new HCTaskListener(){

			@EventHandler
		    public void onOtherDrop(KillSkillSupplyDropEvent e){
				e.setCancelled(true);
		    }
			
			@EventHandler
			public void onOpenChest(KillSkillSupplyOpenEvent e){
				if(e.getSupplyChest().equals(chest)){
					hologram.despawn();
					getScheduler().stop();
				}
			}
			
		})
		.withInterval(1)
		.build()
		.start();
		
	}

	@Override
	public void handleOpenChest(SupplyChest supplyChest, KillSkillPlayer ksPlayer) {
		api.getStringsAPI()
			.get("killskill.supply.picked-up-by")
			.replace("%player%", ksPlayer.getName())
			.sendChatP();
		ksPlayer.addSupplyOpened();
		
		if(supplyChest.getBlock() != null){
			Chest chest = supplyChest.getBlock();
			Location ground = supplyChest.getGroundLocation();
			chest.getBlock().setType(Material.AIR);	
			List<ItemStack> items = itemStore.getRandomSupplyItems(ksPlayer);
			animateDropSupplyItems(ground, items);
			
		}
		
	}

	/**
	 * This method will make the chest bounce in the air,
	 * display some particles , play some sounds,
	 * then make the chest crash on the ground and eject neraby players.
	 * The items will be scaterred evenly around the crash location
	 * @param location
	 * @param items
	 */
	@SuppressWarnings("deprecation")
	private void animateDropSupplyItems(Location location, List<ItemStack> items) {
		World world = location.getWorld();
		FallingBlock fallingBlock = (FallingBlock) world.spawnFallingBlock(location, 58, (byte) 2);
		api.buildTask("animate drop supply items", new HCTask() {

			private double angle = 0;
			
			@Override
			public void run() {
				ThrownExpBottle thrownExp = (ThrownExpBottle) world.spawnEntity(fallingBlock.getLocation(), EntityType.THROWN_EXP_BOTTLE);
				thrownExp.setVelocity(new Vector(Math.cos(angle),-0.1,Math.sin(angle)));
				angle += 0.314;
				if(fallingBlock.getVelocity().getY() > 0){
					ParticleEffect.LAVA.display(1f, 1f, 1f, 0.2f, 10, fallingBlock.getLocation().clone().subtract(0, 0.5, 0), 50);
				}
			}
		})
		.withFirstCallback(new HCTask() {
			
			@Override
			public void run() {
				fallingBlock.setVelocity(new Vector(0,0.8,0));
				api.getSoundAPI().play(Sound.FIREWORK_LAUNCH, location, 2, 2);
			}
			
		})
		.withInterval(2)
		.addListener(new HCTaskListener(){
			
			@EventHandler
		    public void onOtherDrop(KillSkillSupplyDropEvent e){
				e.setCancelled(true);
		    }
			
			@EventHandler
		    public void onFallingBlockLand(EntityChangeBlockEvent e){
		        if(e.getEntity().equals(fallingBlock)){
		        	getScheduler().stop();
		        	for(ItemStack itemStack : items){
	        			Item item = world.dropItem(location, itemStack);
	        			item.setVelocity(new Vector(Math.cos(10d*RandomUtils.nextDouble()), 1, Math.sin(10d*RandomUtils.nextDouble())).multiply(0.2));
		        	}

		        	// play some sounds and effects
		    		api.getSoundAPI().play(Sound.ZOMBIE_WOODBREAK, location, 2, 1);
		        	api.getSoundAPI().play(Sound.EXPLODE, location, 2, 2);
					ParticleEffect.EXPLOSION_HUGE.display(3f, 3f, 3f, 1F, 10, location, 300);
					ParticleEffect.BLOCK_CRACK.display(new BlockData(Material.WOOL, (byte) 4), 3f, 3f, 3f, 1F, 10, location, 300);
					
					// eject nearby players from the chest landing location
					for(Entity entity : world.getNearbyEntities(location, 8, 8, 8)){
						if(entity.getType().equals(EntityType.PLAYER)){
							Player player = (Player) entity;
							Vector direction = player.getLocation().toVector().subtract(location.toVector());
							direction = direction.normalize();
							direction = direction.setY(0.3);
							player.setVelocity(direction.multiply(3));
	        				
						}
					}
					
		        	api.sync(()->{
			        	e.getBlock().setType(Material.AIR);
		        	});
		        }
		    }
			
		})
		// security if for whatever reason the chest doesn't touch the ground
		.withLastCallback(new HCTask() {
			
			@Override
			public void run() {
				scheduler.stop();
				if(fallingBlock.isValid()){
					fallingBlock.remove();
				}else{
					fallingBlock.getLocation().getBlock().setType(Material.AIR);
				}
			}
		})
		.withIterations(100) // max fall duration 10 sec
		.build()
		.start();
	}

	@Override
	public void handleExpireChest(SupplyChest supplyChest) {
		api.getStringsAPI()
			.get("killskill.supply.expired")
			.sendChatP();
		api.getSoundAPI().play(Sound.CHEST_CLOSE, 2, 2);
		if(supplyChest.getBlock() != null){
			Log.debug("clear chest");
			Chest chest = supplyChest.getBlock();
			chest.getBlock().setType(Material.AIR);
		}
		
	}
	
	
	/////////////
	// Internal
	//////////////
	
	private Location getRandomDropLocation(){
		return dropLocations.get(Randoms.randomInteger(0, dropLocations.size()-1));
	}
	
	private void alertNewSupply(Location location){
		
		api.getStringsAPI()
			.get("killskill.supply.alert-new-supply")
			.replace("%x%", String.valueOf(location.getBlockX()))
			.replace("%z%", String.valueOf(location.getBlockZ()))
			.sendChatP();
		
		api.buildTask("supply alert sound", new HCTask() {
			
			@Override
			public void run() {
				api.getSoundAPI().play(Sound.NOTE_PLING, 1, 2);
			
			}
		})
		.withInterval(5)
		.withIterations(7)
		.build()
		.start();
		
	}
}
