package com.gmail.val59000mc.killskill.grenades;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.killskill.items.KillSkillItems;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.ItemCompareOption;


public class GrenadeManager {
	
	private HCGameAPI api;
	
	public GrenadeManager(HCGameAPI api){
		this.api = api;
	}


	public void launchGrenade(KillSkillPlayer ksPlayer) {
		if(ksPlayer.isPlaying()){
			Player player = ksPlayer.getPlayer();
			Grenade grenade = new Grenade(api, ksPlayer);
			grenade.start();
			Inventories.remove(player.getInventory(), KillSkillItems.getGrenadeItem(1),  new ItemCompareOption.Builder().withLore(true).build());
		}
	}
}
