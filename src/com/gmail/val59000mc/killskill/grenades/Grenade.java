package com.gmail.val59000mc.killskill.grenades;

import java.lang.reflect.Field;

import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftTNTPrimed;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.killskill.common.Constants;
import com.gmail.val59000mc.killskill.items.KillSkillItems;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;

import net.minecraft.server.v1_8_R3.EntityLiving;
import net.minecraft.server.v1_8_R3.EntityTNTPrimed;

public class Grenade extends HCListener{

	private KillSkillPlayer ksPlayer;
	private HCGameAPI api;
	private Item grenade;
	
	public Grenade(HCGameAPI api, KillSkillPlayer ksPlayer){
		this.api = api;
		this.ksPlayer = ksPlayer;
	}
	
	public void start(){
		if(ksPlayer.isOnline()){

			api.registerListener(this);

			Player player = ksPlayer.getPlayer();
			
			api.getStringsAPI()
				.get("killskill.grenade.launched")
				.sendChatP(ksPlayer);
			
			Vector direction = player.getEyeLocation().getDirection().normalize();
			
			grenade = player.getWorld().dropItem(player.getEyeLocation().subtract(0, 0.2, 0),  KillSkillItems.getThrownGrenadeItem());
			grenade.setVelocity(direction.multiply(1.5));
			grenade.setCustomNameVisible(true);
			
			api.getSoundAPI().play(Sound.IRONGOLEM_HIT, player.getLocation(), 1, 2);
			
			api.buildTask("grenade launched by "+ksPlayer.getName(), new HCTask() {

				private int countdown = 3;
				
				@Override
				public void run() {
					grenade.setCustomName("§e§l[ §6"+countdown+" §e§l]");
					api.getSoundAPI().play(Sound.NOTE_STICKS, player.getLocation(), 1, 1);
					countdown--;
				}
				
			})
			.withIterations(3)
			.withInterval(20)
			.withLastCallback(new HCTask() {
				
				@Override
				public void run() {
					Location location = grenade.getLocation();
					grenade.remove();

					
					for(int i = 0 ; i < 625 ; i+=125){
						TNTPrimed tnt =spawnTnt(player, location, 20, 5);
						tnt.setVelocity(new Vector(Math.cos((double) i / 100d), 1, Math.sin((double) i / 100d)).multiply(0.4));
					}
					
					scheduler.stop();
					
				}
				
				private TNTPrimed spawnTnt(Player owner, Location location, int fuseTicks, float yield){
					
					// Spawn a tnt
					TNTPrimed tnt = (TNTPrimed) location.getWorld().spawnEntity(location,EntityType.PRIMED_TNT);
					tnt.setFuseTicks(fuseTicks);
					tnt.setIsIncendiary(false);
					tnt.setYield(yield);
					tnt.setMetadata(Constants.GRENADE_METADATA, new FixedMetadataValue(getPlugin(), null));
					
					// Change via NMS the source of the TNT by the player
					EntityLiving nmsEntityLiving = (((CraftLivingEntity) player).getHandle());
					EntityTNTPrimed nmsTNT = (((CraftTNTPrimed) tnt).getHandle());
					nmsTNT.setInvisible(true);
					try {
					    Field source = EntityTNTPrimed.class.getDeclaredField("source");
					    source.setAccessible(true);
					    source.set(nmsTNT, nmsEntityLiving);
					} catch (Exception ex) {
					    ex.printStackTrace();
					}
				
					return tnt;
					
				}
				
			}).addListener(new HCTaskListener(){
				
				@EventHandler
				public void onEntityDamage(PlayerPickupItemEvent e){
					if(e.getItem().equals(grenade)){
						e.setCancelled(true);
					}
				}
				
			})
			.build()
			.start();
			
		}
	}
}
