package com.gmail.val59000mc.killskill.players;

import org.bukkit.entity.Player;

import com.gmail.val59000mc.hcgameslib.players.HCPlayer;

public class KillSkillPlayer extends HCPlayer{

	// pvp and spawn logger
	private long lastPVP = 0;
	private boolean isPvpLogging;
	
	// perks
	private int experienceAmount;
	private int lapisChance;
	private int potionChance;
	private int ironChance;
	private int diamondChance;
	private int blazeChance;
	private int headChance;
	private int supplyAmount;
	private int supplyLevel;
	
	
	// stats
	private int experienceLooted;
	private int lapisLooted;
	private int potionLooted;
	private int ironLooted;
	private int diamondLooted;
	private int blazeLooted;
	private int headLooted;
	private int grenadeLooted;
	private int tntLooted;
	private int cannonLooted;
	private int supplyOpened;
	
	public KillSkillPlayer(Player player) {
		super(player);
		
		// pvp logger
		this.lastPVP = 0;
		this.isPvpLogging = false;
		
		// perks
		this.experienceAmount = 1;
		this.lapisChance = 5;
		this.potionChance = 20;
		this.ironChance = 5;
		this.diamondChance = 5;
		this.blazeChance = 6;
		this.headChance = 1;
		this.supplyAmount = 2;
		this.supplyLevel = 1;
		
		//stats
		this.experienceLooted = 0;
		this.lapisLooted = 0;
		this.potionLooted = 0;
		this.ironLooted = 0;
		this.diamondLooted = 0;
		this.blazeLooted = 0;
		this.headLooted = 0;
		this.grenadeLooted = 0;
		this.tntLooted = 0;
		this.cannonLooted = 0;
		this.supplyOpened = 0;
	}
	
	public KillSkillPlayer(HCPlayer hcPlayer) {
		super(hcPlayer);
		KillSkillPlayer ksPlayer = (KillSkillPlayer) hcPlayer;

		// pvp logger
		this.lastPVP = ksPlayer.lastPVP;
		this.isPvpLogging = ksPlayer.isPvpLogging;

		// perks
		this.experienceAmount = ksPlayer.experienceAmount;
		this.lapisChance = ksPlayer.lapisChance;
		this.potionChance = ksPlayer.potionChance;
		this.ironChance = ksPlayer.ironChance;
		this.diamondChance = ksPlayer.diamondChance;
		this.blazeChance = ksPlayer.blazeChance;
		this.headChance = ksPlayer.headChance;
		this.supplyAmount = ksPlayer.supplyAmount;
		this.supplyLevel = ksPlayer.supplyLevel;
		
		//stats
		this.experienceLooted = ksPlayer.experienceLooted;
		this.lapisLooted = ksPlayer.lapisLooted;
		this.potionLooted = ksPlayer.potionLooted;
		this.ironLooted = ksPlayer.ironLooted;
		this.diamondLooted = ksPlayer.diamondLooted;
		this.blazeLooted = ksPlayer.blazeLooted;
		this.headLooted = ksPlayer.headLooted;
		this.grenadeLooted = ksPlayer.grenadeLooted;
		this.tntLooted = ksPlayer.tntLooted;
		this.cannonLooted = ksPlayer.cannonLooted;
		this.supplyOpened = ksPlayer.supplyOpened;
	}
	
	public void subtractBy(HCPlayer lastUpdatedSession) {
		super.subtractBy(lastUpdatedSession);
		KillSkillPlayer ksPlayer = (KillSkillPlayer) lastUpdatedSession;

		//stats
		this.experienceLooted -= ksPlayer.experienceLooted;
		this.lapisLooted -= ksPlayer.lapisLooted;
		this.potionLooted -= ksPlayer.potionLooted;
		this.ironLooted -= ksPlayer.ironLooted;
		this.diamondLooted -= ksPlayer.diamondLooted;
		this.blazeLooted -= ksPlayer.blazeLooted;
		this.headLooted -= ksPlayer.headLooted;
		this.grenadeLooted -= ksPlayer.grenadeLooted;
		this.tntLooted -= ksPlayer.tntLooted;
		this.cannonLooted -= ksPlayer.cannonLooted;
		this.supplyOpened -= ksPlayer.supplyOpened;
	}
	
	public long remainingFightSeconds(){
		long remaining = System.currentTimeMillis() - this.lastPVP;
		if(remaining <= 60000){
			return ((60000-remaining)/1000);
		}else{
			return 0;
		}
	}
	
	public long remainingBeforeSpawnSeconds(){
		long remaining = System.currentTimeMillis() - this.lastPVP;
		if(remaining <= 8000){
			return ((8000-remaining)/1000);
		}else{
			return 0;
		}
	}
	
	public void refreshLastPVP(){
		this.lastPVP = System.currentTimeMillis();
	}
	
	public boolean isPvpLogging(){
		return isPvpLogging;
	}
	
	public void setIsPvpLogging(boolean isPvpLogging){
		this.isPvpLogging = isPvpLogging;
	}
	
	public void resetLastPVP(){
		this.lastPVP = 0;
	}
	
	public int getExperienceAmount() {
		return experienceAmount;
	}

	public void setExperienceAmount(int experienceAmount) {
		this.experienceAmount = experienceAmount;
	}

	public int getLapisChance() {
		return lapisChance;
	}

	public void setLapisChance(int lapisChance) {
		this.lapisChance = lapisChance;
	}

	public int getPotionChance() {
		return potionChance;
	}

	public void setPotionChance(int potionChance) {
		this.potionChance = potionChance;
	}

	public int getIronChance() {
		return ironChance;
	}

	public void setIronChance(int ironChance) {
		this.ironChance = ironChance;
	}

	public int getDiamondChance() {
		return diamondChance;
	}

	public void setDiamondChance(int diamondChance) {
		this.diamondChance = diamondChance;
	}
	
	public int getBlazeChance() {
		return blazeChance;
	}

	public void setBlazeChance(int blazeChance) {
		this.blazeChance = blazeChance;
	}

	public int getHeadChance() {
		return headChance;
	}

	
	public int getSupplyAmount() {
		return supplyAmount;
	}

	public void setSupplyAmount(int supplyAmount) {
		this.supplyAmount = supplyAmount;
	}

	public int getSupplyLevel() {
		return supplyLevel;
	}
	public void setSupplyLevel(int supplyLevel) {
		this.supplyLevel = supplyLevel;
	}

	public int getExperienceLooted() {
		return experienceLooted;
	}
	public void addExperienceLooted(int amount){
		this.experienceLooted += amount;
	}

	public int getLapisLooted() {
		return lapisLooted;
	}
	public void addLapisLooted(){
		this.lapisLooted++;
	}

	public int getPotionLooted() {
		return potionLooted;
	}
	public void addPotionLooted(){
		this.potionLooted++;
	}

	public int getIronLooted() {
		return ironLooted;
	}
	public void addIronLooted(){
		this.ironLooted++;
	}

	public int getDiamondLooted() {
		return diamondLooted;
	}
	public void addDiamondLooted(){
		this.diamondLooted++;
	}

	public int getBlazeLooted() {
		return blazeLooted;
	}
	public void addBlazeLooted(){
		this.blazeLooted++;
	}

	public int getHeadLooted() {
		return headLooted;
	}
	public void addHeadLooted(){
		this.headLooted++;
	}

	public int getSupplyOpened() {
		return supplyOpened;
	}
	public void addSupplyOpened(){
		this.supplyOpened++;
	}

	public int getGrenadeLooted() {
		return grenadeLooted;
	}
	public void addGrenandeLooted(int amount){
		this.grenadeLooted += amount;
	}

	public int getTntLooted() {
		return tntLooted;
	}
	public void addTntLooted(int amount){
		this.tntLooted += amount;
	}

	public int getCannonLooted() {
		return cannonLooted;
	}
	public void addCannonLooted(int amount){
		this.cannonLooted += amount;
	}
	
}
