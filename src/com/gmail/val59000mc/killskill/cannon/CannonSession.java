package com.gmail.val59000mc.killskill.cannon;

import static java.lang.Math.PI;

import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftArmorStand;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.util.Vector;
import org.spigotmc.event.entity.EntityDismountEvent;

import com.gmail.val59000mc.hcgameslib.events.HCPlayerKilledByPlayerEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerKilledEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerQuitEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerStateChangeEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.hcgameslib.tasks.HCTaskScheduler;
import com.gmail.val59000mc.killskill.common.Constants;
import com.gmail.val59000mc.killskill.items.KillSkillItems;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.ItemCompareOption;

public class CannonSession extends HCListener{

	private Block slab;
	private ArmorStand support;
	private Vector direction;
	private KillSkillPlayer ksPlayer;
	private HCTaskScheduler actionBarTask;
	private CannonManager cannonManager;
	
	public CannonSession(CannonManager cannonManager, KillSkillPlayer ksPlayer, Block slab, Vector direction){
		this.cannonManager = cannonManager;
		this.ksPlayer = ksPlayer;
		this.slab = slab;
		this.direction = direction;
	}
	
	public void startSession(){
		
		// spawn support and set passenger
		support = (ArmorStand) getApi().getWorldConfig().getWorld().spawnEntity(slab.getLocation().add(0.5,-0.5,0.5).add(direction.normalize().multiply(0.5)), EntityType.ARMOR_STAND);
		net.minecraft.server.v1_8_R3.EntityArmorStand supportNMS = (((CraftArmorStand) support).getHandle());
		supportNMS.setInvisible(true);
		supportNMS.setGravity(false);
		support.setGravity(false);
		support.setPassenger(ksPlayer.getPlayer());
		
		// action bar
		actionBarTask = getApi().buildTask("cannonball action bar", new HCTask() {
			
			@Override
			public void run() {
				getStringsApi()
					.get("killskill.cannon.help-bar")
					.sendActionBar(ksPlayer,3);
			}
		})
		.withInterval(60)
		.withIterations(-1)
		.build();
		actionBarTask.start();
		
		// title
		getStringsApi()
			.get("killskill.cannon.firing-mode-on")
			.sendTitle(ksPlayer, 15, 20, 15);
		
		// reload cannon sounds
		getSoundApi().play(ksPlayer, Sound.ANVIL_BREAK,1,2f);
		
	}
	
	private void stopSession(){
		
		support.remove();
		
		// stop action bar
		actionBarTask.stop();

		// message
		getStringsApi()
			.get("killskill.cannon.firing-mode-off")
			.sendChatP(ksPlayer);
		
		getApi().unregisterListener(this);
		
		cannonManager.endSession(this);
					
	}
	
	
	public Block getSlab() {
		return slab;
	}

	public ArmorStand getArmorStand() {
		return support;
	}

	public KillSkillPlayer getKsPlayer() {
		return ksPlayer;
	}

	@EventHandler
	public void onDismount(EntityDismountEvent e){
		if(e.getEntity().getUniqueId().equals(ksPlayer.getUuid())){
			ArmorStand cannon = cannonManager.getCannon(this);
			Player player = (Player) e.getEntity();
			float yaw = player.getLocation().getYaw();
			if(cannon != null){
				Location dest = cannon.getLocation().add(0,0.75,0);
				dest.setYaw(yaw);
				dest.setPitch(0);
				Bukkit.getScheduler().runTaskLater(getPlugin(), new Runnable() {
					@Override
					public void run() {
						player.teleport(dest);
					}
				}, 1);
			}
			stopSession();
		}
	}
	

	@EventHandler
	public void onQuit(HCPlayerQuitEvent e){
		if(e.getHcPlayer().equals(ksPlayer)){
			stopSession();
		}
	}

	@EventHandler
	public void onChangeState(HCPlayerStateChangeEvent e){
		if(e.getHcPlayer().equals(ksPlayer)){
			stopSession();
		}
	}

	@EventHandler
	public void onKilled(HCPlayerKilledEvent e){
		if(e.getHcPlayer().equals(ksPlayer)){
			stopSession();
		}
	}
	

	@EventHandler
	public void onKilled(HCPlayerKilledByPlayerEvent e){
		if(e.getHCKilled().equals(ksPlayer)){
			stopSession();
		}
	}
	
	@EventHandler
	public void onFire(PlayerInteractEvent e){
		if(e.getPlayer().getUniqueId().equals(ksPlayer.getUuid())){
			if(e.getAction().equals(Action.RIGHT_CLICK_AIR)){
				if(KillSkillItems.isCannonItem(e.getItem())){
					
					// check if facing the correct direction
					Location pLoc = e.getPlayer().getLocation();
					Vector pVector = pLoc.getDirection();
					double angle = direction.angle(pVector);
					double angleLimit = 0.45*PI;
					if(angle < -angleLimit || angle > angleLimit){
						getStringsApi()
							.get("killskill.cannon.wrong-firing-direction")
							.sendChat(ksPlayer);
						getSoundApi().play(ksPlayer, Sound.VILLAGER_NO, 1, 2);
					}else{
						getStringsApi()
							.get("killskill.cannon.ball-fired")
							.sendTitle(ksPlayer,0,8,0);
						getSoundApi().play(Sound.EXPLODE, e.getPlayer().getLocation(), 2, 2);
						getApi().getWorldConfig().getWorld().spigot().playEffect(support.getLocation(), Effect.EXPLOSION);
						Fireball fireball = (Fireball) pLoc.getWorld().spawnEntity(pLoc.add(pVector.normalize().multiply(2)), EntityType.FIREBALL);
						fireball.setIsIncendiary(false);
						fireball.setVelocity(pVector.normalize().multiply(3));
						fireball.setShooter(ksPlayer.getPlayer());
						fireball.setYield(6);
						fireball.setMetadata(Constants.CANNON_METADATA, new FixedMetadataValue(getPlugin(), null));
						Inventories.remove(e.getPlayer().getInventory(), KillSkillItems.getCanonItem(1), new ItemCompareOption.Builder().withLore(true).build());
					}
					
				}else{
					getStringsApi()
						.get("killskill.cannon.need-item-to-fire")
						.sendChatP(ksPlayer);
					getSoundApi().play(ksPlayer, Sound.VILLAGER_NO, 1, 2);
				}
			}
		}
	}
	
}
