package com.gmail.val59000mc.killskill.cannon;

import java.util.Optional;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.libs.jline.internal.Log;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.util.Vector;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.hcgameslib.listeners.HCTaskListener;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.killskill.events.KillSkillStartCannonSessionEvent;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.spigotutils.Blocks;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Sets;

public class CannonManager {
	
	private HCGameAPI api;
	
	private BiMap<ArmorStand,CannonSession> sessions;
	
	public CannonManager(HCGameAPI api){
		this.api = api;
		this.sessions = HashBiMap.create(8);
	}

	public boolean isCannon(Entity entity) {
		return entity.getType().equals(EntityType.ARMOR_STAND)
				&& entity.getCustomName() != null
				&& entity.getCustomName().equals("Tirer");
	}

	private boolean isOccupied(ArmorStand entity) {
		return sessions.containsKey(entity);
	}

	private boolean isUsingACannon(KillSkillPlayer ksPLayer) {
		return sessions.containsValue(ksPLayer);
	}


	public void placeInFiringPosition(KillSkillPlayer ksPlayer, ArmorStand armorStandWithName) {
		if(ksPlayer.isOnline()){

			if(isUsingACannon(ksPlayer)){
				api.getStringsAPI()
				.get("killskill.cannon.already-firing")
				.sendChatP(ksPlayer);
			}else if(isOccupied(armorStandWithName)){
				api.getStringsAPI()
					.get("killskill.cannon.occupied")
					.sendChatP(ksPlayer);
			}else{
				
				KillSkillStartCannonSessionEvent event = new KillSkillStartCannonSessionEvent(api,ksPlayer);
				Bukkit.getPluginManager().callEvent(event);
				if(!event.isCancelled()){

					@SuppressWarnings("deprecation")
					// find quartz slab nearby
					Optional<Block> optionalSlab = Blocks.findNearbyBlocks(armorStandWithName.getLocation(), 4, Sets.newHashSet(Material.STEP))
						.stream()
						.filter(block -> block.getData() == (byte) 7)
						.findFirst();
					if(optionalSlab.isPresent()){
						Block slab = optionalSlab.get();
						Vector direction = slab.getLocation().add(0.5,0,0.5).toVector().subtract(armorStandWithName.getLocation().toVector());
						CannonSession session = new CannonSession(this,ksPlayer, slab, direction);
						sessions.put(armorStandWithName, session);
						api.registerListener(session);
						session.startSession();
					}else{
						Log.warn("Couldn't find quartz slab near armor stand");
					}
					
				}
			}	
			
		}	
	}

	public void endSession(CannonSession cannonSession) {
		sessions.inverse().remove(cannonSession);
		api.buildTask("cooldown player cannon", new HCTask() {
			
			@Override
			public void run() {
				// TODO Auto-generated method stub
				
			}
		}).addListener(new HCTaskListener() {

			private KillSkillPlayer ksPlayer = cannonSession.getKsPlayer();
			
			@EventHandler
			public void onStartCannonSession(KillSkillStartCannonSessionEvent e){
				if(e.getKsPlayer().equals(ksPlayer)){
					getStringsApi()
						.get("killskill.cannon.wait-5s")
						.sendChatP(ksPlayer);
					e.setCancelled(true);
				}
			}
		})
		.withDelay(100)
		.withIterations(1)
		.build()
		.start();
	}

	public ArmorStand getCannon(CannonSession cannonSession) {
		return sessions.inverse().get(cannonSession);
	}
}
