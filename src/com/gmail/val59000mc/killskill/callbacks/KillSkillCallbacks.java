package com.gmail.val59000mc.killskill.callbacks;

import java.util.Iterator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.hcgameslib.api.HCStringsAPI;
import com.gmail.val59000mc.hcgameslib.api.impl.DefaultPluginCallbacks;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.HCTeam;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.stuff.Stuff;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.hcgameslib.worlds.WorldConfig;
import com.gmail.val59000mc.hcgameslib.worlds.WorldManager;
import com.gmail.val59000mc.killskill.bonuses.PortalBonusManager;
import com.gmail.val59000mc.killskill.cannon.CannonManager;
import com.gmail.val59000mc.killskill.common.Constants;
import com.gmail.val59000mc.killskill.crafts.CraftManager;
import com.gmail.val59000mc.killskill.grenades.GrenadeManager;
import com.gmail.val59000mc.killskill.items.KillSkillItems;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.killskill.stats.ScoreboardStats;
import com.gmail.val59000mc.killskill.stats.ScoreboardType;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Parser;
import com.gmail.val59000mc.spigotutils.items.ItemBuilder;
import com.google.common.collect.Lists;

public class KillSkillCallbacks extends DefaultPluginCallbacks{

	private LocationBounds lobbyBounds;
	private PortalBonusManager portalBonusManager;
	private CannonManager cannonManager;
	private GrenadeManager grenadeManager;
	private ScoreboardStats scoreboardStats;
	private CraftManager craftManager;
	
	public LocationBounds getLobbyBounds(){
		return lobbyBounds;
	}
	
	public PortalBonusManager getPortalBonusManager(){
		return portalBonusManager;
	}

	public CannonManager getCannonManager() {
		return cannonManager;
	}
	
	public GrenadeManager getGrenadeManager() {
		return grenadeManager;
	}	

	public CraftManager getCraftManager() {
		return craftManager;
	}

	@Override
	public HCPlayer newHCPlayer(Player player){
		return new KillSkillPlayer(player);
	}
	
	@Override
	public HCPlayer newHCPlayer(HCPlayer hcPlayer){
		return new KillSkillPlayer(hcPlayer);
	}
	
	@Override
	public List<HCTeam> createTeams() {
		return Lists.newArrayList(
				newHCTeam("Défaut", ChatColor.GRAY, null)
		);
	}
	

	@Override
	public List<Stuff> createStuffs() {
		
		return Lists.newArrayList(
				
			new Stuff.Builder(Constants.MEMBER_STUFF_NAME)
				.addArmorItem(new ItemStack(Material.GOLD_HELMET))
				.addArmorItem(
					new ItemBuilder(Material.GOLD_CHESTPLATE)
						.buildMeta()
						.withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, false)
						.item()
						.build()
				)
				.addArmorItem(new ItemStack(Material.GOLD_LEGGINGS))
				.addArmorItem(
					new ItemBuilder(Material.GOLD_BOOTS)
						.buildMeta()
						.withEnchant(Enchantment.DEPTH_STRIDER, 3, false)
						.item()
						.build()
				)
				.addInventoryItem(new ItemStack(Material.IRON_SWORD))
				.addInventoryItem(new ItemStack(Material.BOW))
				.addInventoryItem(new ItemStack(Material.ARROW,64))
				.build(),
				

			new Stuff.Builder(Constants.VIP_STUFF_NAME)
				.addArmorItem(new ItemStack(Material.CHAINMAIL_HELMET))
				.addArmorItem(
					new ItemBuilder(Material.CHAINMAIL_CHESTPLATE)
						.buildMeta()
						.withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, false)
						.item()
						.build()
				)
				.addArmorItem(new ItemStack(Material.CHAINMAIL_LEGGINGS))
				.addArmorItem(
					new ItemBuilder(Material.CHAINMAIL_BOOTS)
						.buildMeta()
						.withEnchant(Enchantment.DEPTH_STRIDER, 3, false)
						.item()
						.build()
				)
				.addInventoryItem(new ItemStack(Material.IRON_SWORD))
				.addInventoryItem(new ItemStack(Material.BOW))
				.addInventoryItem(new ItemStack(Material.ARROW,64))
				.build(),
				

			new Stuff.Builder(Constants.VIP_PLUS_STUFF_NAME)
				.addArmorItem(
						new ItemBuilder(Material.CHAINMAIL_HELMET)
						.buildMeta()
						.withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, false)
						.item()
						.build()
				)
				.addArmorItem(
					new ItemBuilder(Material.CHAINMAIL_CHESTPLATE)
						.buildMeta()
						.withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, false)
						.item()
						.build()
				)
				.addArmorItem(
						new ItemBuilder(Material.CHAINMAIL_LEGGINGS)
						.buildMeta()
						.withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, false)
						.item()
						.build()
				)
				.addArmorItem(
					new ItemBuilder(Material.CHAINMAIL_BOOTS)
						.buildMeta()
						.withEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, false)
						.withEnchant(Enchantment.DEPTH_STRIDER, 3, false)
						.item()
						.build()
				)
				.addInventoryItem(
					new ItemBuilder(Material.IRON_SWORD)
						.buildMeta()
						.withEnchant(Enchantment.DAMAGE_ALL, 1, false)
						.item()
						.build()
				)
				.addInventoryItem(new ItemStack(Material.BOW))
				.addInventoryItem(new ItemStack(Material.ARROW,64))
				.build()
	    );
	}
	

	@Override
	public WorldConfig configureWorld(WorldManager worldManager) {
		
		KillSkillItems.setup(getApi());
		
		World killskill = Bukkit.getWorlds().get(0);
		
		Location lobby = Parser.parseLocation(killskill, getConfig().getString("config.world.lobby"));
		Location center = Parser.parseLocation(killskill, getConfig().getString("config.world.center"));
		
		Location minLobby = Parser.parseLocation(killskill, getConfig().getString("lobby-bounds.min"));
		Location maxLobby = Parser.parseLocation(killskill, getConfig().getString("lobby-bounds.max"));
		
		this.lobbyBounds = new LocationBounds(minLobby, maxLobby);
		this.portalBonusManager = new PortalBonusManager(getApi());
		this.cannonManager = new CannonManager(getApi());
		this.grenadeManager = new GrenadeManager(getApi());
		this.scoreboardStats = new ScoreboardStats(getApi());
		this.craftManager = new CraftManager(getApi());
		craftManager.registerCrafts();
		
		killskill.setGameRuleValue("doDaylightCycle", "true");
		killskill.setGameRuleValue("commandBlockOutput", "false");
		killskill.setGameRuleValue("logAdminCommands", "false");
		killskill.setGameRuleValue("sendCommandFeedback", "false");
		killskill.setGameRuleValue("doMobSpawning", "false");
		killskill.setGameRuleValue("randomTickSpeed", "0");
		killskill.setGameRuleValue("doFireTick", "false");
		killskill.setTime(6000);
		killskill.setStorm(false);
		killskill.setWeatherDuration(999999999);
		killskill.setDifficulty(Difficulty.HARD);
		killskill.setSpawnLocation(lobby.getBlockX(), lobby.getBlockY(), lobby.getBlockZ());
		killskill.setKeepSpawnInMemory(false);
		killskill.save();
		
		scoreboardStats.update(scoreboardType.next());
		
		getApi().buildTask("next scoreboard type", new HCTask() {
			
			@Override
			public void run() {
				scoreboardType = scoreboardType.next();
				getPmApi().updatePlayersScoreboards();
				scoreboardStats.update(scoreboardType.next());
			}
		})
		.withDelay(400) // 20s
		.withInterval(400) // 20s
		.build()
		.start();
		
		getApi().buildTask("remove arrows on ground", new HCTask() {
			
			@Override
			public void run() {
				for(Entity entity : killskill.getEntities()){
					if(entity.getType().equals(EntityType.ARROW)){
						entity.remove();
					}
				}
				
			}
		})
		.withInterval(12000) // 10 min
		.withDelay(300)
		.build()
		.start();
		

		return new WorldConfig
			.Builder(killskill)
			.withLobby(lobby)
			.withCenter(center)
			.build();
	}

	private ScoreboardType scoreboardType = ScoreboardType.OWN_STATS;
	
	@Override
	public List<String> updatePlayingScoreboard(HCPlayer hcPlayer) {
		
		KillSkillPlayer ksPlayer = (KillSkillPlayer) hcPlayer;
		
		switch(scoreboardType){
			case MONTH_STATS:
				return scoreboardStats.getMonthsStats(ksPlayer);
			case ONLINE_STATS:
				return scoreboardStats.getOnlineStats(ksPlayer);
			case OWN_MONTH_STATS:
				return scoreboardStats.getOwnMonthStats(ksPlayer);
			default:
			case OWN_STATS:
				return scoreboardStats.getOwnStats(ksPlayer);
		}
		
	}
	
	@Override
	public void waitPlayerAtLobby(HCPlayer hcPlayer) {
		// should not be called in a continuous game
		if(hcPlayer.isOnline()){
			Player player = hcPlayer.getPlayer();
			player.teleport(getApi().getWorldConfig().getLobby());
		}
	}
	
	@Override
	public void relogPlayingPlayer(HCPlayer hcPlayer) {
		if(hcPlayer.isOnline()){
			getStringsApi()
				.get("killskill.player-has-joined-the-game")
				.replace("%player%", hcPlayer.getName())
				.replace("%count%", String.valueOf(getApi().getPlayersManagerAPI().getPlayersCount(true, PlayerState.PLAYING)))
				.replace("%total%",  String.valueOf(getApi().getPlayersManagerAPI().getMaxPlayers()))
				.sendChatP();
			
			KillSkillPlayer ksPlayer = (KillSkillPlayer) hcPlayer;
			ksPlayer.resetLastPVP();
			ksPlayer.setIsPvpLogging(false);
			Player player = hcPlayer.getPlayer();
			player.setGameMode(GameMode.SURVIVAL);
			
			if(!player.hasPlayedBefore() || ksPlayer.getTimePlayed() == 0){
				player.teleport(getApi().getWorldConfig().getLobby());
			}
			
			player.setHealth(player.getMaxHealth());

			getStringsApi()
				.get("messages.welcome-player")
				.replace("%player%", hcPlayer.getName())
				.replace("%game%", getApi().getName())
				.sendChatP(hcPlayer);
		
			getStringsApi()
				.getRaw(ChatColor.GREEN+getApi().getName())
				.sendTitle(hcPlayer, 20, 30, 20);
		}
	}
	
	@Override
	public void spectatePlayer(HCPlayer hcPlayer) {
		if(hcPlayer.isOnline()){
			
			getStringsApi()
				.get("killskill.spec.spec-help-message")
				.sendChatP(hcPlayer);
			
			Player player = hcPlayer.getPlayer();
	
			player.teleport(getApi().getWorldConfig().getCenter());

			getSoundApi().play(hcPlayer, Sound.ENDERMAN_TELEPORT, 1, 1.5f);
		}
	}
	
	@Override
	public void vanishedPlayer(HCPlayer hcPlayer) {
		
		getStringsApi().get("messages.vanished-title").sendTitle(hcPlayer,0,80,0);
		getStringsApi().get("messages.vanished-bar").sendActionBar(hcPlayer);

		getStringsApi()
			.get("killskill.spec.vanish-help-message")
			.sendChatP(hcPlayer);
		
		if(hcPlayer.isOnline()){
			hcPlayer.getPlayer().teleport(getApi().getWorldConfig().getCenter());
		}
		getSoundApi().play(hcPlayer, Sound.ENDERMAN_TELEPORT, 1, 1.5f);
		
	}
	
	@Override
	public void startPlayer(HCPlayer hcPlayer) {
		// Nothing : should not be called in a continuous game
	}
	
	@Override
	public void assignStuffToPlayer(HCPlayer hcPlayer){
		hcPlayer.setStuff(null);
	}
	
	@Override
	public void respawnPlayer(HCPlayer hcPlayer) {

		if(hcPlayer.isOnline()){
			hcPlayer.getPlayer().setSaturation(20);
			((KillSkillPlayer) hcPlayer).resetLastPVP();
		}
		
	}
	
	@Override
	public void handleDeathEvent(PlayerDeathEvent event, HCPlayer hcKilled) {		
		removeUnwantedDrops(event.getDrops());
		event.setKeepLevel(true);
		event.setDroppedExp(0);
		getPmApi().autoRespawnPlayerAfter20Ticks(event.getEntity());
	}
	/**
	 * Modify the death event if needed
	 * Default: respawn player after 20 ticks
	 * @param event
	 * @param hcPlayer
	 */
	@Override
	public void handleKillEvent(PlayerDeathEvent event, HCPlayer hcKilled, HCPlayer hcKiller) {		
		removeUnwantedDrops(event.getDrops());
		getPmApi().autoRespawnPlayerAfter20Ticks(event.getEntity());
		if(!((KillSkillPlayer) hcKilled).isPvpLogging()){
			getPmApi().rewardMoneyTo(hcKiller, getKillReward(hcKiller));
			event.getDrops().addAll(KillSkillItems.getRewardLoots((KillSkillPlayer) hcKiller));
		}
		event.setKeepLevel(true);
		event.setDroppedExp(0);
	}

	@Override
	public void formatDeathMessage(HCPlayer hcKilled, HCPlayer hcKiller, PlayerDeathEvent event) {
		if(hcKilled.getLastDamagerEntity() != null){
			Entity entity = hcKilled.getLastDamagerEntity();
			if(!entity.getMetadata(Constants.GRENADE_METADATA).isEmpty()){
				event.setDeathMessage(getStringsApi().get("killskill.grenade.death-message")
					.replace("%killer%",hcKiller.getColor()+hcKiller.getName())
					.replace("%killed%",hcKilled.getColor()+hcKilled.getName())
					.toString());
			}else if (!entity.getMetadata(Constants.CANNON_METADATA).isEmpty()){
				event.setDeathMessage(getStringsApi().get("killskill.cannon.death-message")
					.replace("%killer%",hcKiller.getColor()+hcKiller.getName())
					.replace("%killed%",hcKilled.getColor()+hcKilled.getName())
					.toString());
			}else if (!entity.getMetadata(Constants.TNT_METADATA).isEmpty()){
				event.setDeathMessage(getStringsApi().get("killskill.tnt.death-message")
					.replace("%killer%",hcKiller.getColor()+hcKiller.getName())
					.replace("%killed%",hcKilled.getColor()+hcKilled.getName())
					.toString());
			}else{
				super.formatDeathMessage(hcKilled, hcKiller, event);
			}
		}else{
			super.formatDeathMessage(hcKilled, hcKiller, event);
		}
	}
	
	public void removeUnwantedDrops(List<ItemStack> drops){

		Iterator<ItemStack> iterator = drops.iterator();
		while(iterator.hasNext()){
			ItemStack stack = iterator.next();
			switch(stack.getType()){
				case LEATHER_BOOTS:
				case LEATHER_CHESTPLATE:
				case LEATHER_LEGGINGS:
				case LEATHER_HELMET:
				case GOLD_HELMET:
				case GOLD_CHESTPLATE:
				case GOLD_LEGGINGS:
				case GOLD_BOOTS:
				case GLASS_BOTTLE:
				case STONE_SWORD:
				case JACK_O_LANTERN:
				case ARROW:
					iterator.remove();
					break;
				case BOW:
				case IRON_SWORD:
					if(stack.getEnchantments().isEmpty())
						iterator.remove();
					break;
				default:
					break;
			}
		}
	}
	
	@Override
	public void alertGameIsFull(HCPlayer hcPlayer) {
		getStringsApi().get("killskill.game-is-full").sendChatP(hcPlayer);
		getSoundApi().play(hcPlayer, Sound.VILLAGER_NO, 2f , 1.2f);
	}
	
	@Override
	public Location getNextRespawnLocation(HCPlayer hcPlayer) {
		return getApi().getWorldConfig().getLobby();
	}
	
	@Override
	public com.gmail.val59000mc.simpleinventorygui.items.Item.ItemBuilder getSpecInvPlayerItem(HCPlayer hcPlayer){
		
		HCStringsAPI s = getStringsApi();
		
		KillSkillPlayer ksPlayer = (KillSkillPlayer) hcPlayer;
		
//		killskill:
//		  spec:
//			    loots: "Loots"
//			    exp-loot: "Bouteille d'XP"
//			    lapis-chance: "Lapis"
//			    potion-chance: "Potion de soin"
//			    iron-chance: "Lingot de fer"
//			    diamond-chance: "Diamant"
//			    blaze-chance: "Poudre de blaze"
			    	
			    	
		return new com.gmail.val59000mc.simpleinventorygui.items.Item.ItemBuilder(Material.SKULL_ITEM)
			.withDamage((short) 3)
			.withName(hcPlayer.getColor()+hcPlayer.getName())
			.withPlayerSkullName(hcPlayer.getName())
			.withLore(Lists.newArrayList(
				" ",
				s.get("messages.spec.player").toString()+hcPlayer.getName(),
				s.get("messages.spec.kills").toString()+hcPlayer.getKills(),
				s.get("messages.spec.deaths").toString()+hcPlayer.getDeaths(),
				" ",
				s.get("killskill.spec.loots").toString(),
				s.get("killskill.spec.exp-loot").toString()+ksPlayer.getExperienceAmount(),
				s.get("killskill.spec.lapis-chance").toString()+ksPlayer.getLapisChance()+"%",
				s.get("killskill.spec.potion-chance").toString()+ksPlayer.getPotionChance()+"%",
				s.get("killskill.spec.iron-chance").toString()+ksPlayer.getIronChance()+"%",
				s.get("killskill.spec.diamond-chance").toString()+ksPlayer.getDiamondChance()+"%",
				s.get("killskill.spec.blaze-chance").toString()+ksPlayer.getBlazeChance()+"%",
				" ",
				s.get("messages.spec.tp").toString()
				)
			);
	}
}
