package com.gmail.val59000mc.killskill.items;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import java.util.regex.Pattern;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.hcgameslib.api.HCGameAPI;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.spigotutils.Randoms;
import com.gmail.val59000mc.spigotutils.items.ItemBuilder;
import com.google.common.collect.Lists;

public class KillSkillItems {

	private static HCGameAPI api;
	
	public static void setup(HCGameAPI gameApi){
		api = gameApi;
	}
	
	public static boolean isHeadItem(ItemStack item){
		if(item != null 
			&& item.getType().equals(Material.SKULL_ITEM) 
			&& item.getDurability() == (short) 3 
			&& item.hasItemMeta()
			&& item.getItemMeta().getLore() != null){
			List<String> lore = Lists.newArrayList(api.getStringsAPI().get("killskill.items.head-lore").toString().split(Pattern.quote("|")));
			return item.getItemMeta().getLore().containsAll(lore);
		}
		return false;
	}

	public static boolean isCannonItem(ItemStack item){
		if(item != null 
			&& item.getType().equals(Material.FIREBALL) 
			&& item.hasItemMeta()
			&& item.getItemMeta().getLore() != null){
			List<String> lore = Lists.newArrayList(api.getStringsAPI().get("killskill.items.cannonball-lore").toString().split(Pattern.quote("|")));
			return item.getItemMeta().getLore().containsAll(lore);
		}
		return false;
	}

	public static boolean isTntItem(ItemStack item){
		if(item != null 
			&& item.getType().equals(Material.TNT) 
			&& item.hasItemMeta()
			&& item.getItemMeta().getLore() != null){
			List<String> lore = Lists.newArrayList(api.getStringsAPI().get("killskill.items.tnt-lore").toString().split(Pattern.quote("|")));
			return item.getItemMeta().getLore().containsAll(lore);
		}
		return false;
	}

	public static boolean isGrenadeItem(ItemStack item){
		if(item != null 
			&& item.getType().equals(Material.MONSTER_EGG) 
			&& item.getDurability() == (short) 50
			&& item.hasItemMeta()
			&& item.getItemMeta().getLore() != null){
			List<String> lore = Lists.newArrayList(api.getStringsAPI().get("killskill.items.grenade-lore").toString().split(Pattern.quote("|")));
			return item.getItemMeta().getLore().containsAll(lore);
		}
		return false;
	}
	
	public static ItemStack getHeadItem(int amount){
		return new ItemBuilder(Material.SKULL_ITEM)
				.withDurability(3)
				.withAmount(amount)
				.buildMeta()
				.withDisplayName(api.getStringsAPI().get("killskill.items.head-name").toString())
				.withLore(api.getStringsAPI().get("killskill.items.head-lore").toString().split(Pattern.quote("|")))
				.item()
				.build();
	}
	
	public static ItemStack getGrenadeItem(int amount){
		return new ItemBuilder(Material.MONSTER_EGG)
				.withDurability(50)
				.withAmount(amount)
				.buildMeta()
				.withDisplayName(api.getStringsAPI().get("killskill.items.grenade-name").toString())
				.withLore(api.getStringsAPI().get("killskill.items.grenade-lore").toString().split(Pattern.quote("|")))
				.item()
				.build();
	}
	
	public static ItemStack getThrownGrenadeItem(){
		return new ItemBuilder(Material.MONSTER_EGG)
				.withDurability(50)
				.withAmount(1)
				.buildMeta()
				.withLore(Lists.newArrayList(UUID.randomUUID().toString()))
				.item()
				.build();
	}
	
	public static ItemStack getTntItem(int amount){
		return new ItemBuilder(Material.TNT)
				.withAmount(amount)
				.buildMeta()
				.withDisplayName(api.getStringsAPI().get("killskill.items.tnt-name").toString())
				.withLore(api.getStringsAPI().get("killskill.items.tnt-lore").toString().split(Pattern.quote("|")))
				.item()
				.build();
	}
	
	public static ItemStack getCanonItem(int amount){
		return new ItemBuilder(Material.FIREBALL)
				.withAmount(amount)
				.buildMeta()
				.withDisplayName(api.getStringsAPI().get("killskill.items.cannonball-name").toString())
				.withLore(api.getStringsAPI().get("killskill.items.cannonball-lore").toString().split(Pattern.quote("|")))
				.item()
				.build();
	}
	
	public static ItemStack getExperienceLoot(int amount){
		return new ItemStack(Material.EXP_BOTTLE, amount);
	}
	
	public static ItemStack getLapisLoot(int amount){
		return new ItemStack(Material.INK_SACK, amount, (short) 4);
	}
	
	public static ItemStack getHealPotionLoot(int amount){
		return new ItemStack(Material.POTION, amount, (short) 8229);
	}
	
	public static ItemStack getIronLoot(int amount){
		return new ItemStack(Material.IRON_INGOT, amount);
	}
	
	public static ItemStack getDiamondLoot(int amount){
		return new ItemStack(Material.DIAMOND, amount);
	}
	
	public static ItemStack getBlazeLoot(int amount){
		return new ItemStack(Material.BLAZE_POWDER, amount);
	}
	
	
	private static ItemStack getLoot(ItemStack stack, int chance){
		if(Randoms.randomInteger(1, 100) <= chance){
			return stack;
		}else{
			return null;
		}
	}

	public static Set<ItemStack> getRewardLoots(KillSkillPlayer ksPlayer) {
		Set<ItemStack> loots = new HashSet<ItemStack>();
		
		ItemStack exp = getExperienceLoot(ksPlayer.getExperienceAmount());
		if(exp.getAmount() > 0){
			ksPlayer.addExperienceLooted(exp.getAmount());
			loots.add(exp);
		}
		
		ItemStack lapis = getLoot(getLapisLoot(1), ksPlayer.getLapisChance());
		if(lapis != null){
			ksPlayer.addLapisLooted();
			loots.add(lapis);
		}
		
		ItemStack potion = getLoot(getHealPotionLoot(1), ksPlayer.getPotionChance());
		if(potion != null){
			ksPlayer.addPotionLooted();
			loots.add(potion);
		}
		
		ItemStack iron = getLoot(getIronLoot(1), ksPlayer.getIronChance());
		if(iron != null){
			ksPlayer.addIronLooted();
			loots.add(iron);
		}
		
		ItemStack diamond = getLoot(getDiamondLoot(1), ksPlayer.getDiamondChance());
		if(diamond != null){
			ksPlayer.addDiamondLooted();
			loots.add(diamond);
		}
		
		ItemStack blaze = getLoot(getBlazeLoot(1), ksPlayer.getBlazeChance());
		if(blaze != null){
			ksPlayer.addBlazeLooted();
			loots.add(blaze);
		}
				
		ItemStack head = getLoot(getHeadItem(1), ksPlayer.getHeadChance());
		if(head != null){
			ksPlayer.addHeadLooted();
			loots.add(head);
		}
		
		
		return loots;
	}
}
