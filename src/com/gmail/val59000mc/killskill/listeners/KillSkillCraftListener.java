package com.gmail.val59000mc.killskill.listeners;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.killskill.callbacks.KillSkillCallbacks;
import com.gmail.val59000mc.killskill.crafts.CraftManager;
import com.gmail.val59000mc.spigotutils.Logger;

public class KillSkillCraftListener extends HCListener {

	private CraftManager craftManager;
	
	@EventHandler
	public void afterLoad(HCAfterLoadEvent e){
		Logger.setDebug(true);;
		craftManager = ((KillSkillCallbacks) getCallbacksApi()).getCraftManager();
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onUseItem(PlayerInteractEvent e){
		Action action = e.getAction();
		
		if(action == Action.LEFT_CLICK_AIR || action == Action.LEFT_CLICK_BLOCK){
			Player player = e.getPlayer();
			HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
			if(hcPlayer != null){
				ItemStack hand = player.getItemInHand();
				if(hand != null && hand.getType() != Material.AIR && hand.hasItemMeta()){
					if(craftManager.isRandomIronEnchantBook(hand)){
						craftManager.addMediumQualityEnchant(hand,hcPlayer);
						e.setCancelled(true);
					}else if(craftManager.isRandomDiamondEnchantBook(hand)){
						craftManager.addHighQualityEnchant(hand,hcPlayer);
						e.setCancelled(true);
					}else if(craftManager.isRandomIronPotion(hand)){
						craftManager.addMediumQualityEffect(hand,hcPlayer);
						e.setCancelled(true);
					}else if(craftManager.isRandomDiamondPotion(hand)){
						craftManager.addHighQualityEffect(hand,hcPlayer);
						e.setCancelled(true);
					}
				}
			}
		}

	}
}
