package com.gmail.val59000mc.killskill.listeners;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.hanging.HangingBreakEvent;
import org.bukkit.event.hanging.HangingPlaceEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerBucketEmptyEvent;
import org.bukkit.event.player.PlayerBucketFillEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.vehicle.VehicleDamageEvent;
import org.spigotmc.event.entity.EntityMountEvent;

import com.gmail.val59000mc.hcgameslib.events.HCBeforeContinuousEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerDamagedByHCPlayerEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerDamagedByPotionEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerDamagedEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.killskill.callbacks.KillSkillCallbacks;
import com.gmail.val59000mc.killskill.cannon.CannonManager;
import com.gmail.val59000mc.killskill.events.KillSkillTypeSpawnCommandEvent;
import com.gmail.val59000mc.killskill.grenades.GrenadeManager;
import com.gmail.val59000mc.killskill.items.KillSkillItems;
import com.gmail.val59000mc.killskill.melting.MeltingApi;
import com.gmail.val59000mc.killskill.melting.MeltingManager;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.Time;

public class KillSkillDamageAndInteractListener extends HCListener{

	private LocationBounds lobbyBounds;
	private CannonManager cannonManager;
	private GrenadeManager grenadeManager;
	private MeltingApi meltingManager;
	private Map<Block,Byte> anvils;
	
	
	
	// load lobby bounds
	@EventHandler
	public void onStartContinuous(HCBeforeContinuousEvent e){
		this.lobbyBounds = ((KillSkillCallbacks) getCallbacksApi()).getLobbyBounds();
		this.cannonManager = ((KillSkillCallbacks) getCallbacksApi()).getCannonManager();
		this.grenadeManager = ((KillSkillCallbacks) getCallbacksApi()).getGrenadeManager();
		this.meltingManager = new MeltingManager(getApi());
		this.anvils = new HashMap<Block,Byte>();
		
		getApi().buildTask("repair anvils", new HCTask() {
			
			@SuppressWarnings("deprecation")
			@Override
			public void run() {
				for(Iterator<Map.Entry<Block,Byte>> it = anvils.entrySet().iterator(); it.hasNext(); ) {
			        Entry<Block,Byte> entry = it.next();
			        if(!entry.getKey().getType().equals(Material.ANVIL)){
						Block anvil = entry.getKey();
						anvil.setType(Material.ANVIL);
						if(entry.getValue() % 2 == 0){
		                    anvil.setData((byte) 0);  //If the anvil's data is even, then it goes north/south. So setting it to 0 won't make a visual difference.
		                }else{
		                    anvil.setData((byte) 1); //If the anvils data is odd, then it goes east/west. So setting it's data to 1 will not make a visual difference.
		                }
						it.remove();
					}
			    }
				
			}
		})
		.withInterval(6000) // 5 minutes
		.build()
		.start();
	}
	
	// pvp logger
	@EventHandler(priority=EventPriority.LOWEST)
	public void onTypeSpawnCommand(KillSkillTypeSpawnCommandEvent e){
		long remainingFightSeconds = e.getKsPlayer().remainingBeforeSpawnSeconds();
		if(remainingFightSeconds > 0){
			getStringsApi()
				.get("killskill.command.spawn.cannot-go-to-spawn-while-fighting")
				.replace("%time%", Time.getFormattedTime(remainingFightSeconds))
				.sendChatP(e.getKsPlayer());
			e.setCancelled(true);
		}
	}
	
	// prevent firing arrow or potions in spawn
	@EventHandler
	public void onShootBow(EntityShootBowEvent e){
		if(e.getEntity().getType().equals(EntityType.PLAYER)){
			if(lobbyBounds.contains(((Player) e.getEntity()).getLocation())){
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onPlayerDamagedByPlayer(HCPlayerDamagedByHCPlayerEvent e){
		if(lobbyBounds.contains(e.getHcDamager().getPlayer().getLocation())
			|| lobbyBounds.contains(e.getHcDamaged().getPlayer().getLocation())){
			e.getWrapped().setCancelled(true);
		}else{
			KillSkillPlayer ksDamaged = ((KillSkillPlayer) e.getHcDamaged());
			if(ksDamaged.getPlayer() != null){
				if(ksDamaged.remainingFightSeconds() == 0){
					getStringsApi().get("killskill.now-combat-tagged").sendChatP(ksDamaged);
				}
				ksDamaged.refreshLastPVP();
			}
			
			KillSkillPlayer ksDamager = ((KillSkillPlayer) e.getHcDamager());
			if(ksDamager.getPlayer() != null){
				if(ksDamager.remainingFightSeconds() == 0){
					getStringsApi().get("killskill.now-combat-tagged").sendChatP(ksDamager);
				}
				ksDamager.refreshLastPVP();
			}
		}
	}
	
	@EventHandler
	public void onPlayerDamagedByPotion(HCPlayerDamagedByPotionEvent e){
		if(lobbyBounds.contains(e.getHcDamager().getPlayer().getLocation())
			|| lobbyBounds.contains(e.getHcDamaged().getPlayer().getLocation())){
			e.getWrapped().setCancelled(true);
		}else{

			KillSkillPlayer ksDamaged = ((KillSkillPlayer) e.getHcDamaged());
			if(ksDamaged.getPlayer() != null){
				if(ksDamaged.remainingFightSeconds() == 0){
					getStringsApi().get("killskill.now-combat-tagged").sendChatP(ksDamaged);
				}
				ksDamaged.refreshLastPVP();
			}
			
			KillSkillPlayer ksDamager = ((KillSkillPlayer) e.getHcDamager());
			if(ksDamager.getPlayer() != null){
				if(ksDamager.remainingFightSeconds() == 0){
					getStringsApi().get("killskill.now-combat-tagged").sendChatP(ksDamager);
				}
				ksDamager.refreshLastPVP();
			}
		}
	}
	

	@EventHandler
	public void onPlayerDamaged(HCPlayerDamagedEvent e){
		if(e.getWrapped().getCause().equals(DamageCause.FALL) || lobbyBounds.contains(e.getHcDamaged().getPlayer().getLocation())){
			e.getWrapped().setCancelled(true);
		}else{
			KillSkillPlayer ksPlayer = ((KillSkillPlayer) e.getHcDamaged());
			if(ksPlayer.getPlayer() != null){
				if(ksPlayer.remainingFightSeconds() == 0){
					getStringsApi().get("killskill.now-combat-tagged").sendChatP(ksPlayer);
				}
				ksPlayer.refreshLastPVP();
			}
		}
	}
	
	// do not rotate item frame
	@EventHandler
	public void onRotateItemFrame(PlayerInteractEntityEvent e){
		Entity entity = e.getRightClicked();
		if(entity.getType().equals(EntityType.ITEM_FRAME)){
			e.setCancelled(true);
		}
	}
	
	// prevent breaking hanging entity
	@EventHandler
	public void onBreakPainting(HangingBreakEvent e){
		e.setCancelled(true);
	}
	
	//prevent placing hanging entity
	@EventHandler
	public void onPlacePainting(HangingPlaceEvent e){
		e.setCancelled(true);
	}

	// do not damage item frame
	@EventHandler
	public void onDamageItemFrameOrPainting(EntityDamageEvent e){
		EntityType type = e.getEntityType();
		if(type.equals(EntityType.ITEM_FRAME)){
			e.setCancelled(true);
		}
	}

	// use tnt cannon
	@EventHandler
	public void onInteractAtEntity(PlayerInteractAtEntityEvent e){
		Entity entity = e.getRightClicked();
		if (cannonManager.isCannon(entity)){
			Player player = e.getPlayer();
			KillSkillPlayer ksPLayer = (KillSkillPlayer) getPmApi().getHCPlayer(player);
			if(ksPLayer != null){
				if(KillSkillItems.isCannonItem(player.getItemInHand())){
					cannonManager.placeInFiringPosition(ksPLayer, (ArmorStand) entity);
				}else{
					getStringsApi()
						.get("killskill.cannon.need-item-to-activate")
						.sendChatP(ksPLayer);
				}
			}
		}
	}
	
	@EventHandler
	public void onDamageMinecart(VehicleDamageEvent e){
		EntityType type = e.getVehicle().getType();
		if(type.equals(EntityType.MINECART) 
				|| type.equals(EntityType.MINECART_CHEST)
				|| type.equals(EntityType.MINECART_HOPPER) 
				|| type.equals(EntityType.MINECART_TNT)){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onUseBucket(PlayerBucketEmptyEvent e){
		e.setCancelled(true);
	}

	@EventHandler
	public void onUseBucket(PlayerBucketFillEvent e){
		e.setCancelled(true);
	}

	@EventHandler
	public void onUseBucket(PlayerBedEnterEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void onEnterVehicle(EntityMountEvent e){
		if(e.getEntity().getType().equals(EntityType.PLAYER) && e.getMount().getType().equals(EntityType.MINECART)){
			e.setCancelled(true);
		}
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
    public void onInteractAtBlock(PlayerInteractEvent event){
    	Player player = event.getPlayer();
    	
        if(event.getAction() == Action.RIGHT_CLICK_BLOCK){
        	
        	// repair anvil
            if(event.getClickedBlock().getType() == Material.ANVIL){
                Block anvil = event.getClickedBlock();
                int data = anvil.getData();
                if(data % 2 == 0){
                    anvil.setData((byte) 0);  //If the anvil's data is even, then it goes north/south. So setting it to 0 won't make a visual difference.
                }else{
                    anvil.setData((byte) 1); //If the anvils data is odd, then it goes east/west. So setting it's data to 1 will not make a visual difference.
                }
                if(!anvils.containsKey(anvil)){
                    anvils.put(anvil,new Byte((byte) data));
                }
                
            }else if(event.getClickedBlock().getType() == Material.FURNACE){

                // try melting crafted item
    			HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
            	if(hcPlayer != null && lobbyBounds.contains(player.getLocation())){
            		event.setCancelled(true);
            		meltingManager.attemptMelting(hcPlayer);
            	}
                
            }else if(event.hasItem() && event.getItem().getType().equals(Material.FIREBALL)){
            	event.setCancelled(true);
            }

        }
        
        if(event.getAction() == Action.LEFT_CLICK_BLOCK){
            // prevent lightning off fire
        	if(event.hasBlock() && event.getClickedBlock().getRelative(event.getBlockFace()).getType().equals(Material.FIRE)){
        		event.setCancelled(true);
        	}
        }
        
        if(event.getAction() == Action.PHYSICAL){
            // prevent jumping on soil
        	if(event.hasBlock() && event.getClickedBlock().getType().equals(Material.SOIL)){
        		event.setCancelled(true);
        	}
        }

        // launch grenace
        if(event.hasItem() && KillSkillItems.isGrenadeItem(event.getItem()) && !lobbyBounds.contains(player.getLocation())){
    		KillSkillPlayer ksPLayer = (KillSkillPlayer) getPmApi().getHCPlayer(player);
    		if(ksPLayer != null){
        		grenadeManager.launchGrenade(ksPLayer);
    		}
    	}
	
    }

	// cancel food level change
	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent event){
		event.setCancelled(true);
	}
	
	// cancel owner TNT damage
	@EventHandler(priority = EventPriority.LOW)
	public void doMoreDamageByTNT(EntityDamageByEntityEvent e){
		if(e.getEntity() instanceof Player && e.getDamager() instanceof TNTPrimed){
			e.setDamage(e.getDamage()*1.2d);
		}
	}
}
