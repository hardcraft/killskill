package com.gmail.val59000mc.killskill.listeners;

import org.bukkit.Bukkit;
import org.bukkit.command.PluginCommand;
import org.bukkit.event.EventHandler;

import com.gmail.val59000mc.hcgameslib.HCGamesLib;
import com.gmail.val59000mc.hcgameslib.commands.HCCommand;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;

public class KillSkillCommandOverrideListener extends HCListener{

	private HCCommand specExecutor;
	private HCCommand vanishExecutor;
	private HCCommand playExecutor;

	public KillSkillCommandOverrideListener(HCCommand specExecutor, HCCommand vanishExecutor, HCCommand playExecutor) {
		this.specExecutor = specExecutor;
		this.vanishExecutor = vanishExecutor;
		this.playExecutor = playExecutor;
	}

	@EventHandler
	public void afterLoad(HCAfterLoadEvent e){
		PluginCommand specCmd = Bukkit.getServer().getPluginCommand("spec");
		if(specCmd.getPlugin().equals(HCGamesLib.getPlugin())){
			specExecutor.setApi(getApi());
			specCmd.setExecutor(specExecutor);
		}
		

		PluginCommand vanishCmd = Bukkit.getServer().getPluginCommand("vanish");
		if(vanishCmd.getPlugin().equals(HCGamesLib.getPlugin())){
			vanishExecutor.setApi(getApi());
			vanishCmd.setExecutor(vanishExecutor);
		}

		PluginCommand playCmd = Bukkit.getServer().getPluginCommand("play");
		if(playCmd.getPlugin().equals(HCGamesLib.getPlugin())){
			playExecutor.setApi(getApi());
			playCmd.setExecutor(playExecutor);
		}
	}
	
	
}
