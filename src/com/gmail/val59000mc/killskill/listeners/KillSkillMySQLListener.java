
package com.gmail.val59000mc.killskill.listeners;

import java.sql.SQLException;

import javax.sql.rowset.CachedRowSet;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;

import com.gmail.val59000mc.hcgameslib.api.HCMySQLAPI;
import com.gmail.val59000mc.hcgameslib.common.Log;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBInsertedEvent;
import com.gmail.val59000mc.hcgameslib.database.HCPlayerDBPersistedSessionEvent;
import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.spigotutils.Logger;

public class KillSkillMySQLListener extends HCListener{

	// Queries
	private String createKillSkillPlayerSQL;
	private String createKillSkillPlayerStatsSQL;
	private String createKillSkillPlayerPerksSQL;
	private String insertKillSkillPlayerSQL;
	private String insertKillSkillPlayerStatsSQL;
	private String insertKillSkillPlayerPerksSQL;
	private String updateKillSkillPlayerGlobalStatsSQL;
	private String updateKillSKillPlayerStatsSQL;
	private String selectKillSKillPlayerPerksSQL;
	
	/**
	 * Load sql queries from resources files
	 */
	public void readQueries() {
		HCMySQLAPI sql = getMySQLAPI();
		if(sql.isEnabled()){
			createKillSkillPlayerSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/create_killskill_player.sql");
			createKillSkillPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/create_killskill_player_stats.sql");
			createKillSkillPlayerPerksSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/create_killskill_player_perks.sql");
			insertKillSkillPlayerSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/insert_killskill_player.sql");
			insertKillSkillPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/insert_killskill_player_stats.sql");
			insertKillSkillPlayerPerksSQL = sql.readQueryFromResource(getApi().getPlugin(),"resources/sql/insert_killskill_player_perks.sql");
			updateKillSkillPlayerGlobalStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/update_killskill_player_global_stats.sql");
			updateKillSKillPlayerStatsSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/update_killskill_player_stats.sql");
			selectKillSKillPlayerPerksSQL = sql.readQueryFromResource(getApi().getPlugin(), "resources/sql/select_killskill_player_perks.sql");
		}
	}
	
	/**
	 * Insert game if not exists
	 * @param e
	 */
	@EventHandler
	public void onGameFinishedLoading(HCAfterLoadEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		readQueries();
		
		if(sql.isEnabled()){
			
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {
					
					try{
					
						sql.execute(sql.prepareStatement(createKillSkillPlayerSQL));
						
						sql.execute(sql.prepareStatement(createKillSkillPlayerStatsSQL));
						
						sql.execute(sql.prepareStatement(createKillSkillPlayerPerksSQL));
						
					}catch(SQLException e){
						Logger.severe("Couldnt create tables for Kill Skill stats or perks");
						e.printStackTrace();
					}
					
				}
			});
		}
	}
	
	/**
	 * Add new player if not exists when joining
	 * Update name (because it may change)
	 * Update current played game 
	 * @param e
	 */
	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerJoin(HCPlayerDBInsertedEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		if(sql.isEnabled()){
			
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {

					String id = String.valueOf(e.getHcPlayer().getId());


					try {
						// Insert ctf player if not exists
						sql.execute(sql.prepareStatement(insertKillSkillPlayerSQL, id));
						
						// Insert ctf player stats if not exists
						sql.execute(sql.prepareStatement(insertKillSkillPlayerStatsSQL, id, sql.getMonth(), sql.getYear()));
						
						sql.execute(sql.prepareStatement(insertKillSkillPlayerPerksSQL, id));

						CachedRowSet perks = sql.query(sql.prepareStatement(selectKillSKillPlayerPerksSQL, id));
						perks.first();
						KillSkillPlayer killSkillPlayer = (KillSkillPlayer) e.getHcPlayer();
						killSkillPlayer.setExperienceAmount(perks.getInt("experience_amount"));
						killSkillPlayer.setLapisChance(perks.getInt("lapis_chance"));
						killSkillPlayer.setPotionChance(perks.getInt("potion_chance"));
						killSkillPlayer.setIronChance(perks.getInt("iron_chance"));
						killSkillPlayer.setDiamondChance(perks.getInt("diamond_chance"));
						killSkillPlayer.setBlazeChance(perks.getInt("blaze_chance"));
						killSkillPlayer.setSupplyAmount(perks.getInt("supply_amount"));
						killSkillPlayer.setSupplyLevel(perks.getInt("supply_level"));
					} catch (SQLException e1) {
						Log.severe("Couldn't find perks for player "+e.getHcPlayer().getName());
					}
				}
			});
		}
	}
	
	
	/**
	 * Save all players global data when game ends
	 * @param e
	 */
	@EventHandler
	public void onGameEnds(HCPlayerDBPersistedSessionEvent e){
		HCMySQLAPI sql = getMySQLAPI();
		
		if(sql.isEnabled()){
			
			KillSkillPlayer ksPlayer = (KillSkillPlayer) e.getHcPlayer();
			
			// Launch one async task to save all data
			Bukkit.getScheduler().runTaskAsynchronously(getPlugin(), new Runnable() {
				@Override
				public void run() {

					try{
						// Update ctf player global stats
						sql.execute(sql.prepareStatement(updateKillSkillPlayerGlobalStatsSQL,
								ksPlayer.getExperienceLooted(),
								ksPlayer.getLapisLooted(),
								ksPlayer.getPotionLooted(),
								ksPlayer.getIronLooted(),
								ksPlayer.getDiamondLooted(),
								ksPlayer.getBlazeLooted(),
								ksPlayer.getHeadLooted(),
								ksPlayer.getTntLooted(),
								ksPlayer.getGrenadeLooted(),
								ksPlayer.getCannonLooted(),
								ksPlayer.getSupplyOpened(),
								ksPlayer.getTimePlayed(),
								ksPlayer.getKills(),
								ksPlayer.getDeaths(),
								ksPlayer.getMoney(),
								ksPlayer.getId()
						));
						
	
						// Update ctf player stats
						sql.execute(sql.prepareStatement(updateKillSKillPlayerStatsSQL,
								ksPlayer.getExperienceLooted(),
								ksPlayer.getLapisLooted(),
								ksPlayer.getPotionLooted(),
								ksPlayer.getIronLooted(),
								ksPlayer.getDiamondLooted(),
								ksPlayer.getBlazeLooted(),
								ksPlayer.getHeadLooted(),
								ksPlayer.getTntLooted(),
								ksPlayer.getGrenadeLooted(),
								ksPlayer.getCannonLooted(),
								ksPlayer.getSupplyOpened(),
								ksPlayer.getTimePlayed(),
								ksPlayer.getKills(),
								ksPlayer.getDeaths(),
								ksPlayer.getMoney(),
								ksPlayer.getId(),
								sql.getMonth(),
								sql.getYear()
						));
						
					}catch(SQLException e){
						Logger.severe("Couldnt update Kill Skill player stats for player="+ksPlayer.getName()+" uuid="+ksPlayer.getUuid());
						e.printStackTrace();
					}
					
				}
			});
		}

	}
}
