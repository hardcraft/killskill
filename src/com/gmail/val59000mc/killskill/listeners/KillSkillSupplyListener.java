package com.gmail.val59000mc.killskill.listeners;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerJoinEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerQuitEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.killskill.commands.KillSkillTestCommand;
import com.gmail.val59000mc.killskill.events.KillSkillSupplyExpireEvent;
import com.gmail.val59000mc.killskill.events.KillSkillSupplyOpenEvent;
import com.gmail.val59000mc.killskill.events.KillSkillSupplyTouchesGroundEvent;
import com.gmail.val59000mc.killskill.supply.SupplyApi;
import com.gmail.val59000mc.killskill.supply.SupplyManager;

public class KillSkillSupplyListener extends HCListener{

	private SupplyApi supplyManager;
	private Map<UUID, Long> lastPickupRareLoot;
	private String pickupRareLore;
	
	@EventHandler
	public void afterLoad(HCAfterLoadEvent e){
		this.supplyManager = new SupplyManager(getApi());
		this.pickupRareLore = getStringsApi().get("killskill.supply.pickup-rare-lore").toString();
		this.lastPickupRareLoot = new HashMap<>();
		
		// link suuply manager with test command
		KillSkillTestCommand cmd = getApi().getRegisteredCommand(KillSkillTestCommand.class);
		cmd.setSupplyManager(supplyManager);
	}

	@EventHandler
	public void onJoin(HCPlayerJoinEvent e){
		this.lastPickupRareLoot.put(e.getHcPlayer().getUuid(), System.currentTimeMillis());
	}

	@EventHandler
	public void onQuit(HCPlayerQuitEvent e){
		this.lastPickupRareLoot.remove(e.getHcPlayer().getUuid());
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerPickupItem(PlayerPickupItemEvent event){
		Player player = event.getPlayer();
		ItemStack item = event.getItem().getItemStack();
		
		if(item.hasItemMeta() && item.getItemMeta().hasLore()){
			ItemMeta meta = item.getItemMeta();
			List<String> lore = meta.getLore();
			if(lore.size() > 0 && lore.get(lore.size()-1).equals(pickupRareLore)){
				Long now = System.currentTimeMillis();
				if(now - lastPickupRareLoot.get(player.getUniqueId()) > 2000){
					getStringsApi().get("killskill.supply.pickup-rare-loot")
						.replace("%player%", player.getName())
						.sendChatP();
					getSoundApi().play(Sound.CHICKEN_EGG_POP, 2, 1);
					lastPickupRareLoot.put(player.getUniqueId(), now);
				}
				lore.remove(lore.size()-1);
				meta.setLore(lore);;
				item.setItemMeta(meta);
			}
		}
		
	}
	
	@EventHandler
	public void onChestTouchesGround(KillSkillSupplyTouchesGroundEvent e){
		supplyManager.handleLandingChest(e.getSupplyChest());
	}
	
	@EventHandler
	public void onOpenChest(KillSkillSupplyOpenEvent e){
		supplyManager.handleOpenChest(e.getSupplyChest(), e.getKsPlayer());
	}
	
	@EventHandler
	public void onExpireChest(KillSkillSupplyExpireEvent e){
		supplyManager.handleExpireChest(e.getSupplyChest());
	}
}
