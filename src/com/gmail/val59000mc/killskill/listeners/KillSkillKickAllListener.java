package com.gmail.val59000mc.killskill.listeners;

import org.bukkit.event.EventHandler;

import com.gmail.val59000mc.hcgameslib.events.HCKickAllEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;

public class KillSkillKickAllListener extends HCListener{
	
	@EventHandler
	public void onKickAll(HCKickAllEvent e){
		for(HCPlayer hcPlayer : getPmApi().getPlayers(true,  PlayerState.PLAYING)){
			((KillSkillPlayer) hcPlayer).resetLastPVP();
		}
	}
}
