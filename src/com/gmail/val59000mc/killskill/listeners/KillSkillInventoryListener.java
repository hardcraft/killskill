package com.gmail.val59000mc.killskill.listeners;

import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.google.common.base.Preconditions;

public class KillSkillInventoryListener extends HCListener{
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDrinkPotion(PlayerItemConsumeEvent e){
		if(e.getItem().getType().equals(Material.POTION)){
		
			// add potion drinked
			KillSkillPlayer ksPlayer = (KillSkillPlayer) getPmApi().getHCPlayer(e.getPlayer());
			if(ksPlayer != null && ksPlayer.isPlaying()){

				// remove potion after drink
				Bukkit.getScheduler().runTaskLater(getPlugin(), new Runnable() {
					
					@Override
					public void run() {
						if(ksPlayer.isOnline()){
							ksPlayer.getPlayer().getInventory().remove(Material.GLASS_BOTTLE);
						}
					}
				}, 1);
				
			}
			
		}
	}
	
	
	
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDrop(PlayerDropItemEvent event){
		ItemStack stack = event.getItemDrop().getItemStack();
		if(!allowDrop(stack)){
			event.getItemDrop().remove();
		}
	}
	
	/**
	 * Returns true if item is allowed to drop, false otherwise
	 * @param stack
	 * @return
	 */
	public boolean allowDrop(ItemStack stack){
		Preconditions.checkNotNull(stack);
		
		switch(stack.getType()){
			case LEATHER_BOOTS:
			case LEATHER_CHESTPLATE:
			case LEATHER_LEGGINGS:
			case LEATHER_HELMET:
			case GOLD_HELMET:
			case GOLD_CHESTPLATE:
			case GOLD_LEGGINGS:
			case GOLD_BOOTS:
			case GLASS_BOTTLE:
			case STONE_SWORD:
			case ARROW:
				return false;
			case BOW:
			case IRON_SWORD:
				if(stack.getEnchantments().isEmpty())
					return false;
				break;
			default:
				break;
		}
		
		return true;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerPickupItem(PlayerPickupItemEvent event){
		Player player = event.getPlayer();
		ItemStack item = event.getItem().getItemStack();

		Map<Enchantment,Integer> ench = item.getEnchantments();
		
		switch(item.getType()){
			case CHAINMAIL_HELMET:
			case CHAINMAIL_CHESTPLATE:
			case CHAINMAIL_LEGGINGS:
			case CHAINMAIL_BOOTS:
				if(player.hasPermission("hardcraftpvp.killskill.nopickup.chainmail")){
					if(ench.size() == 0 
						|| (ench.size() == 1 && item.getEnchantmentLevel(Enchantment.PROTECTION_ENVIRONMENTAL) == 1)
						|| (ench.size() == 1 && item.getEnchantmentLevel(Enchantment.DEPTH_STRIDER) == 3)
						|| (ench.size() == 2 && item.getEnchantmentLevel(Enchantment.PROTECTION_ENVIRONMENTAL) == 1 && item.getEnchantmentLevel(Enchantment.DEPTH_STRIDER) == 3))
						event.setCancelled(true);
				}
				break;
			case IRON_SWORD:
				if(player.hasPermission("hardcraftpvp.killskill.nopickup.ironsword")){
					if(ench.size() == 0 || (ench.size() == 1 && item.getEnchantmentLevel(Enchantment.DAMAGE_ALL) == 1))
						event.setCancelled(true);
				}
				break;
			default:
				break;
		}
		
	}
	
}
