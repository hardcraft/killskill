package com.gmail.val59000mc.killskill.listeners;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;

import com.gmail.val59000mc.hcgameslib.events.HCAfterLoadEvent;
import com.gmail.val59000mc.hcgameslib.events.HCPlayerQuitEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.tasks.HCTask;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.google.common.collect.Sets;

import me.konsolas.aac.AAC;
import me.konsolas.aac.api.PlayerViolationCommandEvent;

public class KillSkillConnectionListener extends HCListener{

	@EventHandler(priority=EventPriority.HIGHEST)
	public void onPlayerJoin(PlayerJoinEvent e){
		Player player = e.getPlayer();

		Bukkit.getScheduler().runTaskLater(getPlugin(), ()->{
			player.getInventory().addItem(new ItemStack(Material.WOOD_SWORD, 1));
			player.updateInventory();
			Bukkit.getScheduler().runTaskLater(getPlugin(), ()->{
				player.getInventory().remove(new ItemStack(Material.WOOD_SWORD, 1));
				player.updateInventory();
			}, 1);
		}, 50);
		
		
	}
	
	@EventHandler(priority=EventPriority.LOWEST)
	public void onPlayerQuit(PlayerQuitEvent e){
		
		KillSkillPlayer ksPlayer = (KillSkillPlayer) getPmApi().getHCPlayer(e.getPlayer());
		if(ksPlayer != null){
			
			if(ksPlayer.is(PlayerState.PLAYING)){
			
				long remainingFightSeconds = ksPlayer.remainingFightSeconds();
				if(remainingFightSeconds > 0 && ksPlayer.isOnline()){
					Player player = e.getPlayer();
					if(player.getHealth() > 0){
						ksPlayer.setIsPvpLogging(true);
						
						player.damage(1000);
						
						getStringsApi()
							.get("killskill.pvp-logger")
							.replace("%player%", player.getName())
							.sendChatP();
					}
				}
				
			}else if(ksPlayer.isAny(Sets.newHashSet(PlayerState.DEAD, PlayerState.SPECTATING, PlayerState.VANISHED))){
				Player player = e.getPlayer();
				player.teleport(getApi().getWorldConfig().getLobby());
			}
			
		}
		
	}
	
	@EventHandler
	public void onPlayerQuit(HCPlayerQuitEvent e){
		
		if(e.getHcPlayer().is(PlayerState.PLAYING)){
			getStringsApi()
				.get("killskill.player-has-left-the-game")
				.replace("%player%", e.getHcPlayer().getName())
				.sendChatP();
		}
	
		int currentCount = getPmApi().getPlayersCount(true, PlayerState.PLAYING);
		
		if(currentCount == getPmApi().getMaxPlayers()){
			List<HCPlayer> spectating = getPmApi().getPlayersMultiplesStates(true, Sets.newHashSet(PlayerState.SPECTATING, PlayerState.DEAD));
			getStringsApi()
				.get("killskill.game-not-full-anymore-alert")
				.sendChatP(spectating);
			
			// play fast sound
			getApi().buildTask("sound game not full anymore", new HCTask() {
				
				@Override
				public void run() {
					getSoundApi().play(spectating, Sound.NOTE_PLING, 1, 2);
				}
			})
			.withIterations(5)
			.withInterval(3)
			.build()
			.start();
		}
	
	}
	
	@EventHandler
    public void afterGameLoad(HCAfterLoadEvent e) {
		
		Plugin aac = Bukkit.getPluginManager().getPlugin("AAC");
		if(aac != null && aac instanceof AAC){
			getApi().registerListener(new AACListener());
		}
		
    }
	
	private class AACListener extends HCListener{
		
		@EventHandler
	    public void onPlayerViolationCommand(PlayerViolationCommandEvent e) {
			KillSkillPlayer ksPlayer = (KillSkillPlayer) getPmApi().getHCPlayer(e.getPlayer());
			if(ksPlayer != null){
				ksPlayer.resetLastPVP();
			}
	    }
		
	}
}
