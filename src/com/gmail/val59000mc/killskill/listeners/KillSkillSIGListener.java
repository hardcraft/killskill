package com.gmail.val59000mc.killskill.listeners;

import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.inventory.ItemStack;

import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.hcgameslib.players.HCPlayer;
import com.gmail.val59000mc.hcgameslib.players.PlayerState;
import com.gmail.val59000mc.hcgameslib.stuff.Stuff;
import com.gmail.val59000mc.killskill.common.Constants;
import com.gmail.val59000mc.killskill.events.KillSkillOpenSpecInventoryEvent;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.simpleinventorygui.actions.Action;
import com.gmail.val59000mc.simpleinventorygui.events.SIGRegisterTriggerEvent;
import com.gmail.val59000mc.simpleinventorygui.inventories.Inventory;
import com.gmail.val59000mc.simpleinventorygui.inventories.InventoryManager;
import com.gmail.val59000mc.simpleinventorygui.items.Item;
import com.gmail.val59000mc.simpleinventorygui.players.PlayersManager;
import com.gmail.val59000mc.simpleinventorygui.players.SigPlayer;
import com.gmail.val59000mc.simpleinventorygui.triggers.RightClickEntityUUIDTrigger;
import com.gmail.val59000mc.simpleinventorygui.triggers.TriggerManager;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class KillSkillSIGListener extends HCListener{

	@EventHandler
	public void onSigTriggersLoad(SIGRegisterTriggerEvent e){

		///////////////////////////////
		// Assign actions to armor stands
		///////////////////////////////
		
//		armorstands:
//		  member: 8e44dd7b-7cb3-4970-8165-3bfabfa365d2
//		  vip: ad97f1a4-6323-49fd-b5da-74fa874dbb10
//		  vip+: 4bfdbb49-f5ae-488f-ad31-8aa3c773cf90
//		  free-vip: 063adbe8-87d4-475f-8c7c-828556311061
//		  free-vip+: eb33e8e6-292d-48ae-95ba-bb5f86724a5b
		
		TriggerManager tm = e.getTm();
		
		ConfigurationSection section = getApi().getConfig().getConfigurationSection("armor-stands");
		if(section != null){
			
			UUID member = UUID.fromString(section.getString("member", UUID.randomUUID().toString()));
			tm.addTrigger(new RightClickEntityUUIDTrigger(Lists.newArrayList(), 0, "kits", Lists.newArrayList(new GiveKitAction(getItemsApi().getStuff(Constants.MEMBER_STUFF_NAME))), member));	

			UUID vip = UUID.fromString(section.getString("vip", UUID.randomUUID().toString()));
			tm.addTrigger(new RightClickEntityUUIDTrigger(Lists.newArrayList("hardcraftpvp.killskill.kit.vip"), 0, "kits", Lists.newArrayList(new GiveKitAction(getItemsApi().getStuff(Constants.VIP_STUFF_NAME))), vip));
			
			UUID vipPlus = UUID.fromString(section.getString("vip+", UUID.randomUUID().toString()));
			tm.addTrigger(new RightClickEntityUUIDTrigger(Lists.newArrayList("hardcraftpvp.killskill.kit.vipplus"), 0, "kits", Lists.newArrayList(new GiveKitAction(getItemsApi().getStuff(Constants.VIP_PLUS_STUFF_NAME))), vipPlus));
			
			UUID freeVip = UUID.fromString(section.getString("free-vip", UUID.randomUUID().toString()));
			tm.addTrigger(new RightClickEntityUUIDTrigger(Lists.newArrayList(), 3600, null, Lists.newArrayList(new GiveKitAction(getItemsApi().getStuff(Constants.VIP_STUFF_NAME))), freeVip));
			
			UUID freeVipPlus = UUID.fromString(section.getString("free-vip+", UUID.randomUUID().toString()));
			tm.addTrigger(new RightClickEntityUUIDTrigger(Lists.newArrayList(), 3600, null, Lists.newArrayList(new GiveKitAction(getItemsApi().getStuff(Constants.VIP_PLUS_STUFF_NAME))), freeVipPlus));
			
		}
	}
	
	@EventHandler
	public void onKillSkillOpenSpecInv(KillSkillOpenSpecInventoryEvent e){
		
		if(e.getKsPlayer().isOnline() && e.getKsPlayer().isAny(Sets.newHashSet(PlayerState.SPECTATING, PlayerState.DEAD, PlayerState.VANISHED))){
			Player player = e.getKsPlayer().getPlayer();
			SigPlayer sigPlayer = PlayersManager.getSigPlayer(player);
			if(sigPlayer != null){
				
				InventoryManager im = InventoryManager.instance();
				String invName = Constants.SPEC_INV_NAME+"-"+player.getName();
				Inventory invToRemove = im.getInventoryByName(invName);
				if(invToRemove != null)
					im.removeInventory(invToRemove);
				
				Inventory invToAdd = new Inventory(invName, "§aSpectateur", 6);
				
				int pos = 0;
				for(HCPlayer hcPlayer : getPmApi().getPlayers(true, PlayerState.PLAYING)){
					if(pos == 54){
						break;
					}
					Item playerHeadItem = getCallbacksApi().getSpecInvPlayerItem(hcPlayer).withPosition(pos).build();
					playerHeadItem.setActions(Lists.newArrayList(new SpecInventoryTpAction(hcPlayer)));
					invToAdd.addItem(playerHeadItem);
					pos++;
				}
				
				im.addInventory(invToAdd);
				invToAdd.openInventory(player, sigPlayer);
				
			}
		}
		
		
	}
	
	/**
	 * Action to execute when teleporting to a player when a spectator
	 * clicks on a player head in the spectator inventory
	 *
	 */
	private class SpecInventoryTpAction extends Action{

		
		private HCPlayer target;

		public SpecInventoryTpAction(HCPlayer target){
			this.target = target;
		}
		
		@Override
		public void executeAction(Player player, SigPlayer sigPlayer) {
			HCPlayer hcPlayer = getPmApi().getHCPlayer(player);
			if(hcPlayer != null && target.is(true, PlayerState.PLAYING)){
				player.closeInventory();
				
				Location loc = target.getPlayer().getLocation().clone();
				player.teleport(loc.subtract(loc.getDirection().multiply(1.5)));
				getSoundApi().play(hcPlayer, Sound.ENDERMAN_TELEPORT, 1, 1);
				getStringsApi().get("messages.spec.teleported-to-target").replace("%player%", target.getColor()+target.getName()).sendChat(hcPlayer);
			}else{
				getStringsApi().get("messages.spec.target-not-playing").replace("%player%", target.getName());
			}
			
			InventoryManager im = InventoryManager.instance();
			String invName = Constants.SPEC_INV_NAME+"-"+player.getName();
			Inventory invToRemove = im.getInventoryByName(invName);
			if(invToRemove != null)
				im.removeInventory(invToRemove);

			executeNextAction(player, sigPlayer,true);
			
			
		}
		
	}
	
	/**
	 * Give stuff action
	 *
	 */
	private class GiveKitAction extends Action{

		
		private Stuff stuff;

		public GiveKitAction(Stuff stuff){
			this.stuff = stuff;
		}
		
		@Override
		public void executeAction(Player player, SigPlayer sigPlayer) {

			KillSkillPlayer ksPlayer = (KillSkillPlayer) getPmApi().getHCPlayer(player);
			if(ksPlayer != null && ksPlayer.isPlaying()){
				Inventories.give(player, stuff.getInventory());

				int count = 0;
				for(int i=0 ; i<4 ; i++){
					ItemStack slot = player.getInventory().getArmorContents()[i];
					if(slot == null || slot.getType().equals(Material.AIR)){
						count++;
					}
				}
				
				if(count == 4){ // empty armor
					Inventories.setArmor(player, stuff.getArmor());
					getStringsApi()
						.get("killskill.armor-equiped")
						.sendActionBar(ksPlayer);
				}else{
					Inventories.give(player, stuff.getArmor());
					getStringsApi()
						.get("killskill.armor-received")
						.sendActionBar(ksPlayer);
				}
				
				getSoundApi().play(ksPlayer, Sound.CHICKEN_EGG_POP, 1, 1);
			}

			executeNextAction(player, sigPlayer,true);
			
			
		}
		
	}
	
	
}
