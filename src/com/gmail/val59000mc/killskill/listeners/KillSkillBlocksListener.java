package com.gmail.val59000mc.killskill.listeners;

import java.lang.reflect.Field;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftLivingEntity;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftTNTPrimed;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.entity.ItemMergeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;

import com.gmail.val59000mc.hcgameslib.events.HCBeforeContinuousEvent;
import com.gmail.val59000mc.hcgameslib.listeners.HCListener;
import com.gmail.val59000mc.killskill.bonuses.PortalBonusManager;
import com.gmail.val59000mc.killskill.callbacks.KillSkillCallbacks;
import com.gmail.val59000mc.killskill.common.Constants;
import com.gmail.val59000mc.killskill.items.KillSkillItems;
import com.gmail.val59000mc.killskill.players.KillSkillPlayer;
import com.gmail.val59000mc.spigotutils.Inventories;
import com.gmail.val59000mc.spigotutils.ItemCompareOption;
import com.gmail.val59000mc.spigotutils.Locations.LocationBounds;
import com.gmail.val59000mc.spigotutils.particles.ParticleEffect;
import com.gmail.val59000mc.spigotutils.particles.ParticleEffect.BlockData;

import net.minecraft.server.v1_8_R3.EntityLiving;
import net.minecraft.server.v1_8_R3.EntityTNTPrimed;

public class KillSkillBlocksListener extends HCListener{
	
	private PortalBonusManager beaconManager;
	private LocationBounds lobbyBounds;
	
	// load beaconManager
	@EventHandler
	public void onStartContinuous(HCBeforeContinuousEvent e){
		this.beaconManager = ((KillSkillCallbacks) getCallbacksApi()).getPortalBonusManager();
		this.lobbyBounds = ((KillSkillCallbacks) getCallbacksApi()).getLobbyBounds();
	}
	
	// cancel item merge in spawn
	@EventHandler
	public void onItemMerge(ItemMergeEvent e){
		if(lobbyBounds.contains(e.getEntity().getLocation())){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent e){
		if(!e.getPlayer().isOp()){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onArrowIgniteTnt(EntityChangeBlockEvent e){
		if(e.getEntity().getType().equals(EntityType.ARROW) && e.getBlock().getType().equals(Material.TNT)){
			e.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPlaceHeadOnEndBlock(BlockPlaceEvent e){
		Block portal = e.getBlock().getRelative(BlockFace.DOWN);
		Player player = e.getPlayer();

		if(portal != null && portal.getType().equals(Material.ENDER_PORTAL_FRAME)){
			ItemStack item = e.getItemInHand();
			e.setCancelled(true);
			if(KillSkillItems.isHeadItem(item)){ // place head item on portal frame
				
				KillSkillPlayer ksPlayer = (KillSkillPlayer) getPmApi().getHCPlayer(e.getPlayer());
				boolean isbonusStarted = beaconManager.startPortalBonus(ksPlayer, portal);
				
				if(isbonusStarted){
					Inventories.remove(player.getInventory(), KillSkillItems.getHeadItem(1), new ItemCompareOption.Builder().withLore(true).build());
					player.updateInventory();
				}
				
			}
		}else if(KillSkillItems.isTntItem(e.getItemInHand()) && !lobbyBounds.contains(e.getPlayer().getLocation())){ // place TNT
			// place tnt
			
			Location tntLocation = e.getBlock().getLocation().clone().add(0.5, 0, 0.5);
			e.getBlock().setType(Material.AIR, false);
			
			// Spawn a tnt
			TNTPrimed tnt = (TNTPrimed) player.getWorld().spawnEntity(tntLocation,EntityType.PRIMED_TNT);
			tnt.setFuseTicks(30);
			tnt.setYield(6);
			tnt.setIsIncendiary(false);
			tnt.setMetadata(Constants.TNT_METADATA, new FixedMetadataValue(getPlugin(), null));

			// Change via NMS the source of the TNT by the player
			EntityLiving nmsEntityLiving = (((CraftLivingEntity) player).getHandle());
			EntityTNTPrimed nmsTNT = (((CraftTNTPrimed) tnt).getHandle());
			try {
			    Field source = EntityTNTPrimed.class.getDeclaredField("source");
			    source.setAccessible(true);
			    source.set(nmsTNT, nmsEntityLiving);
			} catch (Exception ex) {
				e.setCancelled(true);
			    ex.printStackTrace();
			}
			
			ParticleEffect.BLOCK_CRACK.display(new BlockData(Material.REDSTONE_BLOCK, (byte) 0), 1F, 1F, 1F, 1F, 30, tntLocation, 30);
			ParticleEffect.CLOUD.display(0f, 0.5f, 0f, 1, 30, tntLocation, 30);
			
			Inventories.remove(player.getInventory(), KillSkillItems.getTntItem(1), new ItemCompareOption.Builder().withLore(true).build());
			player.updateInventory();
			
			e.setCancelled(true);
			
		}else{
			if(!e.getPlayer().isOp()){
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onBlockExplode(BlockExplodeEvent event){
		event.setCancelled(true);
	}
	
	
	@EventHandler
	public void onEntityExplodeBlockst(EntityExplodeEvent e){
		e.blockList().clear();
	}
}
